import sys
import numpy as np

import utils.vrep_utils as utils

class robotiq85(object):
  def __init__(self, sim_client):
    '''
    VRep robotiq85 gripper class.
    Currently only does torque control.
    This see child script of ROBOTIQ_85 in simulation.
    Signal "gripperOpen" is used to control the gripper

    Args:
      - sim_client: VRep client object to communicate with simulator over
      Returns: the simulator api call return
      '''
    self.sim_client = sim_client


  def open(self):
    '''
    Open the gripper as much as is possible
    Returns: the simulator api call return    
    '''
    gripperOpen = 1
    ret=utils.controlRobotiq85(self.sim_client, gripperOpen)
    return ret

  def close(self):
    '''
    Close the gripper as much as is possible
    Returns: the simulator api call return 
    '''
    gripperOpen = 0
    ret=utils.controlRobotiq85(self.sim_client, gripperOpen)
    return ret
