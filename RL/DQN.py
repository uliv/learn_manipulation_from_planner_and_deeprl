# -*- coding: utf-8 -*-
"""
Reinforcement Learning (DQN) with V-REP simulator to learn manipulation task
=====================================
based on:
website: https://pytorch.org/tutorials/intermediate/reinforcement_q_learning.html

  # E.g. run experiment specified by config.ini 2 times (creates run-sub folders "0","1")
  python DQN.py --config_file config.ini--run_start 0 --run_n 2
"""

import sys
import os
import math
import random
import numpy as np

import matplotlib.pyplot as plt
import argparse
from collections import namedtuple
from itertools import count

import torch
import torch.optim as optim
#import torch.nn.functional as F

sys.path.append('./')
sys.path.append('..')
sys.path.append('../simulation')
sys.path.append('../nets')
sys.path.append('../scripts')

import utils.vrep_utils as utils
import nets
import offlineSimEnv as offlineEnv
import replayBuffer as mem
import expertExplorePolicy as expert

#from scene_gen_utils import vrepEnv, import_config_planner, write_list_to_txt_file, copy_files_to_path
import scene_gen_utils as scene
import configparser
import DQN_utils
import actions as a




def main(config_file="config.ini",run_no=None,env_list=[]):
  plt.ion()  
  print("main start")
  print("random sample in main (is it random?)", random.random())
  
  # import parameters setting for script from config.ini file  
  config = configparser.RawConfigParser()
  config.read(config_file)   
  git_location = config.get('main', 'git_location') # need this to backup files
  
  # logging section
  output_path = config.get('logging', 'output_path')
  if run_no != None:
    output_path = os.path.join(output_path, str(run_no))
  print('output_path', output_path)  
  file_out = DQN_utils.create_logging_files(output_path, git_location, config_file)  

  save_intermediate_models = config.getboolean('logging', 'save_intermediate_models')
  save_intermediate_models_every_x_episodes =\
    config.getint('logging', 'save_intermediate_models_every_x_episodes')
  save_plots = config.getboolean('logging', 'save_plots')
  n_episodes_plot = config.getint('logging', 'n_episodes_plot')

  save_experience = config.getboolean('logging', 'save_experience')
  if save_experience:
    file_general,file_scenes,file_images,dataset_path_images,dataset_path_labels\
      = scene.logging_files_for_save_experience(output_path,config_file)
    file_general.close() # not writing anything to this (I don't need it now)
    file_scenes.close()  # not writing anything to this (I don't need it now)
  
  # Device section
  use_GPU = config.getboolean('device', 'use_GPU')
  GPU_no = config.getint('device', 'GPU_no')  
  
  # simulator section
  use_fixed_seed = config.getboolean('simulator', 'use_fixed_seed')  
  sim_port_offset = config.getint('simulator', 'sim_port_offset') 
  use_offline_sim_data = config.getboolean('simulator', 'use_offline_sim_data')
  NUM_ACTIONS = config.getint('simulator', 'NUM_ACTIONS') 
  use_multiple_scene = config.getboolean('simulator', 'use_multiple_scene')

  # training_param section
  EVAL_ONLY = config.getboolean('training_param', 'EVAL_ONLY')   
  use_pretrain_model = config.getboolean('training_param', 'use_pretrain_model')
  pre_trained_model = config.get('training_param', 'pre_trained_model')
  use_pretrain_folder = config.getboolean('training_param', 'use_pretrain_folder')
  pre_train_folder = config.get('training_param', 'pre_train_folder')
  SCALE_FC3 = config.getfloat('training_param', 'SCALE_FC3')  

  # Expert exploration  
  use_expert_explore = config.getboolean('training_param', 'use_expert_explore')
  policy_init_str = config.get('training_param', 'policy_init_str')
  expert_prob = config.getfloat('training_param', 'expert_prob')
  expert_model = config.get('training_param', 'expert_model')  
  
  # Learning setting
  BATCH_SIZE = config.getint('training_param', 'BATCH_SIZE')
  LR = config.getfloat('training_param', 'LR')
  MIN_DATA_BEFORE_LEARNING = config.getint('training_param', 'MIN_DATA_BEFORE_LEARNING')
  GAMMA = config.getfloat('training_param', 'GAMMA')
  EPS_START = config.getfloat('training_param', 'EPS_START')
  EPS_END = config.getfloat('training_param', 'EPS_END') 
  EPS_DECAY = config.getfloat('training_param', 'EPS_DECAY')
  STEPS_TIMEOUT = config.getint('training_param', 'STEPS_TIMEOUT')   
  TIMEOUT_REWARD = config.getfloat('training_param', 'TIMEOUT_REWARD')   
  num_episodes = config.getint('training_param', 'num_episodes')   
  N_OPTIMIZER_STEPS = config.getint('training_param', 'N_OPTIMIZER_STEPS')   
  TAU = config.getfloat('training_param', 'TAU')   
  REPLAY_BUFFER_SIZE = config.getint('training_param', 'REPLAY_BUFFER_SIZE')   
  P_TRAJ_START = config.getfloat('training_param', 'P_TRAJ_START') 
  PLANNED_TRAJ_MAX_EPISODE = config.getint('training_param', 'PLANNED_TRAJ_MAX_EPISODE') 
  PLANNER_FOR_NEXT_Q = config.getboolean('training_param', 'PLANNER_FOR_NEXT_Q')   

  use_PER_memory = config.getboolean('training_param', 'use_PER_memory')

  BASELINE = config.get('training_param', 'BASELINE')

  learn_best_q_value_only = config.getboolean('BESTQ', 'learn_best_q_value_only')
  best_q_value_margin = config.getboolean('BESTQ', 'best_q_value_margin')
  q_margin = config.getfloat('BESTQ_DQFD', 'q_margin')
  DQFD_w = config.getfloat('BESTQ_DQFD', 'DQFD_w')

  if BASELINE != "NONE":
    print("BASELINE", BASELINE)
    assert use_expert_explore,\
      "baseline requires expert explore (the planner) for expert rollouts"
  if not use_expert_explore and expert_prob > 0.0:
    print("Warning: use_expert_explore is false, but expert_prob is set to ", expert_prob)
    print("Setting expert_prob = 0.0")
    expert_prob = 0.0

  if BASELINE != "BESTQ": 
    assert not learn_best_q_value_only ,\
      "learn_best_q_value_only should only be used for BASELINE == BESTQ"
    assert not best_q_value_margin ,\
      "learn_best_q_value_only should only be used for BASELINE == BESTQ"

  if BASELINE != "BESTQ" and BASELINE != "DQFD":
    assert q_margin == 0.0, "q_margin should only be used for BASELINE BESTQ and DQFD"
    
  if BASELINE == "BESTQ" and BASELINE == "DQFD":
    print("q_margin", q_margin)
    assert q_margin > 0.0, "q_margin needed for BASELINE BESTQ and DQFD"  

  add_noise = config.getboolean('image_processing', 'add_noise')
  noise_prob_min = config.getfloat('image_processing', 'noise_prob_min')  
  noise_prob_max = config.getfloat('image_processing', 'noise_prob_max')   
    
  if use_fixed_seed:
    print("Using fixed seed for simulator setup.")
    random.seed(7) # fixed seed for deteministic initial scene,
                   # this fixes the peg/hole config, but not the clutter
  else:
    random.seed()  


  if use_offline_sim_data:
    if use_multiple_scene:
      # data loaded before main() and passed as a list of env     
      # initialize to use 1st scene
      env_idx = 0
      env = env_list[env_idx]
    else:
      env = offlineEnv.offlineSimEnv(config_file)
      env.load_data()
      
    if save_experience:
      # saves rollouts as a dataset for SL learning
      env.set_dataset_path_images(dataset_path_images)
      env.set_file_images(file_images)
  else:
    # use V-REP simulator
    env = scene.vrepEnv(config_file,sim_port_offset)
    env.setup_scene()

  env.init_state()
  delta_x, delta_y, delta_z = env.get_grid_deltas()    
  
  q_values, best_action, best_index = env.getLabels()
  random.seed()  
  print("random sample in main (is it random?)", random.random())  
  
  # use simulator to eval a single net (or many nets from many runs at different
  # numers of episodes)
  if EVAL_ONLY:
    if pre_trained_model == '':
      print("evaluating random net")
    if use_expert_explore:
      print("Warning: using expert explore with eval")
      print("Rollout data to create a dataset?")      
    else:
      EPS_START = 0.0
      EPS_END = 0.0
    LR = 0.0
    P_TRAJ_START = 0.0
    PLANNED_TRAJ_MAX_EPISODE = 0
    PLANNER_FOR_NEXT_Q = False
    if(use_pretrain_model and use_pretrain_folder):
      input("Warning: Make sure you setup your config file correctly. Do you want to evaluate a single " +\
          "network (use_pretrain_model) or multiple runs (use_pretrain_folder)?")

    


  cuda_device = "cuda:" + str(GPU_no)
  print("torch.cuda.is_available()", torch.cuda.is_available())
  device = torch.device(cuda_device if (torch.cuda.is_available() and use_GPU) else "cpu") 
 
  Transition = namedtuple('Transition',
                          ('state', 'action', 'next_state', 'reward', 'next_Q',
                           'current_q_expert', 'current_a_expert',
                           'isExpert'))
  
  # which output of net to use (softmax for classification or not)
  classification = (BASELINE == "DAGGER")
  policy_net = nets.DQN_lenet(classification).to(device)
  target_net = nets.DQN_lenet(classification).to(device)

  if pre_trained_model != '' and use_pretrain_model:
    print("Loading pretrained model")
    print(pre_trained_model)
    state_dict = torch.load(pre_trained_model)
    if SCALE_FC3 != 1.0:
      # modify weights of last layer if using a policy net (classification)
      # as initial weights
      print("scaling weights with factor: ", SCALE_FC3)
      state_dict['fc3.weight'] = SCALE_FC3*state_dict['fc3.weight']
      state_dict['fc3.bias'] = SCALE_FC3*state_dict['fc3.bias']
    policy_net.load_state_dict(state_dict)
    target_net.load_state_dict(state_dict)
    
  if use_pretrain_folder and EVAL_ONLY:
    # for evaluating different networks
    # (at different number of learning episodes)
    # change dir into "current run" folder (run_no)
    pre_train_folder = os.path.join(pre_train_folder,str(run_no))
    nets_at_different_iterations = os.listdir(pre_train_folder)
    nets_at_different_iterations = [k for k in nets_at_different_iterations
                                    if (k.startswith("policy_net") and k.endswith(".pt"))]
    nets_at_different_iterations.sort(key= lambda net : int(net.split('_')[-1].split('.')[0]))    
    iteration_2_net_dict = {}    
    for net in nets_at_different_iterations:
      last_digit = int(net.split('_')[-1].split('.')[0])
      iteration_2_net_dict[last_digit] = net  
    print("iteration_2_net_dict", iteration_2_net_dict)
    state_dict = torch.load(os.path.join(pre_train_folder,iteration_2_net_dict[0]))
    policy_net.load_state_dict(state_dict)

  if use_expert_explore:
    # Replace some portion of random actions by an expert action
    # Create/init expert object
    print("Expert Explore")
    if policy_init_str == 'move_down':
      policy_init = expert.policy_init_move_down(device)
    elif policy_init_str == 'net' and expert_model != '':
      print("Loading expert_model model as policy_init")
      print(expert_model)
      net_init = nets.DQN_lenet().to(device)
      state_dict = torch.load(expert_model)
      net_init.load_state_dict(state_dict)
      policy_init = expert.policy_init_net(net_init)
    elif policy_init_str == 'planner':
      print("Using planner as policy_init (expert explore)")
      policy_init = expert.policy_init_planner(env,device)
    else:
      print("Invalid string for policy_init_str: ", policy_init_str)
      
  
  target_net.load_state_dict(policy_net.state_dict())
  target_net.eval()  
  optimizer = optim.Adam(policy_net.parameters(),lr=LR)

  if use_PER_memory:
    memory = mem.PrioritizedReplayMemory(REPLAY_BUFFER_SIZE,Transition,device)
  else:
    memory = mem.ReplayMemory(REPLAY_BUFFER_SIZE,Transition)
  
  steps_done = 0  
  # lists to store values for plotting
  episode_durations = []
  episode_total_rewards = []
  episode_success = []
  losses_1_episode = []
  losses_episodes = []
  data_to_file_several_lines = []

  # for counting various events
  scene_no, traj_no, img_no, traj_last = 0,0,0,0 

  for i_episode in range(num_episodes):
      ############################################
      # SWITCH ENVIRONMENT (if multiple scenes)
      # Load a new net if evaluating
      # Update epsilon
      ############################################
      eps_threshold = EPS_END + (EPS_START - EPS_END) * \
        math.exp(-1. * i_episode / EPS_DECAY)
      # after around 40k episodes, the value is so small, so better just set to 0  
      if eps_threshold < 0.001: 
        eps_threshold = 0.0 
      # Initialize the environment and state
      if i_episode % 10 == 0:
        print("Episode: ", i_episode, "eps_threshold: ", eps_threshold)
        
      if use_multiple_scene:
        env_idx = i_episode % len(env_list)
        env = env_list[env_idx]
        scene_no = env_idx
        traj_no = int(i_episode / len(env_list)) # integer part without fraction 
        if save_experience and traj_no == 0:
          env.set_dataset_path_images(dataset_path_images)
          env.set_file_images(file_images)

      img_no = 0
      traj_last = 0        
      
      if use_expert_explore and policy_init_str == 'planner':
        # only policy_init_planner needs to be updated with new env
        # to get the correct ground truth
        # because it calls env.getLabels()
        policy_init.update_env(env)
      
      if use_pretrain_folder:
        if i_episode in iteration_2_net_dict:
          path_2_net = os.path.join(pre_train_folder,iteration_2_net_dict[i_episode])
          print("Loading net ", path_2_net)
          state_dict = torch.load(path_2_net)
          policy_net.load_state_dict(state_dict)

      ############################################
      #
      # INITIALIZE STATE
      #
      ############################################
      init_success = env.init_state(high_z_only=True) # high_z_only=True: allow only "upper half" in z-axis
      debug_history = []
      debug_history.append(env.get_current_state_idx3d())

      if not init_success:
        print("env.get_scene_sub_folder()", env.get_scene_sub_folder())        
        input("not init_success, press a key")

      state =  env.getObservations()

      # add noise to observation
      if add_noise:
        replace_rate = random.uniform(noise_prob_min, noise_prob_max)
        image_zeros = np.ones(state.shape)
        mask = np.random.choice([0, 1], size=state.shape, p=((1 - replace_rate), replace_rate)).astype(np.bool)
        state[mask] = image_zeros[mask]
     

      state = DQN_utils.depthImageToTensor(state,device)
      total_rewards = 0.0
      success = 0.0
      
      ############################################
      #
      # EXECUTE TRAJECTORY
      #
      ############################################      
      for c in count():
          debug = False
          ######################
          # SELECT ACTION
          ######################
          # 1-eps: use current policy
          # eps: use expert_prob eps*expert_prob, use random eps(1-expert_prob)
          action_index, isExpert = a.select_action_expert_explore(state,policy_net,policy_init,device,eps_threshold,expert_prob,NUM_ACTIONS,debug)

          steps_done += 1 # total steps         
          
          # This is for 27 actions (allowing diagonal moves)
          action = utils.actionIndexToAction(action_index.item(),delta_x,delta_y,delta_z) # put into "step" function or observation function (or wrapper function) ?

          # save the image before the step
          if save_experience:
            env.save_image_and_labels(scene_no, traj_no, img_no, traj_last)
            
          current_q_planner, _, current_a_planner = env.getLabels()
          current_q_planner, only_bad_q = DQN_utils.qvaluesToTensor(current_q_planner, -TIMEOUT_REWARD, device,
                                              learn_best_q_value_only,
                                              best_q_value_margin,
                                              q_margin)
          if only_bad_q: # this should not happen
            # print debug info
            print("env.get_scene_sub_folder()", env.get_scene_sub_folder())
            print("c", c)
            print("debug_history (x,y,z of states visited in this episode)", debug_history)
            env.getObservations()
            plt.imshow(env.getObservations())
            plt.show()            
            input("press a key")
          current_a_planner = torch.tensor([[current_a_planner-1]], device=device, dtype=torch.long)
          
          #################################
          # EXECUTE ACTION IN ENV
          #################################
          if c < STEPS_TIMEOUT:
            reward, done = env.step(action)
          else:
            reward = TIMEOUT_REWARD # same as collision or out of workspace
            done = 1
          debug_history.append(env.get_current_state_idx3d())

          total_rewards += reward
          reward = torch.tensor([reward], device=device)
          
  
          # Observe new state
          if not done:
              next_state = env.getObservations()
              if add_noise:
                replace_rate = random.uniform(noise_prob_min, noise_prob_max)
                image_zeros = np.ones(next_state.shape)
                mask = np.random.choice([0, 1], size=next_state.shape, p=((1 - replace_rate), replace_rate)).astype(np.bool)
                next_state[mask] = image_zeros[mask]
                
              next_state = DQN_utils.depthImageToTensor(next_state,device)
              if PLANNER_FOR_NEXT_Q:
                next_Q, best_action, best_index = env.getLabels()
                
          else:
              next_state = None
              if PLANNER_FOR_NEXT_Q:
                next_Q = [0.0 for _ in range(NUM_ACTIONS)]
          if PLANNER_FOR_NEXT_Q:      
            next_Q, only_bad_q = DQN_utils.qvaluesToTensor(next_Q, -TIMEOUT_REWARD, device=device)
             
          else:
            next_Q = [None for _ in range(NUM_ACTIONS)]
          #################################
          # STORE TRANSITION IN MEMORY
          #################################
          memory.push(state, action_index, next_state, reward, next_Q,
                      current_q_planner, current_a_planner, isExpert)
            
          
          if(len(memory) == REPLAY_BUFFER_SIZE-1):
            print("###############################################################")
            print("Episode ", i_episode)
            print("replay memory memory is (almost) full")
            print("###############################################################")            

  
          # Move to the next state
          state = next_state
  
          #################################
          # OPTIMIZE MODEL
          #################################
          if len(memory) >= BATCH_SIZE and\
             len(memory) > MIN_DATA_BEFORE_LEARNING and\
             not EVAL_ONLY:
            loss_sum = 0
            for step in range(N_OPTIMIZER_STEPS):
              loss_sum += DQN_utils.optimize_model(optimizer, policy_net, target_net,
                                                   memory, BATCH_SIZE, GAMMA,
                                                   Transition, device, TAU,
                                                   use_PER_memory, PLANNER_FOR_NEXT_Q,
                                                   BASELINE, q_margin, DQFD_w)
            losses_1_episode.append(loss_sum/N_OPTIMIZER_STEPS)
          #################################
          # END OF EPISODE
          # PLOTTING, LOGGING, DATASET GENERATION
          #################################             
          if done:
              episode_durations.append(c)
              episode_total_rewards.append(total_rewards)
              if reward > TIMEOUT_REWARD:
                success = 1.0
              episode_success.append(success)
              ## Write plot data to a list, which is written every 10 episode to file
              #eps_threshold = EPS_END + (EPS_START - EPS_END) * math.exp(-1. * steps_done / EPS_DECAY)
              if len(losses_1_episode) > 0:
                loss_to_file = losses_1_episode[-1]
              else:
                loss_to_file = 0
              
              # data to log to csv file
              data_to_file_several_lines.append([i_episode,
                                                 steps_done,
                                                 c,
                                                 total_rewards,
                                                 success,
                                                 loss_to_file,
                                                 eps_threshold,
                                                 env.get_scene_sub_folder()])
              total_rewards = 0.0
              success = 0.0
              
              # plot every n_episodes_plot episodes and write data to CSV
              if i_episode % n_episodes_plot == 0 or i_episode == num_episodes-1: 
                for list_of_data in data_to_file_several_lines:
                  scene.write_list_to_txt_file(file_out,list_of_data)
                data_to_file_several_lines = []
                file_out.flush()
                DQN_utils.plot_data(episode_durations,
                                    "Duration of episodes","Episode","Duration",
                                    save_plots,output_path)
                DQN_utils.plot_data(episode_total_rewards,
                                    "Sum of rewards per episode","Episode","Reward",
                                    save_plots,output_path)
                DQN_utils.plot_data(episode_success,"Success per episode","Episode","Success",save_plots,output_path)
              
              if len(losses_1_episode) > 1:
                losses_episodes.append(sum(losses_1_episode) / float(len(losses_1_episode)))
                if i_episode % n_episodes_plot == 0:
                  DQN_utils.plot_data(losses_episodes[1:], "Average loss per episode", "Episode", "Loss",save_plots,output_path) # skip first entry, which is very large
                losses_1_episode = []  
              
              # Save data as a dfataset that can be used for supervised learning
              if save_experience:
                env.save_image_and_labels(scene_no, traj_no, img_no+1, traj_last=1)            
              # next episode
              break
          # within loop for steps: for c in count():   
          img_no += 1
       #within loop for episodes: for i_episode in range(num_episodes):    
      if i_episode % save_intermediate_models_every_x_episodes == 0 and save_intermediate_models:
        print("Saving models")
        torch.save(policy_net.state_dict(),os.path.join(output_path,"policy_net_" + str(GPU_no) + "_" +str(i_episode).zfill(5) + ".pt"))

  file_out.close()
  print("Saving models")
  torch.save(policy_net.state_dict(),os.path.join(output_path,"policy_net_" + str(GPU_no) + "_" +str(i_episode).zfill(5) + ".pt"))
  print('Complete')
  plt.ioff()
 
  env.disconnect_from_sim()

  
if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('--config_file', type=str, default = 'config.ini',
      help='Config file with training parameters etc')
  parser.add_argument('--run_start', type=int, default = None,
      help='(optional) used as the subfolder as a number for the run, if None\
      then dont use subfolders')
  parser.add_argument('--run_n', type=int, default = None,
      help='(optional) specifies how many runs to run,if None\
      then dont use subfolders')
  parser.add_argument('--config_file_list', type=str, default = None,
      help='A text file with a list of config files. Run each config file once. Alternative of running same config_file for different runs.')
  
  args = parser.parse_args()


  config = configparser.RawConfigParser()

  # use a list of config files. Load sim env only once and change config for each call to main  
  if args.config_file_list != None:
    with open(args.config_file_list) as f:
      config_files = [line.rstrip('\n') for line in f]
    config.read(config_files[0])
    use_offline_sim_data = config.getboolean('simulator', 'use_offline_sim_data')
    offline_data_path = config.get('simulator', 'offline_data_path')
    use_multiple_scene = config.getboolean('simulator', 'use_multiple_scene')
    use_pretrain_folder = config.getboolean('training_param', 'use_pretrain_folder')    
    assert(use_multiple_scene == True), "Don't use single scenes anymore"
    assert(use_pretrain_folder == False), "Don't combine use_pretrain_folder and config_file_list"

    # Check that other config files intend to use the same offline data
    for config_file in config_files[1:]:
      config.read(config_file)
      use_offline_sim_data_1 = config.getboolean('simulator', 'use_offline_sim_data')
      offline_data_path_1 = config.get('simulator', 'offline_data_path')
      use_multiple_scene_1 = config.getboolean('simulator', 'use_multiple_scene')
      use_pretrain_folder_1 = config.getboolean('training_param', 'use_pretrain_folder')        
      assert(use_offline_sim_data == use_offline_sim_data_1),\
        "If using config_file_list, different config files should have same entry for these items"
      assert(offline_data_path == offline_data_path_1),\
        "If using config_file_list, different config files should have same entry for these items"      
      assert(use_multiple_scene == use_multiple_scene_1),\
        "If using config_file_list, different config files should have same entry for these items"
      assert(use_pretrain_folder == use_pretrain_folder_1),\
        "If using config_file_list, different config files should have same entry for these items"
  else:
    # Read in offline simulation data only once for 10 runs
    config.read(args.config_file)
    use_offline_sim_data = config.getboolean('simulator', 'use_offline_sim_data')
    offline_data_path = config.get('simulator', 'offline_data_path')
    use_multiple_scene = config.getboolean('simulator', 'use_multiple_scene')
    use_pretrain_folder = config.getboolean('training_param', 'use_pretrain_folder')
  
  #use_pretrain_folder=true: evaluating a folder with several runs, then specify
  if use_pretrain_folder:
    assert(args.run_start != None and args.run_n != None),\
      "If using use_pretrain_folder, then also specify --run_start and --run_n"
  env_list = []  
  if use_offline_sim_data:
    if use_multiple_scene:
      scene_folders = os.listdir(offline_data_path)
      scene_folders.sort()
      print("Numbers of scene folders: ", len(scene_folders))
      for scene_sub_folder in scene_folders:
        if not isinstance(int(scene_sub_folder), int):
          print("Warning: Expecting subfolder with numbers as scenes")
          print("Was offline_data_path specified correctly? (the folder should only contain subfolders as numbers)")
      for scene_sub_folder in scene_folders:    
        print("Loading scene: ", scene_sub_folder)
        if args.config_file_list != None:
          env_tmp = offlineEnv.offlineSimEnv(config_files[0], multiple_scenes=True, scene_sub_folder=scene_sub_folder)
        else:
          env_tmp = offlineEnv.offlineSimEnv(args.config_file, multiple_scenes=True, scene_sub_folder=scene_sub_folder)          
        env_tmp.load_data()
        total_count, upper_count = env_tmp.countValidInitStates()
        print("total_count, upper_count", total_count, upper_count)
        if(upper_count < 750):
          print("#############################################################################################")
          print("WARNING, this scene seems to have only few initial states that have good q-values")
          print("#############################################################################################")
        if(upper_count == 0):
          # I should delete scenes that do not have any initial good values
          input("Press a key")
        env_list.append(env_tmp)
  
  if args.run_start != None and args.run_n != None and args.config_file_list == None:
    for run_no in range(args.run_start, args.run_start + args.run_n):
          print("run_no ", run_no)        
          main(args.config_file, run_no, env_list)
  elif args.config_file_list != None:
    for config_file in config_files:
      print("Running: ", config_file)      
      if args.run_start != None and args.run_n != None:
        for run_no in range(args.run_start, args.run_start + args.run_n):
          print("run_no ", run_no)        
          main(config_file, run_no, env_list)        
      else:
        run_no = 0
        main(config_file, run_no, env_list)
  else: # only a single run (e.g. for a single pretrained network using SL)
    print("training or evaluating a single run only")
    run_no = 0
    main(args.config_file, run_no, env_list=env_list)
