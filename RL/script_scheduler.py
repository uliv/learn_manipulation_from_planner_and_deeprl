#Script that runs other scripts or commands in the terminal one by one according to the entries stored in a text file (as input)

# Run as (example):
#python script_scheduler.py script_schedule.txt script_schedule_output.txt

import os
import sys
import datetime
import subprocess
import time

def get_time_string():
    now = datetime.datetime.now()
    return str(now)#now.strftime("%Y-%m-%d %H:%M")

def print_to_file(fout,string='',data=''):
    line = str(string) + str(data) + '\n'
    fout.write(line) 
    
def update_command_list_from_file(script_schedule_in):
    file_read = False
    while file_read == False:
        try:    
            with open( script_schedule_in, 'r' ) as T :
                lines = T.readlines()
            file_read = True
        except:
            print("could not open file ", script_schedule_in)
            print("trying again in 5 seconds")
            time.sleep(5)
        
    commands = []
    for line in lines:
        if line[0] != '#' and line[0] != ' ' and line[0] != '\n':
            commands.append(line)
    return commands

def run_command_and_log(commands,command_count,fout,script_schedule_out):
    c = commands[command_count].split(' ')
    #print("command_a:", c)
    if c[-1][-1] == '\n': 
        c[-1] = c[-1][:-1] # remove \n from last entry, but only if it is there
    #print("command_b:", c)  
    
    #c = commands[command_count]
    #if c[-1] == '\n': 
    #    c = c[:-1] # remove \n from last entry, but only if it is there

    
    string_out = get_time_string() + ": Running command no "+ str(command_count)  +": " + commands[command_count]
    print_to_file(fout,string_out)
    fout.close()
    fout = open(script_schedule_out, "a")          
    print(sys.argv[0] ,": Running command no "+ str(command_count)  +": " + commands[command_count])
    #p = subprocess.Popen(['/bin/bash', '-c', c])
    #p.wait()
    
    subprocess.call(c)
    return fout     

if __name__ == '__main__':

    script_schedule_in = sys.argv[1] # e.g. script_schedule.txt
    script_schedule_out = sys.argv[2] # e.g. 'script_schedule_output.txt'

    fout = open(script_schedule_out, "w")
    print_to_file(fout,"Started at ", get_time_string())
    fout.close()
    fout = open(script_schedule_out, "a")  

    command_count = 0 # to keep track which command in script_schedule.txt should be executed next (0: first command)    
    
    commands = update_command_list_from_file(script_schedule_in)
    
    # Run commands[command_count], then update commands from list (they might have been updated, e.g. added commands)
    while command_count < len(commands):
        fout = run_command_and_log(commands,command_count,fout,script_schedule_out)
        command_count = command_count + 1
        commands = update_command_list_from_file(script_schedule_in)            
    print("script scheduler finished")     
    print_to_file(fout,"Finished at ", get_time_string())      
    fout.close()
    exit()