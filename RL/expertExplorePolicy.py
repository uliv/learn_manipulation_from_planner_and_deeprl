#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 09-30-2019

Classes for expertExplore or init policies

@author: uli
"""

import torch
import utils.vrep_utils as utils

class expertExplore(object):
  '''
  Parent class for different expert explore policies
  '''
  def __init__(self):
    pass
  
  def get_action_for_state(self, state):
    print("This should be implemented in child class")
    raise NotImplementedError

# some initial policy classes:
class policy_init_move_down(expertExplore):
  '''
    A simple initial policy (always moves down)
    Either used for "expert explore" or for "init policy" of Residual Policy Learning
  '''
  def __init__(self, device):
      self.device = device

  def get_action_for_state(self, state):
    action = [0,0,-0.1]
    action_index, _ = utils.actionToActionIndex(action)
    return torch.tensor([[action_index]], device=self.device, dtype=torch.long)

class policy_init_net(expertExplore):
  '''
    Initial policy as a pretrained net
    Either used for "expert explore" or for "init policy" of Residual Policy Learning
  '''
  def __init__(self, net):
      self.net = net

  def get_action_for_state(self, state):
    with torch.no_grad():
      # t.max(1) will return largest column value of each row.
      # second column on max result is index of where max element was
      # found, so we pick action with the larger expected reward.
      #print("state", state) # this is state tensor([[[[0.1466, 0.1466, 0.1466,  ..., 0.1459, 0.1459, 0.1459],
      #print("policy_net(state)", policy_net(state))
      return self.net(state).max(1)[1].view(1, 1)  

class policy_init_planner(expertExplore):
  '''
    Initial policy as the expert planner (having full state information)
    Either used for "expert explore" or for "init policy" of Residual Policy Learning
  '''
  def __init__(self,env,device):
    self.env = env
    self.device = device
    
  def update_env(self,env):
    self.env = env

  def get_action_for_state(self, state):
    q_values, best_action, best_index = self.env.getLabels() # best_index starts at 1
    return torch.tensor([[best_index-1]], device=self.device, dtype=torch.long)
