#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 09-30-2019

Classes for replay buffers used in DQN.py
 - regular replay buffer (uniformly sampled, no priorization)
 - priotized replay buffer (priority based on TD error)
@author: uli
"""

import numpy as np
import random
import torch

class ReplayMemory(object):
  '''
  Simple experience replay memory for training the DQN. It stores
  the transitions that the agent observes, allowing us to reuse this data
  later. By sampling from it randomly, the transitions that build up a
  batch are decorrelated. It has been shown that this greatly stabilizes
  and improves the DQN training procedure.
  
  ReplayMemory: A cyclic buffer of bounded size that holds the
    transitions observed recently. It also implements a ``.sample()``
    method for selecting a random batch of transitions for training. 
  
  Transition - a named tuple representing a single transition in
    the environment. It maps essentially maps (state, action) pairs
    to their (next_state, reward) result, with the state being the
    depth_image.
  
  '''

  def __init__(self, capacity, Transition):
      self.capacity = capacity
      self.memory = []
      self.position = 0
      self.Transition = Transition

  def push(self, *args):
      """Saves a transition."""
      if len(self.memory) < self.capacity:
          self.memory.append(None)
      self.memory[self.position] = self.Transition(*args)
      self.position = (self.position + 1) % self.capacity

  def sample(self, batch_size):
      return random.sample(self.memory, batch_size)

  def __len__(self):
      return len(self.memory)


# Todo: Put this into seperate file
class PrioritizedReplayMemory(object):
  '''
    Like ReplayMemory, but uses priotized experience replay.
    This way "interesting" data is sampled more often.
    This class is NOT compatible yet with the "transition" used for
    ReplayMemory.
  '''
  def __init__(self, capacity, Transition, device,
               alpha=0.6, beta_start=0.4, beta_frames=10000):
      self.alpha = alpha
      self.capacity = capacity
      self.buffer = []
      self.priorities = np.zeros((capacity,), dtype=np.float32)
      self.frame = 1
      self.beta_start = beta_start
      self.beta_frames = beta_frames
      self.next_idx = 0
      self.eps = 1e-5
      self.Transition = Transition
      #self.MIN_DATA_BEFORE_LEARNING = MIN_DATA_BEFORE_LEARNING
      #self._max_prio = 1.0
      self.device = device

  def beta_by_frame(self, frame_idx):
      return min(1.0, self.beta_start + frame_idx * (1.0 - self.beta_start) / self.beta_frames)

  def push(self, *args):
      if len(self.buffer) < self.capacity:
          self.buffer.append(None)
      self.buffer[self.next_idx] = self.Transition(*args)

      self.priorities[self.next_idx] = 1.0**self.alpha
      self.next_idx = (self.next_idx + 1) % self.capacity      

  def sample(self, batch_size):
      total = len(self.buffer)
      prios = self.priorities[:total]
      probs = prios / prios.sum()

      indices = np.random.choice(total, batch_size, p=probs)
      samples = [self.buffer[idx] for idx in indices]

      beta = self.beta_by_frame(self.frame)
      self.frame += 1

      # Min of ALL probs, not just sampled probs
      prob_min = probs.min()
      max_weight = ((prob_min*total)) ** (-beta)

      weights = (total * probs[indices]) ** (-beta)
      weights /= max_weight
      weights = torch.tensor(weights).float().to(self.device)
      #torch.tensor(weights, device=self.device, dtype=torch.float)

      return samples, indices, weights

  def update(self, batch_indices, batch_errors):
      assert len(batch_indices) == len(batch_errors)
      for idx, error in zip(batch_indices, batch_errors):
          prio = (np.abs(error) + self.eps) ** self.alpha
          self.priorities[idx] = prio

  def __len__(self):
      return len(self.buffer)

def main():
  # Some basic testing/demo
  pass
  
if __name__ == '__main__':
    main()    
  
  