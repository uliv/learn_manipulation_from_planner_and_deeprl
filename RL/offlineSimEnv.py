#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 27 15:02:02 2019

This stores all the state information of a scene to disk (images with labels).
This then can be loaded and be used in the offlineSimulator for RL.

@author: uli
"""

import sys
import time
import numpy as np
import matplotlib.pyplot as plt
import os
import configparser
import pandas as pd
import random

sys.path.append('./')
sys.path.append('..')
sys.path.append('../simulation')
sys.path.append('../scripts')

import scene_gen_utils as scene

import PNG_16bit as PNG

class state(object):
  def __init__(self,image, collision, distToObstacles, pos_target_in_robot,
               best_action, best_index, q_values, q_values_flat,
               NumPixelsVisibleTarget, NumPixelsVisibleObj,
               grid_x, grid_y, grid_z):  
    self.image = image
    self.collision = collision
    self.distToObstacles = distToObstacles
    self.pos_target_in_robot = pos_target_in_robot
    self.best_action = best_action
    self.best_index = best_index
    self.q_values = q_values
    self.q_values_flat = q_values_flat
    self.NumPixelsVisibleTarget = NumPixelsVisibleTarget
    self.NumPixelsVisibleObj = NumPixelsVisibleObj
    self.grid_x = grid_x
    self.grid_y = grid_y
    self.grid_z = grid_z

    

    
class offlineSimEnv(object):
  def __init__(self,config_file,multiple_scenes=False, scene_sub_folder=''):
    self.config_planner = scene.import_config_planner(config_file)
    config = configparser.RawConfigParser()
    config.read(config_file)

    self.multiple_scenes = multiple_scenes
    self.scene_sub_folder = scene_sub_folder

    self.offline_data_path = config.get('simulator', 'offline_data_path')
    if self.multiple_scenes:
      self.offline_data_path = os.path.join(self.offline_data_path,self.scene_sub_folder)
    
    self.csv_file_general = os.path.join(self.offline_data_path,"labels/info_general.csv")    
    self.csv_file_scenes = os.path.join(self.offline_data_path,"labels/info_scenes.csv")
    self.csv_file_images = os.path.join(self.offline_data_path,"labels/info_images.csv")
    self.img_folder = os.path.join(self.offline_data_path,"images/")
    self.timeout_reward = config.getfloat('training_param', 'timeout_reward') 
    self.maxDistance = None # read this from dataset maxDistance must be > distNoPenalty! 
    self.distNoPenalty = config.getfloat('path_planner', 'distNoPenalty') 
    self.distPenaltyFactor = config.getfloat('path_planner', 'distPenaltyFactor') 
    self.valid_init_states_all = 0
    self.valid_init_states_high_z = 0    
    self.states = None # maybe use a 3d array of object "state"
    self.current_state_idx3d = np.asarray([0,0,0],dtype=int)
    try:
      self.init_state_with_good_q_only = config.getboolean('simulator', 'init_state_with_good_q_only') 
    except:
      print("Warning init_state_with_good_q_only not specified, using default False.")
      self.init_state_with_good_q_only = False
    
    # save_experience related
    self.dataset_path_images = None
    self.file_images = None
  
  def get_scene_sub_folder(self):
    return self.scene_sub_folder

  def get_current_state_idx3d(self):
    return self.current_state_idx3d
  
  def load_data(self):
    # read and process info from info_general.csv
    info_general = pd.read_csv(self.csv_file_general)
    values_x = info_general.iloc[0,1]
    values_y = info_general.iloc[0,2]
    values_z = info_general.iloc[0,3]
    self.maxDistance = info_general.iloc[0,4]
    print("maxDistance in dataset", self.maxDistance)
    if self.maxDistance > 0 and self.maxDistance < self.distNoPenalty:
      print("distNoPenalty", self.distNoPenalty)
      print("WARNING, maxDistance < distNoPenalty")
      print("This will lead to wrong calculation of distance penalty because the distance values are not reliable (e.g. not all measured)")      
    self.values_x = np.asarray(values_x.split(' ')).astype('float')
    self.values_y = np.asarray(values_y.split(' ')).astype('float') 
    self.values_z = np.asarray(values_z.split(' ')).astype('float')
    
    self.dim_x = len(self.values_x)
    self.dim_y = len(self.values_y)
    self.dim_z = len(self.values_z)    

    # also load the array of states and fill the objects with data       
    # create 3 dimential array to store all the state information
    self.states = np.asarray([[[None for _ in range(len(self.values_z))] for _ in range(len(self.values_y)) ] for _ in range(len(self.values_x)) ] , dtype=state)    
    info_images = pd.read_csv(self.csv_file_images) 
    for idx in range(len(info_images)):

      ## image
      img_name = os.path.join(self.img_folder,
                                info_images.iloc[idx, 0])
      image = PNG.read_png16bit(img_name) #reads 16bit PNG that was written using write_png16bit. Values will be in [0,1) 
      # swap color axis because
      # numpy image: H x W x C
      # torch image: C X H X W
      #image = image[..., np.newaxis]
      #image = image.transpose((2, 0, 1))
      #image = np.ascontiguousarray(image, dtype=np.float32) 
      
      distToObstacles = info_images.iloc[idx, 12]

      collision = 1 if distToObstacles < 0.0005 else 0

      pos_target_in_robot = info_images.iloc[idx, 7]
      pos_target_in_robot = np.asarray(pos_target_in_robot.split(' '))
      
      best_action = info_images.iloc[idx, 6]
      best_action = np.asarray(best_action.split(' '))
      best_action = best_action.astype('float') # order: {x,y,z]
      
      best_index = info_images.iloc[idx, 5] # This index start with 1-27
      #best_index = np.asarray(info_images.iloc[idx, 5]-1) # best_index, index from 0-26 (-1 because LUA in v-rep index starts from 1)
      
      q_values = info_images.iloc[idx, 4] # q-values
      q_values_flat = np.asarray(q_values.split(' ')).astype('float')      
      q_values = q_values_flat.reshape(-1, 3, 3) #don't reshape, keep flat as outputs of neural net for q-values and actions
      
      #if(np.logical_or(q_values < 0.0,q_values >= 10.0).all()
      #   and collision == 0):
      #  print("q_values", q_values)
      #  print("collision", collision)
      #  print("distToObstacles", distToObstacles)
      #  input("Press a key")

      # probably need to use vrep_utils.ConvertQvalues values here
      # No I do it on the RL side, maybe fix?
      
      NumPixelsVisibleTarget = info_images.iloc[idx, 9]
      NumPixelsVisibleObj = info_images.iloc[idx, 10]
      
      # grid coodinates this state belongs to
      grid_x = int(info_images.iloc[idx, 13])
      grid_y = int(info_images.iloc[idx, 14])
      grid_z = int(info_images.iloc[idx, 15])
          
      new_state = state(image,
                        collision,
                        distToObstacles,
                        pos_target_in_robot,
                        best_action,
                        best_index,
                        q_values,
                        q_values_flat,
                        NumPixelsVisibleTarget,
                        NumPixelsVisibleObj,
                        grid_x, grid_y, grid_z)
      
      self.states[grid_x, grid_y, grid_z] = new_state
  
    #for x in range(self.dim_x):
    #  for y in range(self.dim_y):
    #    for z in range(self.dim_z):
    #      print(self.states[x,y,z].pos_target_in_robot)
    
    #Count states that could be used as initial states:
    
    self.valid_init_states_all, self.valid_init_states_high_z =  self.countValidInitStates()

  def countValidInitStates(self):
    '''
      Need to make sure that there are few initial states that are:
        - collision free
        - target object is visible (>= 16 pixels)
        - obj in gripper is visible (>= 16 pixels)
      Or otherwise it would be difficult to determine how to move to reach the goal.  
    '''
    valid_init_states_all = 0
    valid_init_states_high_z = 0
    for x in range(self.dim_x):
      for y in range(self.dim_y):
        for z in range(self.dim_z):
          state = self.states[x,y,z]
          
          if self.init_state_with_good_q_only:
            no_good_q = self.no_good_q(state.q_values_flat)
          else:
            no_good_q = False  
          

#          if no_good_q:
#            print("no_good_q", no_good_q)
#            print("state.q_values_flat", state.q_values_flat)            
#            input(" No good q")
#            print("state.collision, NumPixelsVisibleTarget, state.NumPixelsVisibleObj", state.collision, state.NumPixelsVisibleTarget, state.NumPixelsVisibleObj)
            
          if state.collision == 0 and state.NumPixelsVisibleTarget >= 16 and state.NumPixelsVisibleObj >= 16 and not no_good_q:
            valid_init_states_all += 1
            if z >= int(self.dim_z/2):
              valid_init_states_high_z += 1
    
    return valid_init_states_all, valid_init_states_high_z
            
    
    
  def get_grid_dims(self):
    return self.dim_x, self.dim_y, self.dim_z
  
  def get_grid_deltas(self):
    return self.values_x[1]-self.values_x[0], self.values_y[1]-self.values_y[0], self.values_z[1]-self.values_z[0]

  ################
  # Helper functions
  ################
  
  def no_good_q(self,q_values):
    q_values = np.asarray(q_values, dtype=np.float32)
    #self.timeout_reward is specified with - sign, but q_values here are positive (before ConvertQvalues)
    # see equivalent col_oob_value that is positive.
    return not((q_values >= 0.0) & (q_values < -self.timeout_reward)).any()
  
  def action2indexDelta(self, action):
    '''
    Input: an action (x,y,z offset in meters as a list), e.g. [-0.01,0,0,01]
    Output: array of index offsets (ints)  (e.g. [-1,0,1])
    '''    
    index_delta_list = []
    for a in action:
      if a < -0.0001:
        d = -1
      elif a > 0.0001:
        d = 1
      else:
        d = 0
      index_delta_list.append(d)
    return np.asarray(index_delta_list, dtype=int)

  def checkInBound(self, state_idx3d):
    if np.any(np.asarray(state_idx3d, dtype=int) < 0) or np.any(state_idx3d >= np.asarray([self.dim_x, self.dim_y, self.dim_z], dtype=int)):
      return False
    else:
      return True

  def set_state(self, state_idx3d):
    if not self.checkInBound(state_idx3d):
      print("set_state: new state out of bound")
      return False
    else:
      self.current_state_idx3d = np.asarray(state_idx3d,dtype=int)
      return True

    
  def moveCost(self,action,current_state_idx3d,next_state_idx3d):
    '''
      Calculate the movement cost for an action.
      The cost is moving the gripper by distance of "action" plus a penalty
      if moving too close to obstacles.
      
      Inputs:
        - action (list): x,y,z displacement og gripper
        - current_state_idx3d: index of current state (needed for distance and collision query)
        - next_state_idx3d: index of next state (needed for distance and collision query)
        
      Outputs:
        - move_cost: move movement cost plus penalty
        - collision2: 1 if next state is on collision, 0 if not in collision
    '''
    state1 = self.states[current_state_idx3d[0],current_state_idx3d[1],current_state_idx3d[2]]
    state2 = self.states[next_state_idx3d[0],next_state_idx3d[1],next_state_idx3d[2]]
    distance1 = state1.distToObstacles
    distance2 = state2.distToObstacles
    #print("distance1, distance2", distance1, distance2)
    
    collision2 = state2.collision

    cost_penalty1 = max(self.distNoPenalty-distance1,0)
    cost_penalty2 = max(self.distNoPenalty-distance2,0)
    
    #print("cost_penalty1, cost_penalty2", cost_penalty1, cost_penalty2)

    # Cost of moving without penalty
    move1_cost = -np.linalg.norm(np.asarray(action))
    
    move_cost = move1_cost * (1 + self.distPenaltyFactor*(cost_penalty1+cost_penalty2)/2)
    
    return move_cost, collision2
  
  #######################
  # RL relatedfunctions (called by RL code)      
  #######################  
  def init_state(self,high_z_only=True):
    if high_z_only: 
      if(self.valid_init_states_high_z == 0):
        print("valid_init_states_high_z is ", self.valid_init_states_high_z )
        return False
      min_z_i = int(len(self.values_z)/2)
    else:
      if(self.valid_init_states_all == 0):
        print("valid_init_states_all ", self.valid_init_states_all )
        return False
      min_z_i = 0

    x = random.choice(range(self.dim_x))
    y = random.choice(range(self.dim_y))
    z = random.choice(range(min_z_i,self.dim_z))      
    state = self.states[x, y, z]
    is_invalid = not(state.collision == 0 and state.NumPixelsVisibleTarget >= 16 and state.NumPixelsVisibleObj >= 16)
    if self.init_state_with_good_q_only:
      no_good_q = self.no_good_q(state.q_values_flat)

    else:
      no_good_q = False
      
    #if no_good_q:
    #  print("initial state has no good q-value: ", state.q_values_flat)
    #  input("Press a key")
    
    while is_invalid or no_good_q:     
      x = random.choice(range(self.dim_x))
      y = random.choice(range(self.dim_y))
      z = random.choice(range(min_z_i,self.dim_z))
      state = self.states[x, y, z]
      is_invalid = not(state.collision == 0 and state.NumPixelsVisibleTarget >= 16 and state.NumPixelsVisibleObj >= 16)
      if self.init_state_with_good_q_only:
        no_good_q = self.no_good_q(state.q_values_flat)
      else:
        no_good_q = False
      
    self.set_state([x,y,z])
    
    return True # if no valid init state found

  def init_state_systematic(self,x,y,z):
    self.set_state([x,y,z])

  def step(self,action):
    # find index of next state based on action
    index_delta = self.action2indexDelta(action)
    next_state_idx3d = self.current_state_idx3d + index_delta
    #print("next_state_idx3d", next_state_idx3d)
    # check for out of bound
    if not self.checkInBound(next_state_idx3d):
      #print("new state out of bound")
      reward = self.timeout_reward
      done = 1
      return reward, done
    # calculate rewards based on distance penalty movement cost etc    
    move_cost, collision = self.moveCost(action,
                                         self.current_state_idx3d,
                                         next_state_idx3d)   
    if collision == 1:
      reward = self.timeout_reward
      done = 1
      
      # Update state (but new state won't be used to make more predictions, because already "done"):
      self.current_state_idx3d = next_state_idx3d    
    else:
      # Update state:
      self.current_state_idx3d = next_state_idx3d
      reward = move_cost
      state = self.states[self.current_state_idx3d[0],self.current_state_idx3d[1],self.current_state_idx3d[2]]
      dist_to_goal = np.linalg.norm(state.pos_target_in_robot)
      if dist_to_goal < 0.005:
        print("Target within 0.005")
        done = 1
      else:
        done = 0

    return reward, done

  def getObservations(self):
    ''' Returns depth images as numpy (depths inverted an scaled to 0-1)
    '''
    return self.states[self.current_state_idx3d[0],self.current_state_idx3d[1],self.current_state_idx3d[2] ].image
  
  def getLabels(self):
    ''' Returns:
          - q_values (list): value of each action, these are "uncorrected values (wrong signs, need to use )
          - best_action (list): best action
          - best_index (int): index of the best action (the argmax of q_values2)
              best_index2 starts with 1
    '''
    
    q_values = self.states[self.current_state_idx3d[0],self.current_state_idx3d[1],self.current_state_idx3d[2] ].q_values_flat
    best_action = self.states[self.current_state_idx3d[0],self.current_state_idx3d[1],self.current_state_idx3d[2] ].best_action
    best_index = self.states[self.current_state_idx3d[0],self.current_state_idx3d[1],self.current_state_idx3d[2] ].best_index
    return q_values, best_action, best_index
  
  def disconnect_from_sim(self): # dummy call to make offineSimEnv class backward
                                 # compatible to vrepEnv class 
    pass

  #######################
  # Save experience related code (for creating a dataset for SL)     
  ####################### 

  def set_dataset_path_images(self, dataset_path_images):
    self.dataset_path_images = dataset_path_images
    
  def set_file_images(self, file_images):
    self.file_images = file_images
    
  def save_image_and_labels(self, scene_no, traj_no, img_no, traj_last=0):
    grid_x = self.current_state_idx3d[0]
    grid_y = self.current_state_idx3d[1]
    grid_z = self.current_state_idx3d[2]
    state = self.states[grid_x, grid_y, grid_z]
    
    depth_data = state.image # 0.5 meters=black, 0 meters=white

    # Save image and label
    scene_no_str = str(scene_no).zfill(6)
    traj_no_str = str(traj_no).zfill(6)
    img_no_str = str(img_no).zfill(6)
    #image_file_name_old = 'img_' + scene_no_str + '_' + traj_no_str + '_' + img_no_str +'_8bit.png'  
    #plt.imsave(dataset_path_images + image_file_name_old, depth_data,cmap=plt.cm.gray, vmin=0, vmax=1)
    
    # save as 16 bit
    image_file_name = 'img_' + scene_no_str + '_' + traj_no_str + '_' + img_no_str +'.png'
    PNG.write_png16bit(depth_data, self.dataset_path_images + image_file_name)
    
    q_values = list(state.q_values_flat)
    best_action = list(state.best_action)
    pos_target_in_robot = list(state.pos_target_in_robot)
    orient_target_in_robot = list([0,0,0])
    best_index = state.best_index
 
    NumPixelsVisibleTarget = int(state.NumPixelsVisibleTarget)
    NumPixelsVisibleObj = int(state.NumPixelsVisibleObj)
    distToObstacles = state.distToObstacles
       
    # reduced number label info to file
    data_to_file = [image_file_name,scene_no,traj_no,img_no,q_values,best_index,best_action,
                    pos_target_in_robot,orient_target_in_robot,NumPixelsVisibleTarget,
                    NumPixelsVisibleObj,traj_last,distToObstacles,
                    grid_x, grid_y, grid_z]
    scene.write_list_of_lists_to_txt_file(self.file_images,data_to_file,round_digits=5)  
        
  
def main():
  # Some testing/demo
  config_file = 'config_offlineSimEnv.ini'
  env = offlineSimEnv(config_file)
  env.load_data()
  image = env.getObservations()
  plt.imshow(image)
  plt.show()
  # check out of bound for too small x
  action = [-0.01,0,0]
  reward, done = env.step(action)
  print("reward, done", reward, done)

  print("check in bound (false?)", env.checkInBound([4,5,4])) # should be false
  print("check in bound (false?)",env.checkInBound([-1,0,4])) # should be false
  print("check in bound (true?)",env.checkInBound([1,0,4])) # should be false  
  # check out of bound for too large y
  env.set_state([4,4,4])
  action = [0,0.01,0]
  reward, done = env.step(action) # should report out of bound
  print("reward, done", reward, done)
  
  
  total_count, upper_count = env.countValidInitStates()
  
  print("total_count, upper_count", total_count, upper_count)
  env.set_state([0,0,2])

  # move along diaginal in state space
  action = [0.01,0.01,0.0]
  for i in range(5):
    reward, done = env.step(action)
    print("reward, done", reward, done)
    image = env.getObservations()
    plt.imshow(image)
    plt.show()
    input("press a key")
  
  print("Init with high z")
  env.init_state(high_z_only=True)
  image = env.getObservations()
  plt.imshow(image)
  plt.show()  
  
if __name__ == '__main__':
    main()    
  
  