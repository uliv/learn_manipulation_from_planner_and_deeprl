# This config file specifies user or system specific parameters, which usually is different from machine to machine.
# This allows the script to find files on the machine or to customize important parameters in the scripts to the individual usage.
# Please copy this file to "config.ini" and adjust the parameters as it applies to your system
# This config file also specifies all the parameters for the experiment runs.


## Main parameters used by several scripts 
[main]
# The installation directory of v-rep
vrep_root = /home/uli/Software/V-REP

# The location of the python scripts
git_location = /home/uli/learn_manipulation_from_planner_and_deepRL/
  
# Script specific parameters
[logging]
# Path to write results and backup files
output_path = /home/uli/learn_manipulation_from_planner_and_deepRL/training_results/DQFD_PER_example/

# should the intermediate models be saved?
save_intermediate_models = true

# how often (after how many episodes) should the model be saved
save_intermediate_models_every_x_episodes = 500

# specifies if training data should be logged and plotted
save_plots = true

# how often to plot intermediate results
n_episodes_plot = 100

# save experience (dataset) for supervised learning (SL)
# create subfolder "images" and "labels" with corresponding files (e.g. png files, label_images.csv) that can be used for SL training
save_experience = false


[device]
# if GPU to use, if false then use CPU
use_GPU = true

# Which GPU to use (only used if use_GPU = true)
GPU_no = 1

[simulator]

# With fixed seed, the scene is always the same from run to run (i.e. peg size/position and hole size)
# This only aplies if use_offline_sim_data = False (V-REP simulator is used)
use_fixed_seed = true

# If clutter should be created or not
# This only aplies if use_offline_sim_data = False (V-REP simulator is used)
create_clutter = true

# Which port to use for v-rep: 19997+sim_port_offset
# This only aplies if use_offline_sim_data = False (V-REP simulator is used)
sim_port_offset = 0

# If the GUI/visualization in V-rep is disable, then it runs faster
# But this is not the same as headless (headless doesn't seem to work when creating shapes)
# This only aplies if use_offline_sim_data = False (V-REP simulator is used)
GUI_enabled = false

# if use_offline_sim_data, the use data from offline_data_path with class offlineSimEnv, is used in DQN.py
# In this case the V-REP simulator is not used, instead a dataset that contains all necessary information for all possible states all informations, like collisions, transition function etc. This data was generated previously by V-REP using DQN_save_GT.py.
# The motivation is that running V-REP is much slower than loading a complete set of all possible states into a data structure, where transitions, getting obervations etc. can be done very fast (loading from memory).
# with use_offline_sim_data = true, then all above parameters are ignored:
# use_fixed_seed
# create_clutter
# sim_port_offset
# GUI_enabled
use_offline_sim_data = true

# how many actions are available at each state
# On a 3x3x3 grid around the current possition there are 27 actions possible (diagonal allowed)
NUM_ACTIONS = 27

# for loading the data for the "offline simulator" (pre-generated states with all informations, like collisions, transition function etc).
offline_data_path = /home/uli/learn_manipulation_from_planner_and_deepRL/datasets/10-04-19_03_pegHole_train_2_scenes
# offline_data_path = /home/uli/research/thesis/datasets/RL_datasets/10-04-19_01_pegHole_500_scenes_train/
# offline_data_path = /home/uli/research/thesis/datasets/RL_datasets/10-04-19_02_pegHole_100_scenes_eval/
# offline_data_path = /home/uli/research/thesis/datasets/RL_datasets/10-04-19_02_pegHole_100_scenes_test/

# Using offline sim data (use_offline_sim_data=True) allows to switch scene quickly during R/L training
# use_multiple_scene = true, this will use all scenes available in offline_data_path
# can only br used with use_offline_sim_data = true (not with V-REP simulator)
use_multiple_scene = true

# At the beginning of each episode, the state is initialized such that (by default):
# 1. the obj in gripper is visible
# 2. the target object is visible
# 3. not in collision
# The parameter below specifies that only initial staes should be allowed, where moving to the target is cheaper than timeout or collision.
init_state_with_good_q_only = true

[training_param]

 # if true, then nothing is learned (LR is set to 0)
EVAL_ONLY = false

# specifies if a model should be finetuned (if the wights should be initialized from a net that was trained before, e.g. with a different/simpler approach, e.g. AGGREVATE).
use_pretrain_model = true

# Path to the model used to load initial weights
pre_trained_model = /home/uli/learn_manipulation_from_planner_and_deepRL/pretrained_models/BESTQ.pt

# this is used to eval many runs within one script
# if there are multiple runs of the same experiment, then do eval for each "run" individually
# if use_pretrain_folder = true, then check folder pre_train_folder for subfolders as "0", "1" (for the runs)
# if there are models for different episodes (see also save_intermediate_models_every_x_episodes), then eval first for
# save_intermediate_models_every_x_episodes episodes, then next one for next save_intermediate_models_every_x_episodes episodes
# until final episode (see num_episodes).
use_pretrain_folder = false
pre_train_folder = none

# Used only if loading a net at pre_trained_model (or nets at pre_train_folder) that was trained as policy (classification, not regression)
# Need to scale the parameters of last FC layer if using a policy net (to match with magnitude of values of the q-net)
# This is a trick default is 1.0 (don't scale), but if a policy was learned, then SCALE_FC3 = 0.01 (in the order of 0.01) is
# appropriate. In that case the -neg log proabilities translate to the range of the q-values [-1.0,0].
SCALE_FC3 = 1.0

######:  Expert explore
# similar to paper "Residual Policy Learning"
# With some probability choose the action from an expert
use_expert_explore = true

# available initial policies (policy_init_str) as the expert
# 1. move_down
#    (a very simple policy, which could help to reach the target more often by chance, because the intitial height of the object in gripper 
#    is always at least 5 cm)
# 2. net (a pretrained-model)
# 3. planner (the full state planner)
policy_init_str = planner

# the pretrained model if using "net" above
expert_model = none

# probability of using the expert action (vs the random action)
# the portion of eps actions (e.g. if sample < EPS, see parameter "EPS") is divided to: (1-expert_prob) random and expert_prob expert action.
# if if sample > EPS: then the greedy action based on the current policy is selected.
expert_prob = 0.8
# END: Expert explore
###################################

# Training batch size
BATCH_SIZE = 64

# Initial learning rate of optimizer (Adam)
LR = 0.00001

# Fill the buffer with at least this ammount of samples before learning (calculating a loss and backprop to update weights)
MIN_DATA_BEFORE_LEARNING = 2000

# reward discount factor
# 1.0 because for simplicity, because otherwise I would need to adjust the SL training labels to reflect different GAMMA.
# Could also try lower GAMMA (e.g. for training DQN from scratch or finetuning etc).
GAMMA = 1.0

# EPS is epsilon for e-greey action selection
# EPSILON is calculates as
# EPSILON = EPS_END + (EPS_START-EPS_END) * exp(-episode/EPS_DECAY)
EPS_START = 1.0
EPS_END = 0.0
EPS_DECAY = 5000

# STEPS_TIMEOUT: How many steps are allowed before terminating the episodes with rewards of TIMEOUT_REWARD
STEPS_TIMEOUT = 30

# Make sure this corresponds to the value returned by the simulator for collision or out-of-bound (see "execute_action" in sim file used to run v-rep)
TIMEOUT_REWARD = -1.0

# How many episodes to train
num_episodes = 50000

#how many optimization update steps to perform after each simulation step
N_OPTIMIZER_STEPS = 1

# rate for soft update of target net.
# Target net is updated at every optimization step (usually same as sim step if N_OPTIMIZER_STEPS = 1) 
TAU = 1e-4 

# Size of the experience replay buffer
REPLAY_BUFFER_SIZE = 50000

use_PER_memory = true

####### Idea: Generate training examples using planner. This generated full episodes (not just single transitions) by a planner.
# Not used anymore, see use_expert_explore with "planner".
# initial probability for planned trajectory:
P_TRAJ_START = 0.0
# usually 0.4
# Stop executing planned traj after x episodes:
PLANNED_TRAJ_MAX_EPISODE = 0
#0 no planner used, usually 2000


####### Thesis method 4: Use planner to calculate q-value of next state in q-target calculation, not used anymore
PLANNER_FOR_NEXT_Q = false


####
# possible baselines:
# DAGGER (learn a policy by imitating the planner as expert)
# AGGREVATE (learn the q-function)
# BESTQ (learn the q function with margin for suboptimal actions)
# DQFD (use TD error + large margin classification loss for expert transitions)
# NONE
BASELINE = DQFD 


[BESTQ]
# parameters for baseline BESTQ
# This affects how ground truth q-values from planner are used for supervised learning targets 
# set all values to -col_oob_value, except for the best q-value
learn_best_q_value_only = false
# only used if learn_best_q_value_only = true
# best_q_value_margin = true: use fixed margin to set all non-optimal action by a margin lower than best q value
# best_q_value_margin = false: use constant col_oob_value (e.g. -0.5) to set all non-optimal actions
best_q_value_margin = false
# margin, only used if learn_best_q_value_only = true

[BESTQ_DQFD]
# usually 0.2
q_margin = 0.2
DQFD_w = 0.1


[image_processing]
# if adding noise to the observation
add_noise = false
noise_prob_min = 0.0
noise_prob_max = 0.0


[path_planner]
# Following only applicable is use_offline_sim_data = false (V_REP is used)

# path planner (grid related)
# how many steps in the grid in each axis
dim_x = 21
dim_y = 21
dim_z = 12
dim_t = 1

# if collision should be checked
checkCol = 1
# if visibility should be checked
checkVis = 1
# if distance to obstacles should be measured
checkDist = 1
# if collision should have a high cost (e.g. -10.0)
# if use_collision_cost_for_planner = 0, then only "distance penalty parameters" (soft penalty) is used.
use_collision_cost_for_planner = 1

# the min and max values of the grid relative to the target.
bound_target_x_low = -0.1
bound_target_x_high = 0.1
bound_target_y_low = -0.1
bound_target_y_high = 0.1
bound_target_z_low = -0.01
bound_target_z_high = 0.1

# using defaultvalues, see utils.sampleValidStatesOnGrid
# distance penalty parameters
maxDistance = 0.0201
distNoPenalty = 0.02
distPenaltyFactor = 100






