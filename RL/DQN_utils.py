#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Aug 12 16:11:53 2019

@author: uli
"""

import numpy as np
import torch
import torch.nn.functional as F
import matplotlib.pyplot as plt
import utils.vrep_utils as utils
import os
import matplotlib
import scene_gen_utils as scene


def depthImageToTensor(image,device):
  ''' Convert numpy image to Tensor.
  
  '''
    # swap color axis because
  # numpy image: H x W x C
  # torch image: C X H X W
  image = image[..., np.newaxis]
  image = image.transpose((2, 0, 1))
  image = np.ascontiguousarray(image, dtype=np.float32) 
  image = torch.from_numpy(image)
  # add a batch dimension (BCHW)
  return image.unsqueeze(0).to(device)    # Resize, and add a batch dimension (BCHW)

# col_oob_value has to be positive here
def qvaluesToTensor(q_values,col_oob_value,device,
                    learn_best_q_value_only=False,
                    best_q_value_margin=False,
                    q_margin=0.0):
  q_values = np.array(q_values) #these q-values are not all with correct sign
  q_values, only_bad_q = utils.ConvertQvalues(q_values, col_oob_value, learn_best_q_value_only,
                                  best_q_value_margin, q_margin)

  q_values = np.ascontiguousarray(q_values, dtype=np.float32)   
  q_values = torch.from_numpy(q_values)
  return q_values.unsqueeze(0).to(device), only_bad_q    # Resize, and add a batch dimension (BCHW)






def plot_data(data_list,title,x_label,y_label,save_to_file=False,output_path=''):
    ''' Plots data (also moving average of 100 data points)
    Inputs:
      - data_list (list): list of data to be plotted
      - title (string): The title for the plot
      - x_label (string): Label for x-axis      
      - y_label (string): Label for y-axis  
      - save_to_file (bool, optional): If true, then save plot to disk using output_path
      - output_path (string, optional): output path for plot so be saved
    '''
    # set up matplotlib
    is_ipython = 'inline' in matplotlib.get_backend()
    if is_ipython:
      from IPython import display    
    plt.figure(2)
    plt.clf()
    data_t = torch.tensor(data_list, dtype=torch.float)
    plt.title(title)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.plot(data_t.numpy())
    # Take 100 episode averages and plot them too
    if len(data_t) >= 100:
        means = data_t.unfold(0, 100, 1).mean(1).view(-1)
        means = torch.cat((torch.zeros(99), means))
        plt.plot(means.numpy())
    if save_to_file:
      if not os.path.exists(output_path):
        print("Warning: can't plot to file because ", output_path, " does not exist")
      else:
        plt.savefig(os.path.join(output_path,'plot_' + y_label +'.png'))
    if is_ipython:
        display.clear_output(wait=True)
        display.display(plt.gcf())

def create_logging_files(output_path, git_location, config_file):
  '''
    Setup logging files for relevant info for DQN learning
    Also backup script files.
  '''

  if(os.path.exists(output_path) == False): #check if directory exist
      os.makedirs(output_path)
  else:
    print("Warning: Folder already exists", output_path)
    input("Press a key to continue")
  
  
  file_plot = open(os.path.join(output_path,"plot.csv"), "a")    
  #list_of_lists = [epochs,loss_train[-1],acc_train[-1],loss_test[-1],acc_test[-1],loss_test2[-1],acc_test2[-1]]
  headers_plot = ["episode", "steps_done", "durations", "rewards", "success","loss","epsilon","scene_folder"]
  scene.write_list_to_txt_file(file_plot,headers_plot)  
  file_plot.flush()

  
  # copy main scripts and config files (as backup)
  file_main_script = os.path.join(git_location,"RL/DQN.py")    
  scene_gen_utils = os.path.join(git_location,"scripts/scene_gen_utils.py")
  config_file = os.path.join(git_location,"RL/", config_file)
  training_file = os.path.join(git_location,"supervised_learning/training.py")
  nets_file = os.path.join(git_location,"nets/nets.py")  

  files_to_copy = [file_main_script,scene_gen_utils,config_file,training_file,nets_file]
  scene.copy_files_to_path(files_to_copy,output_path)  
  return file_plot
        

def large_margin_classification_loss_vector(current_Q,
                                           q_margin,                                     
                                           is_expert_mask,
                                           action_batch,
                                           BATCH_SIZE,
                                           n_actions,
                                           device,
                                           current_a_expert_batch,                                     
                                           current_q_expert_batch,
                                           debug=False):
    ''' L = max_over_a[Q(s,a) + margin(a_e,a)] - Q(s,a_e)
        a_e axpert action
        margin(a_e,a) = pos constant for a != a_e
        margin(a_e,a_e) = 0
        
        Returns: loss_vector_margin_classification
                 A vector for loss_margin_classification
                 for each sample in the batch
    '''

    margin = torch.zeros(BATCH_SIZE, n_actions, device=device)
    #all_ones = torch.ones(BATCH_SIZE, 27, device=device)
    #is_expert_mask applied to batch (dim 0)
  
    # position of expert action is 0, otherwise all 1
    if debug:
      print("is_expert_mask", is_expert_mask)
      print("current_a_expert_batch", current_a_expert_batch)
      print("action_batch", action_batch)
    #print("action_batch.shape", action_batch.shape)        
    expert_one_hot = F.one_hot(action_batch.squeeze(), n_actions).bool()
    #expert_one_hot = make_one_hot(action_batch.squeeze(), 27, device)
    #print("expert_one_hot", expert_one_hot)
    #print("expert_one_hot[is_expert_mask]", expert_one_hot[is_expert_mask])
    #print("~expert_one_hot[is_expert_mask])", ~(expert_one_hot[is_expert_mask]))
    #print("q_margin", q_margin)
    margin = q_margin * (~expert_one_hot).float()
    if debug:    
      print("margin", margin)
      print("current_Q + margin", current_Q + margin)
    current_Q_max_w_margin = (current_Q + margin).max(1)[0]
    if debug:    
      print("current_Q_max_w_margin", current_Q_max_w_margin) #[0] to get the values, [1] would be the indeces
      print("current_Q", current_Q)
    #print("current_Q.shape",current_Q.shape)
  
    #print("(current_Q.gather(1, action_batch).squeeze(1)",
    #        current_Q.gather(1, action_batch).squeeze(1))
    current_Q_expert = current_Q.gather(1, action_batch).squeeze(1)
    if debug:    
      print("current_Q_expert", current_Q_expert)
  
    #margin[is_expert_mask] = q_margin*torch.ones(BATCH_SIZE, 27, device=device)
    loss_vector_margin_classification = torch.zeros(BATCH_SIZE, device=device)

    loss_vector_margin_classification[is_expert_mask] =\
        current_Q_max_w_margin[is_expert_mask] - current_Q_expert[is_expert_mask]
    if debug:
      print("loss_vector_margin_classification", loss_vector_margin_classification)
    loss_vector_margin_classification[loss_vector_margin_classification < 0] = 0
    #if((loss_vector_margin_classification == 0.0) & is_expert_mask).any():
    if( not
       (current_a_expert_batch[(loss_vector_margin_classification  == 0.0) & is_expert_mask]\
            == action_batch[(loss_vector_margin_classification  == 0.0) & is_expert_mask]).all()):

      print("(loss_vector_margin_classification == 0.0) & is_expert_mask)",
           (loss_vector_margin_classification == 0.0) & is_expert_mask)
      print("is_expert_mask", is_expert_mask)
      print("current_a_expert_batch", current_a_expert_batch)
      print("action_batch", action_batch)
      print("the following should be the same (choosen actions should be expert actions)")
      print("current_a_expert_batch[(loss_vector_margin_classification == 0.0) & is_expert_mask]",
                                    current_a_expert_batch[(loss_vector_margin_classification  == 0.0) & is_expert_mask])
      print("action_batch[(loss_vector_margin_classification == 0.0) & is_expert_mask]",
                                    action_batch[(loss_vector_margin_classification  == 0.0) & is_expert_mask])
      
      print("loss_vector_margin_classification", loss_vector_margin_classification)      
      print("action in batch should match expert action")

      assert((current_a_expert_batch[(loss_vector_margin_classification  == 0.0) & is_expert_mask]\
            == action_batch[(loss_vector_margin_classification  == 0.0) & is_expert_mask]).all()),\
            "action in batch do not match expert action, but margin loss is 0!! (should not happen)"

    return loss_vector_margin_classification

def optimize_model(optimizer,policy_net,target_net,memory,BATCH_SIZE,GAMMA,
                   Transition,device,TAU,use_PER_memory,PLANNER_FOR_NEXT_Q=False,
                   BASELINE='NONE',q_margin=0.0,DQFD_w=1.0):
    '''
    calculate the loss depending on the method use (e.g. DAGGER, DQN etc)
    use the loss to optimize the model
    '''
    if len(memory) < BATCH_SIZE:
      raise RuntimeError('In optimize_model: len(memory) < BATCH_SIZE should not happen here (checked outside of optimize_model)')
      return
    
    if use_PER_memory:
      transitions, idxs, is_weights = memory.sample(BATCH_SIZE)
      is_weights = is_weights.unsqueeze(0) # shape from [64] to [1,64]
    else:
      transitions = memory.sample(BATCH_SIZE)
    
    
    # Transpose the batch (see https://stackoverflow.com/a/19343/3343043 for
    # detailed explanation). This converts batch-array of Transitions
    # to Transition of batch-arrays.
    batch = Transition(*zip(*transitions))

    # Compute a mask of non-final states and concatenate the batch elements
    # (a final state would've been the one after which simulation ended)
    non_final_mask = torch.tensor(tuple(map(lambda s: s is not None,
                                          #batch.next_state)), device=device, dtype=torch.uint8)  
                                          batch.next_state)), device=device, dtype=torch.bool)
    non_final_next_states = torch.cat([s for s in batch.next_state
                                                if s is not None])
    is_expert_mask = torch.tensor(tuple(map(lambda e: e is True,
                                          #batch.isExpert)), device=device, dtype=torch.uint8)
                                          batch.isExpert)), device=device, dtype=torch.bool)
    #is_not_expert_mask = torch.tensor(tuple(map(lambda e: e is False,
    #                                      batch.isExpert)), device=device, dtype=torch.bool)
    state_batch = torch.cat(batch.state)
    action_batch = torch.cat(batch.action)
    reward_batch = torch.cat(batch.reward)
    current_q_expert_batch = torch.cat(batch.current_q_expert) # q-value labels from planner
    current_a_expert_batch = torch.cat(batch.current_a_expert) # action labels from planner

    n_actions = current_q_expert_batch.shape[1] # 27 actions

    # Compute Q(s_t, a) - the model computes Q(s_t), then we select the
    # columns of actions taken. These are the actions which would've been taken
    # for each batch state according to policy_net
    if BASELINE == 'DAGGER':
      current_A = policy_net(state_batch) # action probs (as log_softmax)
    elif BASELINE == 'AGGREVATE' or BASELINE == 'BESTQ':
      current_Q = policy_net(state_batch)
    else: # DQN and BASELINE 'DQFD'
      current_Q = policy_net(state_batch) # all Q values
      current_Q_0 = current_Q.gather(1, action_batch)
    #print("current_Q_0",current_Q_0) 


    # Compute the expected Q values (target_Q_0)
   # print('next_Q_0', next_Q_0)
    if PLANNER_FOR_NEXT_Q:
      next_Q_batch = torch.cat(batch.next_Q)      
      
      target_Q_0 = (next_Q_batch.max(1)[0].detach() * GAMMA) + reward_batch
    # DAGGER and AGGREVATE don't need to calculate target_q values (got values from planner)
    elif BASELINE == 'DAGGER':      
      pass # don't do anything here
    elif BASELINE == 'AGGREVATE' or BASELINE == 'BESTQ':
      pass # don't do anything here
    else: # DQN and BASELINE 'DQFD'
      # Double DQN
      # target_Q(s,a) = reward + gamma * Q_target_net(s',argmax_a Q_using_policy_net(s',a))

      online_Qs_0 = policy_net(non_final_next_states) 
      online_action_indices_0 = online_Qs_0.max(1)[1].detach().unsqueeze(1)
      #print("online_action_indices_0", online_action_indices_0)
      target_Qs_0 = target_net(non_final_next_states)
      #print("target_Q_0", target_Q_0)
      # Compute V(s_{t+1}) for all next states.
      # Expected values of actions for non_final_next_states are computed based
      # on the "older" target_net; selecting their best reward with max(1)[0].
      # This is merged based on the mask, such that we'll have either the expected
      # state value or 0 in case the state was final.
      next_Q_0 = torch.zeros(BATCH_SIZE, device=device)
      #next_Q_0_old = torch.zeros(BATCH_SIZE, device=device)
      # Uli:
      # The "max" in next_Q_0_old makes it not "Double DQN", replace max with action from policy net for that state
      # choose "best action" from policy net, then use this action to calculate value using target net
      next_Q_0[non_final_mask] = target_Qs_0.gather(1, online_action_indices_0).squeeze(1)
      #print("next_Q_0 (double)", next_Q_0)
      #next_Q_0_old[non_final_mask] = target_net(non_final_next_states).max(1)[0].detach()
      #print("next_Q_0_old not_Double DQN",next_Q_0_old)      
      target_Q_0 = ((next_Q_0 * GAMMA) + reward_batch).unsqueeze(1)
          

    # update priority, calculate loss
    if use_PER_memory: # try PrioritizedReplayMemory
      if BASELINE == 'DAGGER': # classification neg-log-likelihood loss
        #Can't use the following, because current_A are log-probabilities (floats)
        # and current_a_expert_batch are class labels (integers)
        #errors = torch.abs(current_A - current_a_expert_batch).detach().cpu()
        # Instead use the loss (without any reduction to keep the vecor of BATCH_SIZE,
        # and without the weights)

        # using losses as error        
        loss_vector = F.nll_loss(current_A, current_a_expert_batch.squeeze(1), reduction = 'none')
        errors = loss_vector.detach().cpu()
        loss_vector = loss_vector.unsqueeze(1) # make (64,1)
      elif BASELINE == 'AGGREVATE' or BASELINE == 'BESTQ':
        errors = torch.sum(torch.abs(current_Q - current_q_expert_batch).detach().cpu(),dim=1).squeeze()
        loss_vector = F.smooth_l1_loss(current_Q, current_q_expert_batch, reduction = 'none')
        # sum over action dim to get same shape as in other cases
        # (and later do weighted sum for all samples in batch)
        loss_vector = torch.sum(loss_vector,dim=1).unsqueeze(1) # shape 64,1      
      else: # TD loss (DQN)
        errors = torch.abs(current_Q_0 - target_Q_0).detach().cpu()        
        loss_vector = F.smooth_l1_loss(current_Q_0, target_Q_0, reduction = 'none')
        # how is this different? This seems to work (PER has better performance with expertexplore vs no PER)
        # loss =  F.smooth_l1_loss(is_weights*current_Q_0, is_weights*target_Q_0) 

      loss = torch.mm(is_weights,loss_vector).squeeze() # sum to convert 1x1 tensor to scalar (but still with grad)
      
      if BASELINE == 'DQFD': # large margin clasification loss
        loss_vector_margin_classification = large_margin_classification_loss_vector(
                                                           current_Q,
                                                           q_margin,
                                                           is_expert_mask,
                                                           action_batch,
                                                           BATCH_SIZE,
                                                           n_actions,
                                                           device,
                                                           current_a_expert_batch,
                                                           current_q_expert_batch)

        loss_margin_classification = torch.mm(is_weights,loss_vector_margin_classification.unsqueeze(1)).squeeze()
        loss = loss + DQFD_w*loss_margin_classification  
      
      memory.update(idxs, errors)
    else: # regular replay buffer (no priorities, weights)    
      if BASELINE == 'DAGGER':     
        loss = F.nll_loss(current_A, current_a_expert_batch.squeeze(1), reduction = 'sum')
      elif BASELINE == 'AGGREVATE' or BASELINE == 'BESTQ':
        loss = F.smooth_l1_loss(current_Q, current_q_expert_batch, reduction = 'sum')
      else: # TD error loss (DQN)
        if BASELINE == 'DQFD':
          print("DQFD with unifrom random buffer not implemented")
          raise NotImplementedError
        # Compute Huber loss
        loss = F.smooth_l1_loss(current_Q_0, target_Q_0, reduction = 'sum')
   
    # Optimize the model
    optimizer.zero_grad()
    loss.backward()
    for param in policy_net.parameters():
        param.grad.data.clamp_(-1, 1)
    optimizer.step()
    
    # update the target network (soft update)
    if not PLANNER_FOR_NEXT_Q:
      soft_update(policy_net,target_net, TAU)
    
    return loss.item()


def soft_update(policy_net,target_net, tau):
    """Soft update model parameters.
    θ_target = τ*θ_local + (1 - τ)*θ_target
    Params
    ======
        policy_net (PyTorch model): weights will be copied from
        target_net (PyTorch model): weights will be copied to
        tau (float): interpolation parameter 
    """
    for target_param, policy_param in zip(target_net.parameters(), policy_net.parameters()):
        target_param.data.copy_(tau*policy_param.data + (1.0-tau)*target_param.data)

  

def test_index_action_conversions():
  index = 7
  delta_x = 0.01
  delta_y = 0.02
  delta_z = 0.03
  action = utils.actionIndexToAction(index,delta_x,delta_y,delta_z)
  
  print("action", action)
  assert(action == [0.0,0.02,-0.03])
  
  action_index, action_indices = utils.actionToActionIndex(action)  
  
  print("action_index", action_index)
  print("action_indices", action_indices)
  assert(action_index == 7)
  assert(action_indices == (0,2,1))  