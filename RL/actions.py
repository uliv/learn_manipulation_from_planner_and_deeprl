#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 09-30-2019

Module for action selection functions

@author: uli
"""

import torch
import random


### action selection functions:
    # policy_init: an object for the expertExplore
def select_action_expert_explore(state,policy_net,policy_init,device,eps_threshold,expert_prob,NUM_ACTIONS,debug=False):
    sample = random.random()
    # expert_prob: if 1 then only take expert actions, no random actions
    #        if 0 then only take random actions, no expert actions
    #eps_threshold = EPS_END + (EPS_START - EPS_END) * \
    #    math.exp(-1. * steps_done / EPS_DECAY)
    if debug:
      print("eps_threshold", eps_threshold)
    isExpert = False
    if sample < eps_threshold*expert_prob: # expert
      isExpert = True
      return policy_init.get_action_for_state(state), isExpert
    elif sample < eps_threshold*(1-expert_prob): # random
      return torch.tensor([[random.randrange(NUM_ACTIONS)]], device=device, dtype=torch.long), isExpert
    else: # policy
      with torch.no_grad():
        # t.max(1) will return largest column value of each row.
        # second column on max result is index of where max element was
        # found, so we pick action with the larger expected reward.
        #print("state", state) # this is state tensor([[[[0.1466, 0.1466, 0.1466,  ..., 0.1459, 0.1459, 0.1459],
        #print("policy_net(state)", policy_net(state))
        return policy_net(state).max(1)[1].view(1, 1), isExpert

# Returns of torch tensor for action index in 0..26
def select_action_egreedy(state,policy_net,device,eps_threshold,NUM_ACTIONS,debug=False):
    sample = random.random()

    if debug:
      print("eps_threshold", eps_threshold)
    isExpert = False # never expert in this function
    if sample > eps_threshold:
        with torch.no_grad():
            # t.max(1) will return largest column value of each row.
            # second column on max result is index of where max element was
            # found, so we pick action with the larger expected reward.
            #print("state", state) # this is state tensor([[[[0.1466, 0.1466, 0.1466,  ..., 0.1459, 0.1459, 0.1459],
            #print("policy_net(state)", policy_net(state))
            return policy_net(state).max(1)[1].view(1, 1), isExpert
    else:
      return torch.tensor([[random.randrange(NUM_ACTIONS)]], device=device, dtype=torch.long), isExpert
      '''
        sample = random.random()
        if sample > eps_threshold:
          return policy_net_smart.classify(state)
        else:
          return torch.tensor([[random.randrange(NUM_ACTIONS)]], device=device, dtype=torch.long)
      '''

def select_action_planner(env,device):
  q_values, best_action, best_index = env.getLabels() # best_index starts at 1
  isExpert = True
  return torch.tensor([[best_index-1]], device=device, dtype=torch.long), isExpert

#def select_action_smart(state,policy_net_smart):
#  return policy_net_smart.classify(state)
  

def select_random_action(device,NUM_ACTIONS):
  isExpert = True  
  return torch.tensor([[random.randrange(NUM_ACTIONS)]], device=device, dtype=torch.long), isExpert
