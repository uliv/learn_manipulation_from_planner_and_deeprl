#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 15 12:14:11 2019

Example and test to save and restore numpy array to/from 16-bit PNG

@author: uli
"""

import png

import numpy as np

import cProfile

def write_png16bit(array,file_name):
  array = (65535*array).astype(np.uint16)  
  # Use pypng to write array as a grayscale PNG.
  with open(file_name, 'wb') as f:
      writer = png.Writer(width=array.shape[1], height=array.shape[0], bitdepth=16, greyscale=True)
      writer.write(f, array.tolist())  
  
def read_png16bit(file_name):
  r=png.Reader(file_name)
  row_count, column_count, pngdata, meta = r.asDirect()
  bitdepth = meta["bitdepth"]
  plane_count = meta["planes"]
  # Make sure we're dealing with grayscale files with 16 bits
  assert bitdepth == 16
  assert plane_count == 1
  
  array = np.vstack(map(np.uint16, pngdata))  
  array = array.astype(np.float32)/65535.0
  return array

# test and demo
def main():
  
  # Make an image in a numpy array for this demonstration.
  nrows = 4000  
  ncols = 4000
  np.random.seed(12345)
  x = np.random.rand(nrows, ncols) # random numbers in [0, 1)
  x = x.astype(np.float32)
  print('x', x)
  
  
  
  # Convert y to 16 bit unsigned integers.
  #z = (65535*((y - y.min())/y.ptp())).astype(np.uint16)
  y = (65535*x).astype(np.uint16)
  print('y', y)
  
  # Here's a grayscale example.
  #zgray = z[:, :, 0]
  
  # Use pypng to write z as a grayscale PNG.
  with open('foo_gray.png', 'wb') as f:
      writer = png.Writer(width=y.shape[1], height=y.shape[0], bitdepth=16, greyscale=True)
      y2list = y.tolist()
      writer.write(f, y2list)
  
    
  r=png.Reader('foo_gray.png')
  # Tuple unpacking, using multiple assignment, is
  # very useful for the result of asDirect (and other methods).
  row_count, column_count, pngdata, meta = r.asDirect()
  bitdepth = meta["bitdepth"]
  plane_count = meta["planes"]
  # Make sure we're dealing with grayscale files with 16 bits
  assert bitdepth == 16
  assert plane_count == 1
  
  image_2d = np.vstack(map(np.uint16, pngdata))
  
  print('image_2d', image_2d)
  
  print("image_2d equals y? ", np.all(image_2d == y))
  
  x_in = image_2d.astype(np.float32)/65535.0
  
  print('x_in', x_in)
  
  print("image_2d/65535 equals x? ", np.all(x_in == x))
  
  print("max error between x_in and x ", np.max(np.abs(x_in - x)))
  
  # Error for 8 bits: around 0.0039
  # Error for 16 bits: around 1.53e-05
  
  write_png16bit(x,'another.png')
  test = read_png16bit('another.png')
  print('test', test)
  print("max error between x and test ", np.max(np.abs(x - test)))

  

if __name__ == '__main__':
  main()