import os
import subprocess
import argparse

# E.g. start as:
# python start_vrep_simulators.py /home/uli/vrep_ur5/simulation/scenes/vrep_scene_ur5_v27_peg_insertion_merged25b_26.ttt 1 --port 19997


#VREP_DIR = '/home/uli/Software/V-REP_V3_6_1_rev3' #V-REP'
VREP_DIR = '/home/uli/Software/V-REP' # 3.5.0'
#VREP_DIR = '/home/ur5/Documents/Uli/V-REP_PRO_EDU_V3_5_0_Linux'

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('simulation', type=str,
      help='The path to the simulation to load on startup')
  parser.add_argument('num_simulators', type=int,
      help='Number of V-Rep simulators to start')
  parser.add_argument('--h', default=False, action='store_true',
      help='Use this flag to run V-Rep in headless mode')
  parser.add_argument('--port', type=int, default=19997,
      help='The port to launch the simulator with. If using mulitple simulators this will be the first port set.')

  args = parser.parse_args()

  ports = range(args.port, args.port+args.num_simulators)
  simulation = os.path.abspath(args.simulation)
  if args.h:
    commands = ['./vrep.sh {} -gREMOTEAPISERVERSERVICE_{}_FALSE_FALSE -h'.format(simulation, port) for port in ports]
  else:
    commands = ['./vrep.sh {}  -gREMOTEAPISERVERSERVICE_{}_FALSE_FALSE'.format(simulation, port) for port in ports]
    print("commands", commands)    
    # prints:
    # commands ['./vrep.sh /home/uli/vrep_ur5/simulation/scenes/vrep_scene_ur5_v25_peg_insertion_distance_calc.ttt  -gREMOTEAPISERVERSERVICE_19997_FALSE_FALSE']
    # ['./vrep.sh /home/uli/vrep_ur5/simulation/scenes/vrep_scene_ur5_v25_peg_insertion_distance_calc.ttt  -gREMOTEAPISERVERSERVICE_19998_FALSE_FALSE']



  os.chdir(VREP_DIR)
  for process in [subprocess.Popen(['/bin/bash', '-c', command]) for command in commands]:
    out, err = process.communicate()
