#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 17 15:13:01 2019

@author: uli
"""

import torch
import torch.nn as nn
import torch.nn.functional as F

# N is batch size; D_in is input dimension;
# H is hidden dimension; D_out is output dimension.
N, D_in, H, D_out = 1, 2, 2, 2

# Create random Tensors to hold inputs and outputs
#x = torch.randn(N, D_in)
x = torch.tensor([[1.,2.]])
print('x', x)
#y = torch.randn(N, D_out)
y = torch.tensor([[1.,2.]])

print('y', y)

class net(nn.Module):
    def __init__(self,D_in,H,D_out):
        super(net, self).__init__()      
        self.fc1 = nn.Linear(D_in, H) # 2 conv with pool
        self.fc2 = nn.Linear(H, D_out) # 2 conv with pool

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = self.fc2(x)        
        return x

device = torch.device("cpu")
model = net(D_in,H,D_out).to(device)

# Use the nn package to define our model and loss function.
#model = torch.nn.Sequential(
#    torch.nn.Linear(D_in, H),
#    torch.nn.ReLU(),
#    torch.nn.Linear(H, D_out),
#)
loss_fn = torch.nn.MSELoss(reduction='sum')

# Use the optim package to define an Optimizer that will update the weights of
# the model for us. Here we will use Adam; the optim package contains many other
# optimization algoriths. The first argument to the Adam constructor tells the
# optimizer which Tensors it should update.
learning_rate = 1e-2
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
print("model.parameters()", model.parameters())
for name, param in model.named_parameters():
  print("name", name)  
  print("param", param)
  
for t in range(50):
    # Forward pass: compute predicted y by passing x to the model.
    y_pred = model(x)
    
    print("############# y_pred", y_pred)

    # Compute and print loss.
    loss = loss_fn(y_pred[:,0], y[:,0])
    print(t, loss.item())

    # Before the backward pass, use the optimizer object to zero all of the
    # gradients for the variables it will update (which are the learnable
    # weights of the model). This is because by default, gradients are
    # accumulated in buffers( i.e, not overwritten) whenever .backward()
    # is called. Checkout docs of torch.autograd.backward for more details.
    optimizer.zero_grad()

    # Backward pass: compute gradient of the loss with respect to model
    # parameters
    loss.backward()
    print('loss.grad', loss.grad)
    
    for name, param in model.named_parameters():
      print("name", name)  
      print("param", param)
      print("param.grad", param.grad)
      print("")
    # Calling the step function on an Optimizer makes an update to its
    # parameters
    optimizer.step()
    #input("Press a key")