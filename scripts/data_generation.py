# before running this, start simulator like this:
# python start_vrep_simulators.py /home/uli/vrep_ur5/simulation/scenes/vrep_scene_ur5_v11_states_sample_on_grid.ttt 1

# this is cleaned version of test_ur5_peg_insertion7_python3.py

import sys
import time
import numpy as np
import matplotlib.pyplot as plt
import os
import argparse
import configparser

sys.path.append('./')
sys.path.append('..')
sys.path.append('../simulation')
from simulation import vrep

from grippers.rg2 import RG2
import utils.vrep_utils as utils
import scene_gen_utils as scene
VREP_BLOCKING = vrep.simx_opmode_blocking

  
def main(sim_port_offset=None):
  # Attempt to connect to simulator
  if sim_port_offset==None:
    sim_port = 19997
  else:
    sim_port = 19997+sim_port_offset
  print("connecting to sim_port", sim_port)
  sim_client = utils.connectToSimulation('127.0.0.1', sim_port)    
  # Create RG2 gripper and restart simulator
  gripper = RG2(sim_client)
  sensor_d = utils.VisionSensor(sim_client, 'CAM_SR300_D',z_near=0.01, z_far=0.5)
  #ur5 = UR5(sim_client, gripper)


  ###########################################
  # Setup files for logging, parameters etc #
  ###########################################
  
  config_file = 'config.ini'  
  config_planner = scene.import_config_planner(config_file)

  
  config = configparser.RawConfigParser()
  config.read(config_file)   
  git_location = config.get('main', 'git_location')  
  n_scenes = int(config.get('data_generation', 'n_scenes'))  
  n_traj_per_scene = int(config.get('data_generation', 'n_traj_per_scene'))
  sample_trajectories = bool(int(config.get('data_generation', 'sample_trajectories')))
  dataset_path_output = config.get('data_generation', 'dataset_path_output')
  print("dataset_path_output", dataset_path_output)
  
  
  if sim_port_offset != None:
    dataset_path_output = os.path.join(dataset_path_output,str(sim_port_offset))
  file_general,file_scenes,file_images,dataset_path_images,dataset_path_labels\
        = scene.create_logging_files(dataset_path_output,git_location)

  
  #####################
  ### setup (scene) ###
  #####################
  

  scene_no = 0

  while scene_no < n_scenes:
    print("scene_no: ", scene_no)
    t0_scene = time.time()
    utils.restartSimulation(sim_client)
    utils.GUI_enabled(sim_client,False)
    
    #  IMPORT PLANE, PEG AND scale     
    plane_obj,plane_pos,plane_name,peg_obj,peg_name = scene.generate_env_for_peg_hole(sim_client,gripper,scene_no,file_scenes,git_location)

    ret,plane_obj_handle = utils.getObjectHandle(sim_client,plane_name + '_handle')
    #ret,T_global_plane = utils.getObjectPose(sim_client, plane_obj_handle)
    ret,peg_obj_handle = utils.getObjectHandle(sim_client,peg_name + '_handle')
    T_global_peg_init = utils.getObjectMatrix(sim_client, peg_name + '_handle')
    
    
    # Create Clutter
    ignore_occlusions = True
    scene.clutter_creation(sim_client,plane_name,peg_name,ignore_occlusions)
    
    # need to move gripper and camera in the hierarchy under parent "peg" so that if the peg is moved to a desired location, camera and gripper follow
    gripper_parent, cam_parent = scene.move_gripper_and_camera_under_parent_peg(sim_client,peg_name,plane_name)
    
    # Planning: Check collisions on grid, find trajectories to goal
    planning_handle = scene.planning_trajectories_on_grid(sim_client,peg_name,plane_name,config_planner)  
    if scene_no == 0:
      # Check if correct dist parameters has been used.
      maxDistance, distNoPenalty, distPenaltyFactor =  utils.getDistPenaltyParams(sim_client, planning_handle)
      print("config_planner['checkDist']", config_planner['checkDist'])
      print("dist penalty params. maxDistance, distNoPenalty, distPenaltyFactor: ", maxDistance, distNoPenalty, distPenaltyFactor)      

    utils.findAllPathsToGoal(sim_client,planning_handle)
    
    values_x,values_y,values_z = utils.getGridAxesValues(sim_client,planning_handle)
    dim_x = len(values_x)
    dim_y = len(values_y)
    dim_z = len(values_z)

    
    # collision Grid in shape of x,y,z dimensions
    grid_col = scene.getCollisionGrid(sim_client,planning_handle,dim_x,dim_y,dim_z,dim_t=1)
    grid_vis_target = scene.getVisibilityGridTarget(sim_client,planning_handle,dim_x,dim_y,dim_z,dim_t=1)
    grid_vis_obj = scene.getVisibilityGridObj(sim_client,planning_handle,dim_x,dim_y,dim_z,dim_t=1)   
    grid_dist, maxDistance = scene.getDistanceGrid(sim_client,planning_handle,dim_x,dim_y,dim_z,dim_t=1) 
    #print("grid_vis_obj", grid_vis_obj)    
    grid_visited = scene.gridVisitedArray(grid_col,grid_vis_target,grid_vis_obj)

    if(scene_no == 0):
      data_to_file = [plane_pos,values_x,values_y,values_z,maxDistance]
      scene.write_list_of_lists_to_txt_file(file_general,data_to_file,round_digits=False)
      file_general.close()

    #depthCamera = 'CAM_SR300_D'
    #depthCameraVisibility = 'CAM_SR300_visibility_check_man_obj'
    #pixels_visible = utils.checkVisibility(sim_client,planning_handle,depthCamera,depthCameraVisibility,pixelDeltaThreshold=0.001)
    #print("pixels_visible", pixels_visible)

    ############################################################### 
    ###       Data collection/interaction with the environment    #
    ###############################################################  
  
    traj_no = 0
    img_no = 0
  
    while traj_no < n_traj_per_scene:
      print("traj_no", traj_no)
      num_traj_to_time = 10
      if traj_no % num_traj_to_time  == 0:
        t0_trajectory = time.time()
          

      if img_no == 0: # initial state needs to be sampled
        # Sample an initial state that is not in collision and at least 16 pixels visible           
        result = scene.sample_visible_and_collision_free_state(sim_client,peg_obj_handle,grid_col,grid_vis_target,grid_vis_obj,values_x,values_y,values_z,grid_visited)

        if result == False:
          print("exit for loop AND create new clutter")
          break
        
        p_global_peg,collision,NumPixelsVisible_target,NumPixelsVisible_obj,grid_visited = result
        # Save image and labels
        #print("img_no", img_no)        
        scene.save_image_and_labels(sim_client, sensor_d,\
                      scene_no, traj_no, img_no,\
                      dataset_path_images,\
                      planning_handle, peg_name, plane_name,\
                      file_images,
                      grid_vis_target,
                      grid_vis_obj,
                      grid_dist,                     
                      p_global_peg,
                      values_x, values_y, values_z,
                      grid_x=0, grid_y=0, grid_z=0 # 0 are dummies, not needed here                      
                      )
        img_no += 1       
        # Get trajectory to goal
        #print("p_global_peg", p_global_peg)
        #path, path_length, path_cost = utils.queryPathAndCost(sim_client, planning_handle, p_global_peg)
        #utils.visualizePath(sim_client, path, [0,0,255])

        #print("path", path)
        #print("path_length", path_length)
        #print("path_cost", path_cost)
        

        #input("Press a key")
      if sample_trajectories and img_no >= 1: # next states are along trajectory to goal until terminal state (goal reached)
        # Get trajectory to goal
        #print("p_global_peg", p_global_peg)
        #path, path_length, path_cost = utils.queryPathAndCost(sim_client, planning_handle, p_global_peg)
        #utils.visualizePath(sim_client, path, [0,0,255])
        
        path, path_length = utils.queryPathPositionsAsArray(sim_client, planning_handle, p_global_peg)
        #print("path", path)
        #print("path_length", path_length)

        if utils.check_goal_reached(p_global_peg,path[-1]) == 1: # Initial position is goal
          # Save image and labels (with flag that it is goal/last image in trajectory)
          print("Init state is goal state")
        else: # goal not reached, follow trajectory
          for i,position in enumerate(path[0:]):
            utils.setObjectPosition(sim_client,peg_obj_handle, position)
            #print("img_no", img_no)            
            scene.save_image_and_labels(sim_client, sensor_d,\
                          scene_no, traj_no, img_no,\
                          dataset_path_images,\
                          planning_handle, peg_name, plane_name,\
                          file_images,
                          grid_vis_target,
                          grid_vis_obj, 
                          grid_dist,
                          position,
                          values_x, values_y, values_z,
                          grid_x=0, grid_y=0, grid_z=0 # 0 are dummies, not needed here                          
                          )
            img_no += 1
            #input("press a key")
      # next trajectory
      print("img_no", img_no)       
      img_no = 0
      traj_no += 1

      if traj_no % num_traj_to_time == 0:
        t1_trajectory = time.time()
        print("Time required for ",num_traj_to_time, " trajectories: ", t1_trajectory-t0_trajectory)

    
    ###############  
    ### Cleanup ###
    ###############
    
   
    utils.destroyPlanningTask(sim_client,planning_handle)  
    
    scene_no +=1
    t1_scene = time.time()
    print("Time required for a scene: ", t1_scene-t0_scene)
    
    
  ##########################
  # End create scene       #
  ##########################


  file_scenes.close()
  file_images.close()
  
  vrep.simxStopSimulation(sim_client, VREP_BLOCKING)

  vrep.simxFinish(sim_client)
  print ('Program ended')


if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('--sim_port_offset', type=int, default = 0,
      help='The simulators index starting with 0. Used for port increments.')
  args = parser.parse_args()      
  main(args.sim_port_offset)
