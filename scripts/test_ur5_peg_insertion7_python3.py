# before running this, start simulator like this:
# python start_vrep_simulators.py /home/uli/vrep_ur5/simulation/scenes/vrep_scene_ur5_v11_states_sample_on_grid.ttt 1
# used to calibrate camera position

import sys
import time
import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
import random
import math
from shutil import copyfile
import os
import time
import argparse

sys.path.append('./')
sys.path.append('..')
sys.path.append('../simulation')
from simulation import vrep
from robots.ur5 import UR5
#from grippers.robotiq85 import robotiq85
from grippers.rg2 import RG2
import utils.vrep_utils as utils
from utils import transformations
#from timeit import default_timer as timer
import clutter.clutter as cl
import configparser

config = configparser.RawConfigParser()
config.read('config.ini')  # please copy config_example.ini to config.ini and adjust the paths/parameters in this file


dim_x_global = int(config.get('path_planner', 'dim_x'))
dim_y_global = int(config.get('path_planner', 'dim_y'))
dim_z_global = int(config.get('path_planner', 'dim_z'))
dim_t_global = int(config.get('path_planner', 'dim_t'))
checkCol_global = int(config.get('path_planner', 'checkCol'))
checkVis_global = int(config.get('path_planner', 'checkVis'))
checkDist_global = int(config.get('path_planner', 'checkDist'))
bound_target_x_low = float(config.get('path_planner', 'bound_target_x_low'))
bound_target_x_high = float(config.get('path_planner', 'bound_target_x_high'))
bound_target_y_low = float(config.get('path_planner', 'bound_target_y_low'))
bound_target_y_high = float(config.get('path_planner', 'bound_target_y_high'))
bound_target_z_low = float(config.get('path_planner', 'bound_target_z_low'))
bound_target_z_high = float(config.get('path_planner', 'bound_target_z_high'))

dataset_path_output = config.get('data_generation', 'dataset_path_output')
print("dataset_path_output", dataset_path_output)

git_location = config.get('main', 'git_location')

n_scenes = int(config.get('data_generation', 'n_scenes'))

n_traj_per_scene = int(config.get('data_generation', 'n_traj_per_scene'))

#n_images_per_scene = int(config.get('data_generation', 'n_images_per_scene'))


VREP_BLOCKING = vrep.simx_opmode_blocking
CLOSE = 0
OPEN = 1

def test_gripper(gripper):
  # actuate the gripper
  gripper.closeGripper()
  time.sleep(2)  

  gripper.openGripper()
  time.sleep(2) 
  


def test_create_primitive_shape(sim_client):
  # Generate a cube
  position = [0., 0.5, 0.15]
  orientation = [0, 0, 0]
  size = [0.2, 0.2, 0.2]
  mass = 0.1
  color = [255, 0, 0]
  pos_ref=None
  cube = utils.generateShape(sim_client, 'cube', 'cuboid', size, position, orientation, pos_ref, mass, color)
  print("cube", cube)
  time.sleep(2)      
  
def test_import_mesh(sim_client):  
  # Import a mesh
  position = [0.0, 0.3, 0.1]
  orientation = [0, 0, 0] # values in radians, e.g. np.pi/2
  #color = [0, 255, 0]
  cap = utils.importShape(sim_client, 'cap', '/home/uli/vrep_ur5/simulation/objects/002_cap_coke.stl',
                          position, orientation, frame_ref='Clutter',
                          #color=color,
                          collection = 'Clutter_objects', parent='Clutter', use_dummy_as_handle=1)
  time.sleep(2) 

def import_plane_with_hole(sim_client,name):  
  # Import the plane
  position = [0.0, 0.5, 0.1]
  orientation = [0, 0, 0] # values in radians, e.g. np.pi/2
  color = [255, 255, 0]
  result = utils.importShape(sim_client, name, '/home/uli/vrep_ur5/simulation/objects/plane_2cm_dia_2x2xp1.stl', # default size of hole is 2 cm diameter
                          position, orientation, frame_ref='',
                          color=color,
                          collection = '', parent='', use_dummy_as_handle=1)
#  while result == None:
#    print("utils.importShape returned None. Trying again")
#    time.sleep(0.1)
#    result = utils.importShape(sim_client, name, '/home/uli/vrep_ur5/simulation/objects/plane_2cm_dia_2x2xp1.stl', # default size of hole is 2 cm diameter
#                          position, orientation, frame_ref='',
#                          color=color,
#                          collection = '', parent='', use_dummy_as_handle=1)
  if result != None:                        
    planeShape, planeHandle = result
    return planeHandle,position
  else:
    return None

def test_generate_peg(sim_client):
  # Generate a cylinder as the peg
  name = 'peg2'
  position = [0.0, 0.0, -0.03 ] # relative to pos_ref
  orientation = [0, 0, 0]
  size = [0.02, 0.0, 0.07] #y is ignored for cylinder, diameter 2 cm, length 7 cm
  mass = 0.001
  color = [255, 0, 0]
  pos_ref='UR5_tip'
  shape_type = 'cylinder' # can be 'cuboid','sphere','cylinder','cone'
  parent = 'UR5_tip'
  collection = 'Fingers_and_man_obj'
  static = True
  respondable = True
  use_dummy_as_handle = True
  peg = utils.generateShape(sim_client, name, shape_type, size, position, orientation, pos_ref, mass, color,
                            parent,collection,static,respondable,use_dummy_as_handle)
  return peg
  

  
#def import_peg_and_plane(sim_client):  
#  # Import the plane
#  position = [0.0, 0.5, 0.1]
#  orientation = [0, 0, 0] # values in radians, e.g. np.pi/2
#  color = [255, 255, 0]
#  plane = utils.importShape(sim_client, 'plane', '/home/uli/vrep_ur5/simulation/objects/plane_2cm_dia_2x2xp1.stl', # default size of hole is 2 cm diameter
#                          position, orientation, frame_ref='',
#                          color=color,
#                          collection = '', parent='', use_dummy_as_handle=1)
#  color = [0, 0, 255]
#  position = [0,0,-0.03]#[-0.5*7/8*0.0254-0.001, 0, -0.02] # so that it touched the left finger
#  scale=1.0# I am scaling later, 7.0/8.0*0.0254/0.02# 7/8 inch diameter for 2 cm peg               
#  peg = utils.importShape(sim_client, 'peg', '/home/uli/vrep_ur5/simulation/objects/peg_2cm_dia_10cm_long.stl', # default size of peg is 2 cm diameter
#                          position, orientation, frame_ref='UR5_tip',
#                          color=color,
#                          collection = '', parent='UR5_tip', use_dummy_as_handle=1,
#                          scale=scale) 
                          
                    

def test_scaling_hole_in_plane(sim_client):
  #ret,plane = utils.getObjectHandle(sim_client,'plane')
  #ret = utils.scalePlaneWithHole(sim_client,plane,0.05)
  input("press a key")
  utils.scalePlaneWithHole(sim_client,'plane',0.05)
  #utils.scaleObject(sim_client,plane,[1.,2.,2.])
  input("press a key")

def test_scaling_peg(sim_client):
  ret,peg = utils.getObjectHandle(sim_client,'peg')
  ret = utils.scalePeg(sim_client,peg,7.0/8.0*0.0254,0.055)#7.0/8*0.0254,0.1) # this might modify the reference frame position
  
def test_move_robot(ur5):
  # move the robot
  #pose = transformations.euler_matrix(np.radians(90), 0, np.radians(90))
  pose = transformations.euler_matrix(-np.pi, 0.0, 0.0)
  pose[:3,-1] = [0.0, 0.3, 0.3]
  ur5.moveTo(pose)    
  

def test_set_joints(ur5,sim_client):
    # disable dynamics
    utils.control_physics(sim_client,0)
    
    # move to some joint configs, then retore original joint config
    joints_init = ur5.getJoints()
    print("current joint config", joints_init)
    input("press a key")    
    
    #joints = [np.pi,np.pi,np.pi,np.pi,np.pi,np.pi]
    #ur5.setJoints(joints)
    #input("press a key to continue")

    joints = [0,0,0,0,0,0]
    ur5.setJoints(joints)
    input("press a key to continue")

    joints_current = ur5.getJoints()
    print("current joint config", joints_current)        
    input("press a key to continue")

    # restore ur5 joint position
    ur5.setJoints(joints_init)
    joints_current = ur5.getJoints()
    input("press a key to continue")
    utils.control_physics(sim_client,1)

def test_sensor(sim_client):
  sensor_d = utils.VisionSensor(sim_client, 'CAM_SR300_D',z_near=0.01, z_far=0.5)

  # Get sensor data and display it
  depth_data = sensor_d.getDepthData()
  plt.imshow(depth_data)
  plt.show()
  plt.imsave('/tmp/depth_images/depth_image.png', depth_data,cmap=plt.cm.gray, vmin=0, vmax=1)
  
  depth_data_raw = sensor_d.getDepthDataRaw()
  plt.imshow(depth_data_raw)
  plt.show()
  plt.imsave('/tmp/depth_images/depth_image_raw.png', depth_data_raw,cmap=plt.cm.gray, vmin=0, vmax=1)
  
  
  #sensor_RGB = utils.VisionSensor(sim_client, 'CAM_SR300_RGB')
  # Get sensor data and display it
  #img = sensor_RGB.getRGBData()
  #plt.imshow(img) 
  #plt.show()

# used for generating calibration depth images to determine the correct camera pose to mach the real images
def close_fingers_adjust_peg_and_target(sim_client,gripper,peg_name,plane_name):
  #close the gripper
  utils.control_physics(sim_client, 1)
  gripper.close()
  #input("Adjusting peg position, press a key to continue")
  #adjust the position of the peg, such that it sticks 2 cm out
  # get position of peg in frame Tip_left
  utils.control_physics(sim_client, 0)  
  utils.setObjectZpositionRelativeToReference(sim_client,peg_name + '_handle','Tip_left',-0.02)
  #input("Press a key to continue")  
  utils.setObjectPoseByNameInOtherFrame(sim_client,'UR5_tip_ik',peg_name + '_handle',[0,0,0],[0,0,0]) # move UR5_tip to peg_handle
  #input("Press a key to continue")  
  utils.setObjectPoseByNameInOtherFrame(sim_client,'UR5_target',plane_name + '_handle',[0.2,0,0.02],[0,0,0]) # move UR5 target on table and 0.3 m left of it 
    
  # move UR5_tip to peg_handle, and UR5_target 2 cm above the table  
  
  #input("Opening gripper, press a key to continue")  
  # open the gripper  
  utils.control_physics(sim_client, 1)
  gripper.open()
  utils.control_physics(sim_client, 0)    

def clutter_creation(sim_client,plane_name,peg_name):
  folder_meshes = ""#"/home/uli/research/models_3dnet/Cat50_ModelDatabase_STL"
  target_obj = plane_name + '_handle'
  man_obj = peg_name + '_handle'
  collection_name = 'Clutter_objects_with_target'
  gripper_collection_name = 'Fingers_and_man_obj'
  n_obj_range = [5,15]
  clutter = cl.clutter_peg_insertion(sim_client,folder_meshes,target_obj,man_obj,collection_name,gripper_collection_name,n_obj_range)
  clutter.create_clutter_simple_shapes()
  #clutter.test_import_of_all_meshes_1_by_1()  
  
  #input("before removing all clutter")
  #clutter.remove_clutter()
  return clutter


def move_gripper_and_camera_under_parent_peg(sim_client,peg_name,plane_name):
  # similar to clutter creation, need to move objects to differetn parents
  # need to set to parent "peg2_handle" for the two following objects  
  # store original parents so that they can be restored later
  gripper_parent = utils.getObjectParent(sim_client,'RG2_robotiq85_tips')    
  cam_parent = utils.getObjectParent(sim_client,'CAM_SR300_body')    
  utils.setObjectParent(sim_client,'RG2_robotiq85_tips', peg_name + '_handle')
  utils.setObjectParent(sim_client,'CAM_SR300_body', peg_name + '_handle')
  
  return gripper_parent, cam_parent

def restore_parents_for_gripper_and_camera(sim_client,gripper_parent,cam_parent):
  utils.setObjectParent(sim_client,'RG2_robotiq85_tips',gripper_parent)
  utils.setObjectParent(sim_client,'CAM_SR300_body',cam_parent)  

def planning_trajectories_on_grid(sim_client,peg_name,plane_name):
                                    
  robotName = peg_name + '_handle'
  taskName = 'planning_peg'
  targetObject = plane_name + '_handle'
  gripper_collection_name = 'Fingers_and_man_obj'
  clutter_collection_name = 'Clutter_objects_with_target'
  


  taskHandle = utils.sampleValidStatesOnGrid(sim_client,robotName,taskName,targetObject,gripper_collection_name,clutter_collection_name,
                            dim_x_global,dim_y_global,dim_z_global,dim_t_global,
                            checkCol_global,checkVis_global,checkDist_global,
                            bound_target_x_low,bound_target_x_high,
                            bound_target_y_low,bound_target_y_high,
                            bound_target_z_low,bound_target_z_high)
  return taskHandle

  # calculate collision grid and full search of all states
#  emptyBuff = bytearray()
#  VREP_planning_script = 'pathPlannerPoseSpace3dPos'
#  remoteAPIfunction = 'sampleValidStatesOnGrid'
#  inInts = []
#  inFloats = []
#  robotName = peg_name + '_handle'
#  taskName = 'planning_peg'
#  targetObject = plane_name + '_handle'
#  gripper_collection_name = 'Fingers_and_man_obj'
#  clutter_collection_name = 'Clutter_objects_with_target'
#  inStrings =[robotName,taskName,targetObject,gripper_collection_name,clutter_collection_name]
#  res,retInts,retFloats,retStrings,retBuffer=vrep.simxCallScriptFunction(sim_client,VREP_planning_script,vrep.sim_scripttype_childscript,remoteAPIfunction,inInts,inFloats,inStrings,emptyBuff,vrep.simx_opmode_oneshot_wait)
#  taskHandle = retInts[0]
#  
#  return taskHandle

  

def test_visibility(sim_client):
  threshold = 0.01
  NumPixelsVisible = utils.checkVisibility(sim_client,threshold)
  print("NumPixelsVisible", NumPixelsVisible)    


def generate_env_for_peg_hole(sim_client,gripper,scene_no,file_scenes):
  # hole related
  plane_name = 'plane_' + str(scene_no)
  plane_obj,plane_pos = import_plane_with_hole(sim_client,plane_name)
  hole_d = random.uniform(0.025,0.06)
  print("hole_d", hole_d)
  utils.scalePlaneWithHole(sim_client,plane_name,hole_d)
  
  # Generate a cylinder as the peg
  peg_d = random.uniform(0.02,hole_d-0.005)
  print("peg_d", peg_d)
  peg_name = 'peg_' + str(scene_no)
  peg_pos_x = random.uniform(-0.005,0.005) # left, right
  peg_pos_y = random.uniform(-peg_d/4,peg_d/45)
  peg_pos_z = random.uniform(-0.07,-0.025)
  position = [peg_pos_x, peg_pos_y, peg_pos_z ] # relative to pos_ref
  orientation = [0, 0, 0]
  peg_l = random.uniform(abs(peg_pos_z),abs(peg_pos_z)+0.03) # peg_l should be in [abs(peg_pos_z),abs(peg_pos_z)+0.03]
  size = [peg_d, 0.0, peg_l] #y is ignored for cylinder, diameter 2 cm, length 7 cm
  mass = 0.001
  color = [255, 0, 0]
  pos_ref='UR5_tip'
  shape_type = 'cylinder' # can be 'cuboid','sphere','cylinder','cone'
  parent = 'UR5_tip'
  collection = 'Fingers_and_man_obj'
  static = True
  respondable = True
  use_dummy_as_handle = True
  peg_obj = utils.generateShape(sim_client, peg_name, shape_type, size, position, orientation, pos_ref, mass, color,
                            parent,collection,static,respondable,use_dummy_as_handle)  
                            
  # log info about peg in file
  data_to_log = [scene_no,hole_d,peg_d,peg_pos_x,peg_pos_y,peg_pos_z,peg_l]
  write_list_of_lists_to_txt_file(file_scenes,data_to_log,round_digits=6)
  #input("press a key")
  # close the gripper
  utils.control_physics(sim_client,1)
  gripper.open()
  time.sleep(0.5)    
  gripper.close()
  time.sleep(0.5)  
  utils.control_physics(sim_client,0)  
  
  return plane_obj,plane_pos,plane_name,peg_obj,peg_name


class grid_visited_array(object):
  def __init__(self,grid_col,grid_vis):
    
    self.grid_visited, self.num_not_visited = self.create_grid_visited(grid_col,grid_vis)
  
  def create_grid_visited(self,grid_col,grid_vis):
    # create an array where "false" indicates that grid point can be visited (no collision and target visible) and wasn't visited yet
    # Set to true if it can't be visited (collision or target not visible)
    grid_visited1 = (grid_col ==1)  # true if in collision or target not visible
    grid_visited2 = (grid_vis < 16)
    grid_visited = np.logical_or(grid_visited1, grid_visited2)
    num_not_visited = (grid_visited == False).sum()
    return grid_visited, num_not_visited

  def visit_state(self,x,y,z,t=1):
    if(t==1):
      if(self.grid_visited[z,y,x] == False):
        self.grid_visited[z,y,x] == True
        self.num_not_visited = self.num_not_visited-1
        
    else:
      if(self.grid_visited[t,z,y,x] == False):
        self.grid_visited[t,z,y,x] == True
        self.num_not_visited = self.num_not_visited-1
    assert(self.num_not_visited >= 0), "Error self.num_not_visited is negative"
          
  def get_grid_visited(self):
    return self.grid_visited
    
  def get_visited(self,x,y,z,t=1):
    if(t==1):
      return self.grid_visited[z,y,x]
    else:
      return self.grid_visited[t,z,y,x]
      

  def get_num_not_visited(self):
    return self.num_not_visited

def sample_visible_and_collision_free_state(sim_client,obj_handle,grid_col,grid_vis,values_x,values_y,values_z,grid_visited):
  # TODO use precomputed collision and visibility grid to sample a valid state directly
  if grid_visited.get_num_not_visited() == 0:
    print("No visible and collision free state was found, need to resample clutter?")
    return False
  state_x_i = random.choice(range(len(values_x)))
  state_y_i = random.choice(range(len(values_y)))
  state_z_i = random.choice(range(len(values_z)))

  while grid_visited.get_visited(state_x_i,state_y_i,state_z_i) and grid_visited.get_num_not_visited() > 0:
    state_x_i = random.choice(range(len(values_x)))
    state_y_i = random.choice(range(len(values_y)))
    state_z_i = random.choice(range(len(values_z)))

  grid_visited.visit_state(state_x_i,state_y_i,state_z_i)

  
  # Use lookup table to check visibility (this works if we sample initial states on grid)
  NumPixelsVisible = grid_vis[state_z_i, state_y_i, state_x_i]  
  collision = grid_col[state_z_i, state_y_i, state_x_i] # order z,y,x for index seem to work  
  
  assert(NumPixelsVisible >= 16 and collision == 0), "NumPixelsVisible < 16 or collision == 0"
    
  position = [values_x[state_x_i], values_y[state_y_i], values_z[state_z_i]]
  utils.setObjectPosition(sim_client,obj_handle, position)
  # add oriantation later
  #utils.setObjectOrientation(sim_client,obj_handle, [0,0,math.pi/2])
  
  return position,collision,NumPixelsVisible,grid_visited

def getCollisionGrid(sim_client,planner_id,dim_x,dim_y,dim_z,dim_t=1):
  grid_as_list = utils.getGridCol(sim_client,planner_id)
  grid_col = utils.grid_shaped_as_nparray(grid_as_list,dim_x,dim_y,dim_z,dim_t)
  return grid_col

def getVisibilityGrid(sim_client,planner_id,dim_x,dim_y,dim_z,dim_t=1):
  grid_as_list = utils.getGridVis(sim_client,planner_id)
  grid_vis = utils.grid_shaped_as_nparray(grid_as_list,dim_x,dim_y,dim_z,dim_t)
  return grid_vis



def write_list_to_txt_file(file_out,list_of_data):
    for info in list_of_data:    
        file_out.write(str(info))
        file_out.write(',')
    file_out.write('\n')  

def write_list_of_lists_to_txt_file(file_out,list_of_lists,round_digits=5):
  '''
  Elements between lists (top level) are comma seperated, then in sublists are space separated.
  Float numbers are round to 5 digits.
  
  # Example:
  # list_of_lists = ["string",0.123,1,[1,2,3],[0.12345667,9.61837628368], ["string", "anotherString", 0.14454]]
  # result in file_out is:
  # string,0.123,1,1 2 3,0.12346 9.61838,string anotherString 0.14454
  '''
  for i,l in enumerate(list_of_lists):
    if isinstance(l, list):
      for j,x in enumerate(l):
        if isinstance(x, float) and round_digits:
          x = round(x,round_digits)
        file_out.write(str(x))
        if j != len(l)-1: # do not write the last ' ', because this will be a ',' instead, if there are more poses to be written
          file_out.write(' ')

    else: # not a list
      if isinstance(l, float):
        l = round(l,round_digits)
      file_out.write(str(l))      
    if i != len(list_of_lists)-1: # do not write the last ','
      file_out.write(',')
  file_out.write('\n')      



def create_logging_files(dataset_path):
  '''
    Setup logging files for relevant info on different levels (general, scenes, images)
  '''
  dataset_path_images = os.path.join(dataset_path,'images') + "/"
  dataset_path_labels = os.path.join(dataset_path,'labels') + "/"
  if(os.path.exists(dataset_path_images) == False): #check if directory exist
      os.makedirs(dataset_path_images)
  else:
    print("Warning: Folder already exists", dataset_path_images)
    input("Press a key to continue")
  
  if(os.path.exists(dataset_path_labels) == False): #check if directory exist
      os.makedirs(dataset_path_labels)
  else:
    print("Warning: Folder already exists", dataset_path_labels)
    input("Press a key to continue")
  
  file_general = open(dataset_path_labels + "info_general.csv", "a")    
  file_scenes = open(dataset_path_labels + "info_scenes.csv", "a")
  file_images = open(dataset_path_labels + "info_images.csv", "a")
  
  #delta_actions = ["delta_action_x","delta_action_y","delta_action_z","delta_action_theta"]
  #bounds_global = ["x_min","x_max","y_min","y_max","z_min","z_max"]
  #grid_dims = ["dim_x","dim_y","dim_z","dim_t"]
  #position_target_global = ["p_target_x","p_target_y","p_target_z"]
  headers_general = ["position_target_global", "values_x", "values_y", "values_z"]
  write_list_to_txt_file(file_general,headers_general)
  
  headers_scenes = ["scene_no","hole_d","peg_d","peg_pos_x","peg_pos_y","peg_pos_z","peg_l"]
  write_list_to_txt_file(file_scenes,headers_scenes)

  headers_images = ["image_name","scene_no","traj_no","img_no","q_values","best_index","best_action","pos_target_in_robot","orient_target_in_robot","NumPixelsVisible"]
  write_list_to_txt_file(file_images,headers_images)
  
  file_general.flush()
  file_scenes.flush()
  file_images.flush()
  
  # copy main script to dataset_path_labels (as backup)
  file_main_script = os.path.realpath(__file__)
  utils_file = os.path.join(git_location,"utils/vrep_utils.py")
  scene_file = os.path.join(git_location,"simulation/scenes/vrep_scene_ur5_v22_peg_insertion_planning.ttt")
  config_file = os.path.join(git_location,"scripts/config.ini")  
  files_to_copy = [file_main_script,utils_file,scene_file,config_file]
  copy_files_to_path(files_to_copy,dataset_path_labels)  
  
  return file_general,file_scenes,file_images,dataset_path_images,dataset_path_labels

def copy_files_to_path(files,path):
  for src in files:
    if os.path.isfile(src) and os.path.exists(path):
      dst = os.path.join(path,os.path.basename(src))
      copyfile(src, dst)
    else:
      print("Error copying file")
      if os.path.isfile(src) == False:
        print("Source file does not exist: ", src)
      if os.path.exists(path) == False:
        print("Output path does not exist: ", path)
      return False

  return True
  
def main(sim_port_offset=None):
  #update_globals_from_config_ini()
  # Attempt to connect to simulator
  if sim_port_offset==None:
    sim_port = 19997
  else:
    sim_port = 19997+sim_port_offset
  print("connecting to sim_port", sim_port)
  sim_client = utils.connectToSimulation('127.0.0.1', sim_port)    
  # Create RG2 gripper and restart simulator
  gripper = RG2(sim_client)
  sensor_d = utils.VisionSensor(sim_client, 'CAM_SR300_D',z_near=0.01, z_far=0.5)
  ur5 = UR5(sim_client, gripper)


  ###########################################
  # Setup files for logging, parameters etc #
  ###########################################
  
  config = configparser.RawConfigParser()
  config.read('config.ini')  # please copy config_example.ini to config.ini and adjust the paths/parameters in this file
  
  dataset_path_output = config.get('data_generation', 'dataset_path_output')  
  
  
  if sim_port_offset != None:
    dataset_path_output = os.path.join(dataset_path_output,str(sim_port_offset))
  file_general,file_scenes,file_images,dataset_path_images,dataset_path_labels = create_logging_files(dataset_path_output)

  
  #####################
  ### setup (scene) ###
  #####################

  scene_no = 0

  while scene_no < n_scenes:
    print("scene_no: ", scene_no)
    t0_scene = time.time()
    utils.restartSimulation(sim_client)
    #vrep.simxStopSimulation(sim_client, VREP_BLOCKING)
    #time.sleep(2)
    #vrep.simxStartSimulation(sim_client, VREP_BLOCKING)
    #time.sleep(2)
    utils.GUI_enabled(sim_client,False)
    
  
    # print "ping time", vrep.simxGetPingTime(sim_client)    
    #  IMPORT PLANE, PEG AND scale
    plane_obj,plane_pos,plane_name,peg_obj,peg_name = generate_env_for_peg_hole(sim_client,gripper,scene_no,file_scenes)

    ret,plane_obj_handle = utils.getObjectHandle(sim_client,plane_name + '_handle')
    #ret,T_global_plane = utils.getObjectPose(sim_client, plane_obj_handle)
    ret,peg_obj_handle = utils.getObjectHandle(sim_client,peg_name + '_handle')
    T_global_peg_init = utils.getObjectMatrix(sim_client, peg_name + '_handle')
    
    
    
    #print "T_global_plane", T_global_plane
  
    #input("Press a key to continue")
    
    # Create Clutter
    clutter = clutter_creation(sim_client,plane_name,peg_name)
    #input("Press a key to continue")
    
    # need to move gripper and camera in the hierarchy under parent "peg" so that if the peg is moved to a desired location, camera and gripper follow
    gripper_parent, cam_parent = move_gripper_and_camera_under_parent_peg(sim_client,peg_name,plane_name)
    
    # Planning: Check collisions on grid, find trajectories to goal
    planning_handle = planning_trajectories_on_grid(sim_client,peg_name,plane_name)
    #input("Press a key to continue")
    
    utils.findAllPathsToGoal(sim_client,planning_handle)
    #input("Press a key to continue")
    
    values_x,values_y,values_z = utils.getGridAxesValues(sim_client,planning_handle)
    dim_x = len(values_x)
    dim_y = len(values_y)
    dim_z = len(values_z)
    if(scene_no == 0):
      data_to_file = [plane_pos,values_x,values_y,values_z]
      write_list_of_lists_to_txt_file(file_general,data_to_file,round_digits=False)
      file_general.close()
    
    # collision Grid in shape of x,y,z dimensions
    grid_col = getCollisionGrid(sim_client,planning_handle,dim_x,dim_y,dim_z,dim_t=1)
    grid_vis = getVisibilityGrid(sim_client,planning_handle,dim_x,dim_y,dim_z,dim_t=1)
    grid_visited = grid_visited_array(grid_col,grid_vis)

    
    ############################################################### 
    ###       Data collection/interaction with the environment    #
    ###############################################################  
  
    traj_no = 0  
  
    while traj_no < n_traj_per_scene:
      print("traj_no", traj_no)
      num_traj_to_time = 10
      if traj_no % num_traj_to_time  == 0:
        t0_trajectory = time.time()
      img_no = 0
      # Sample an initial state that is not in collision and at least 16 pixels visible
      
      result = sample_visible_and_collision_free_state(sim_client,peg_obj_handle,grid_col,grid_vis,values_x,values_y,values_z,grid_visited)
      if result == False:
        print("exit for loop AND create new clutter")
        break
      
      p_global_peg,collision,NumPixelsVisible,grid_visited = result

      depth_data = sensor_d.getDepthData() # 0.5 meters=black, 0 meters=white

      scene_no_str = str(scene_no).zfill(6)
      traj_no_str = str(traj_no).zfill(6)
      img_no_str = str(img_no).zfill(6)
      image_file_name = 'img_' + scene_no_str + '_' + traj_no_str + '_' + img_no_str +'.png'   
      plt.imsave(dataset_path_images + image_file_name, depth_data,cmap=plt.cm.gray, vmin=0, vmax=1)

      #q_values, best_action, action_deltas, pos_target_in_robot, orient_target_in_robot, best_value, best_index = utils.getLabelsForCurrentState(sim_client, planning_handle, peg_name + '_handle', plane_name + '_handle')

      q_values2, best_action2, _ , _ , _, _, best_index2 = utils.getFewerLabelsForCurrentState(sim_client, planning_handle, peg_name + '_handle', plane_name + '_handle')      


      #data_to_file = [image_file_name,scene_no,traj_no,img_no,q_values,best_index,best_action,pos_target_in_robot,orient_target_in_robot,NumPixelsVisible]
      # reduced number label info to file
      data_to_file = [image_file_name,scene_no,traj_no,img_no,q_values2,best_index2,best_action2,[0.,0.,0.],[0.,0.,0.],0]
      write_list_of_lists_to_txt_file(file_images,data_to_file)
     
      traj_no += 1
      img_no += 1
      if traj_no % num_traj_to_time == 0:
        t1_trajectory = time.time()
        print("Time required for ",num_traj_to_time, " trajectories: ", t1_trajectory-t0_trajectory)

    
    ###############  
    ### Cleanup ###
    ###############
    

    # restore original peg pose
    #utils.setObjectMatrix(sim_client, peg_name + '_handle',T_global_peg_init)
 
    #restore original parents
    #restore_parents_for_gripper_and_camera(sim_client,gripper_parent,cam_parent)  
    
    utils.destroyPlanningTask(sim_client,planning_handle)  
    
    # Remove Clutter
    #clutter.remove_clutter()
  
    
    # REMOVE PEG and Plane (to import with different geometric parameters next time)
    #utils.removeObjectWithChildren(sim_client,peg_obj)
    #utils.removeObjectWithChildren(sim_client,plane_obj)

    #input("Next scene, press a key")
    scene_no +=1
    t1_scene = time.time()
    print("Time required for a scene: ", t1_scene-t0_scene)
    
    
  ##########################
  # End create scene       #
  ##########################

######## Some old code used for testing individual functions: #########
  #test_import_plane_with_hole(sim_client)
  #test_scaling_hole_in_plane(sim_client)

  #test_generate_peg(sim_client)
  # close the gripper
  #utils.control_physics(sim_client,1)
  #gripper.close()
  #time.sleep(0.5)  
  #input("Press a key to continue")  
  #utils.control_physics(sim_client,0)  
  
  #for i in range(20):
  #clutter = test_clutter_creation(sim_client)
  #test_sensor(sim_client)
  #test_visibility(sim_client)
  
  #input("After test_visibility")  
  
  #t = planning_trajectories_on_grid(sim_client)
  #utils.findAllPathsToGoal(sim_client,t)
  #path = utils.findPath(sim_client,t,[0,0,0.099],'plane_handle')
  #print "path", path

  #clutter.remove_clutter()
  

  #test_scaling_peg(sim_client)
  #test_scaling_hole_in_plane(sim_client)

  #test_sensor(sim_client)

  #utils.destroyPlanningTask(sim_client,t)



  #input("Press a key to exit")                

  # Wait for arm to move the exit
  #time.sleep(10)

  file_scenes.close()
  file_images.close()
  
  vrep.simxStopSimulation(sim_client, VREP_BLOCKING)

  vrep.simxFinish(sim_client)
  print ('Program ended')
  #exit()


if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('--sim_port_offset', type=int, default = 0,
      help='The simulators index starting with 0. Used for port increments.')
  args = parser.parse_args()      
  main(args.sim_port_offset)
