# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 16:47:56 2019

@author: uli

Convert model 3dNet meshes from ply to stl, so that v-rep can read them in
This converts 1373 files
 run as:
 python convert_meshes_ply2stl.py /home/uli/research/models_3dnet/Cat50_ModelDatabase/ /home/uli/research/models_3dnet/Cat50_ModelDatabase_STL/
"""

import os
import subprocess
import argparse

    
if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('input_path', type=str,
      help='The path to root folder containing folders with PLY files')
  parser.add_argument('output_path', type=str,
      help='The path to root folder to write the output STL files (with same folder structure as in input_path)')

  args = parser.parse_args()
 
  if not os.path.isdir(args.output_path):
    os.mkdir(args.output_path)
  #root, ouput_folder_name = os.path.split(args.output_path)
  #print(root, ouput_folder_name)
  os.chdir(args.output_path)
  #print(os.listdir(args.input_path))
  count_convert = 0
  for dirName, subdirList, filelist in os.walk(args.input_path):
    #print("dirName, subdirList, filelist", dirName, subdirList, filelist)
    rel_path_to_input = os.path.relpath(dirName, args.input_path)
    print("rel_path_to_input", rel_path_to_input)
    new_output_path = os.path.join(args.output_path,rel_path_to_input)
    if not os.path.isdir(new_output_path):
      os.mkdir(new_output_path)
    for f in filelist:
      if f.endswith(".ply"):
        print("Converting file: ", f)
        src_file_with_path = os.path.join(dirName,f)
        filename_without_extension, _ = f.split(".")        
        des_file_with_path = os.path.join(new_output_path,filename_without_extension+".stl")
        #print("src_file_with_path", src_file_with_path)
        #print("des_file_with_path", des_file_with_path)
        # commadn to convert the files is like this: ctmconv 002_cap_coke.ply 002_cap_coke.stl
        subprocess.call(["ctmconv",src_file_with_path,des_file_with_path] )
        count_convert +=1
    #input("press a key")
  print("Converted ", count_convert, " files.")
  print("Finished")    
    
