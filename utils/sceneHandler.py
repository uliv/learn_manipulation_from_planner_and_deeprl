#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 21 14:27:46 2019

@author: uli
"""

import sys
sys.path.append('./')
sys.path.append('..')
sys.path.append('../simulation')
sys.path.append('../scripts')
import utils.vrep_utils as utils
import scene_gen_utils as scene
import threading, time

class sceneInfo(object):
  '''
    Info for each simulation of the scenes (info not common across different sims), e.g. the state of it (busy)
    or the sim_port_number.
  '''
  def __init__(self,config_file,sim_port_offset):
    self.sim_port_offset = sim_port_offset
    self.is_busy = False
    self.env = scene.vrepEnv(config_file,self.sim_port_offset)
    self.sim_client = self.env.get_sim_client()
  

class sceneHandler(object):
  # Public
  def __init__(self,config_file,num_sim,sim_port_offset):
    self.config_file = config_file
    #self.sim_port_offsets = [0,1]#,2]#,1,2] #later use an argument for this
    self.num_sim = num_sim
    self.sim_port_offsets = range(sim_port_offset,self.num_sim+sim_port_offset)
    self.sceneInfoList = []
    for sim_port_offset in self.sim_port_offsets:
      self.sceneInfoList.append(sceneInfo(self.config_file,sim_port_offset))
    self.scene_current_idx = 0 # the index in self.sceneInfoList for the current/active scene
    self.scene_idx_update_next = 0
    #self.env = self.scene_current.env # the current environment

    #setup all scene
    self.setup_all_scenes()
  
  def get_next_scene_idx_not_busy(self):
    n_sim_checked = 0
    # Check self.num_sim times
    idx = self.scene_current_idx
    while(n_sim_checked < self.num_sim):
      #print("n_sim_checked", n_sim_checked, "self.num_sim", self.num_sim, "idx", idx)
      if idx == self.num_sim-1:
        idx = 0
      else:
        idx += 1
      #print("idx (updated)", idx)
      if(self.sceneInfoList[idx].is_busy == False):# and idx != self.scene_current_idx):
        return idx
      n_sim_checked += 1
    print("get_next_scene_idx_not_busy: All scenes are busy")
    return None
      
  #  return self.sceneInfoList[self.scene_current_idx].is_busy
  
  def setup_scene_threaded(self,scene_idx,arg2):
    print("started threaded setup scene (busy=True). scene_idx=", scene_idx)
    self.sceneInfoList[scene_idx].is_busy = True
    self.sceneInfoList[scene_idx].env.setup_scene()
    print("finished threaded setup scene (busy=False). scene_idx=", scene_idx)
    self.sceneInfoList[scene_idx].is_busy = False


  
  def setup_scene(self,scene_idx):
    '''
    Update a scene given by the index (restart sim, load peg/hole, clutter, plan on full grid)
    '''
    thread = threading.Thread(target=self.setup_scene_threaded, args=(scene_idx,None))
    thread.start()
    time.sleep(0.5)  # add a delay so that busy flag is set correctly within thread
    
  def setup_all_scenes(self):
    for scene_idx in range(self.num_sim):
      self.setup_scene(scene_idx)
      while(self.sceneInfoList[scene_idx].is_busy):
        print("setup_all_scenes: Busy setting up scene: ", scene_idx)
        time.sleep(1)        
  
  # Public
  def switch_scene(self,update_current_scene_after_switch=False):
    '''
      Switch to the next scene that is not busy
    '''
    #sim_client_0 = self.sceneInfoList[0].sim_client
    #sim_client_1 = self.sceneInfoList[1].sim_client
    #print("sim_client_0, sim_client_1", sim_client_0, sim_client_1)
    #utils.printToTerminal(sim_client_0,"Is this terminal 0?")
    #utils.printToTerminal(sim_client_1,"Is this terminal 1?")

    new_idx = self.get_next_scene_idx_not_busy()
    count = 0
    while(new_idx == None):# or new_idx == self.scene_current_idx):
      new_idx = self.get_next_scene_idx_not_busy()
      print("In switch_scene, new_idx=None or current_idx. count=", count)
      count +=1
      time.sleep(1)
    if update_current_scene_after_switch:
      self.setup_scene(self.scene_current_idx)
    self.scene_current_idx = new_idx
      
      
    print("New scene offset", self.sceneInfoList[self.scene_current_idx].sim_port_offset)
    #while self.sceneInfoList[self.scene_current_idx].is_busy:
    #  print("new scene is still busy")
    #  time.sleep(1)
    #utils.printToTerminal(sim_client_0,"Is this terminal 0?")
    #utils.printToTerminal(sim_client_1,"Is this terminal 1?")      
    return self.get_current_env()
          
    # iterate over scenes (in a cyrcle) until either a new scene was found that is not busy
    # or all scenes are busy
    # after switching scene,
    
    # should return a new scene that is NOT busy,
    # Or return, that switching is not possible (scenes busy)
    
  # Public    
  def get_current_env(self):
    while(self.sceneInfoList[self.scene_current_idx].is_busy):
      print("get_current_env: Current scene is still busy")
      time.sleep(1)      
    return self.sceneInfoList[self.scene_current_idx].env
    