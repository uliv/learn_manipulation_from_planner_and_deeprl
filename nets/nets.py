#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 15:36:29 2019

Definition of network architectures

@author: uli
"""

#from __future__ import print_function, division
import torch.nn as nn
import torch.nn.functional as F

class DQN_lenet(nn.Module):
    '''DQN based on lenet.
    
      This seems to work well for the RL (learning the q-values)
      or for supervised learning (learning actions), see "training.py"
      in "supervised_learning" folder.
    '''  
    def __init__(self,classification=False,num_actions=27):
        super(DQN_lenet, self).__init__()
        self.classification = classification
        self.num_actions = num_actions
        self.conv1 = nn.Conv2d(1, 20, kernel_size=5, stride=1)
        self.conv2 = nn.Conv2d(20, 50, kernel_size=5, stride=1)
        #self.conv3 = nn.Conv2d(50, 80, kernel_size=5, stride=1)        

        self.fc1 = nn.Linear(50*13*13, 500) # 2 conv with pool
        #self.fc1 = nn.Linear(80*4*4, 500) # for 3 conv layers with pool
        #self.fc1 = nn.Linear(50*56*56, 500) # 2 convs without maxpools
        #self.fc1 = nn.Linear(80*52*52, 500) # 3 convs without maxpools
        #self.fc1 = nn.Linear(50*28*28, 500) # 2 convs 1 max pool after 2nd conv
        self.fc2 = nn.Linear(500, 500) # for 3 IP layers       
        self.fc3 = nn.Linear(500, self.num_actions)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        #print(x.shape)
        x = F.max_pool2d(x, 2, 2)
        #print(x.shape)        
        x = F.relu(self.conv2(x))
        #print(x.shape)        
        x = F.max_pool2d(x, 2, 2)
        #print(x.shape)     
        #x = F.relu(self.conv3(x))
        #print(x.shape)        
        #x = F.max_pool2d(x, 2, 2)
        #print(x.shape)           

        x = x.view(-1, 50*13*13) # if using only 2 conv layers
        #x = x.view(-1, 80*4*4) # if using 3 conv layers
        #x = x.view(-1, 50*56*56) # if using only 2 conv layers no pool layers 
        #x = x.view(-1, 80*52*52) # if using only 3 conv layers no pool layers            
        #x = x.view(-1, 50*28*28) # 2 convs 1 max pool after 2nd conv


        #print(x.shape)        
        x = F.relu(self.fc1(x))
        #print(x.shape)        

        # added another IP layer
        x = F.relu(self.fc2(x)) # for 3 IP layers        
        #print(x.shape)        
        x = self.fc3(x)
        #print(x.shape)
        if self.classification:      
          return F.log_softmax(x, dim=1)    
        else: # regression (Q(s) for |a| outputs)
          return x.view(x.size(0), -1)

class DQN(nn.Module):
    '''
    Default DQN network for the cart-pole example. Commented out batchNorm,
    because they do not work well with the depth image (I need to preserve the
    depth information, normalization looses this information).
    It turns our that even without normalizations, this DQN structure is not able
    to learn the right actions from the depth images.
    Therefore, I am using DQN_lenet instead. This works well for both supervised
    learning and DQN (RL).
    '''
    def __init__(self, h, w, dueling=False, num_actions=27):
        super(DQN, self).__init__()
        self.dueling = dueling
        self.num_actions = num_actions
        self.conv1 = nn.Conv2d(1, 16, kernel_size=5, stride=2)
        #self.bn1 = nn.BatchNorm2d(16)
        self.conv2 = nn.Conv2d(16, 32, kernel_size=5, stride=2)
        #self.bn2 = nn.BatchNorm2d(32)
        self.conv3 = nn.Conv2d(32, 32, kernel_size=5, stride=2)
        #self.bn3 = nn.BatchNorm2d(32)

        # Number of Linear input connections depends on output of conv2d layers
        # and therefore the input image size, so compute it.
        def conv2d_size_out(size, kernel_size = 5, stride = 2):
            return (size - (kernel_size - 1) - 1) // stride  + 1
        convw = conv2d_size_out(conv2d_size_out(conv2d_size_out(w)))
        convh = conv2d_size_out(conv2d_size_out(conv2d_size_out(h)))
        linear_input_size = convw * convh * 32
        if self.dueling:
          self.adv = nn.Linear(linear_input_size, self.num_actions)
          self.val = nn.Linear(linear_input_size, 1)
        else:
          self.head = nn.Linear(linear_input_size, self.num_actions) # 448 or 512

    # Called with either one element to determine next action, or a batch
    # during optimization. Returns tensor([[left0exp,right0exp]...]).
    def forward(self, x):
        #x = F.relu(self.bn1(self.conv1(x)))
        #x = F.relu(self.bn2(self.conv2(x)))
        #x = F.relu(self.bn3(self.conv3(x)))
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))        
        x = x.view(x.size(0), -1)
        if self.dueling:
          adv = F.relu(self.adv(x))
          val = F.relu(self.val(x))
          return val + adv - adv.mean(1, keepdim=True)
        else:
          return F.log_softmax(x, dim=1)#self.head(x)
  