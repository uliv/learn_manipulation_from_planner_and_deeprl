#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 15:36:29 2019

@author: uli
"""

from __future__ import print_function, division
import sys
import os
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import pandas as pd
from skimage import io
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import argparse
import filter_data as fd
sys.path.append('../scripts')
import scene_gen_utils as scene # for plotting data to file
import utils.vrep_utils as vrep_utils
import PNG_16bit as PNG
#import nonechucks as nc # to allow filtering out samples in dataloader
import configparser
import random



# Ignore warnings
import warnings
warnings.filterwarnings("ignore")

plt.ion()   # interactive mode


class DQN_lenet(nn.Module):
    def __init__(self,classification=True):
        super(DQN_lenet, self).__init__()
        self.classification = classification
        self.conv1 = nn.Conv2d(1, 20, kernel_size=5, stride=1)
        self.conv2 = nn.Conv2d(20, 50, kernel_size=5, stride=1)
        #self.conv3 = nn.Conv2d(50, 80, kernel_size=5, stride=1)        

        self.fc1 = nn.Linear(50*13*13, 500) # 2 conv with pool
        #self.fc1 = nn.Linear(80*4*4, 500) # for 3 conv layers with pool
        #self.fc1 = nn.Linear(50*56*56, 500) # 2 convs without maxpools
        #self.fc1 = nn.Linear(80*52*52, 500) # 3 convs without maxpools
        #self.fc1 = nn.Linear(50*28*28, 500) # 2 convs 1 max pool after 2nd conv
        self.fc2 = nn.Linear(500, 500) # for 3 IP layers       
        self.fc3 = nn.Linear(500, 27)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        #print(x.shape)
        x = F.max_pool2d(x, 2, 2)
        #print(x.shape)        
        x = F.relu(self.conv2(x))
        #print(x.shape)        
        x = F.max_pool2d(x, 2, 2)
        #print(x.shape)     
        #x = F.relu(self.conv3(x))
        #print(x.shape)        
        #x = F.max_pool2d(x, 2, 2)
        #print(x.shape)           

        x = x.view(-1, 50*13*13) # if using only 2 conv layers
        #x = x.view(-1, 80*4*4) # if using 3 conv layers
        #x = x.view(-1, 50*56*56) # if using only 2 conv layers no pool layers 
        #x = x.view(-1, 80*52*52) # if using only 3 conv layers no pool layers            
        #x = x.view(-1, 50*28*28) # 2 convs 1 max pool after 2nd conv


        #print(x.shape)        
        x = F.relu(self.fc1(x))
        #print(x.shape)        

        # added another IP layer
        x = F.relu(self.fc2(x)) # for 3 IP layers        
        #print(x.shape)        
        x = self.fc3(x)
        #print(x.shape)
        if self.classification:      
          return F.log_softmax(x, dim=1)    
        else: # regression (Q(s) for |a| outputs)
          return x.view(x.size(0), -1)

class DQN(nn.Module):

    def __init__(self, h, w, dueling=False, NUM_ACTIONS=27):
        super(DQN, self).__init__()
        self.dueling = dueling
        self.conv1 = nn.Conv2d(1, 16, kernel_size=5, stride=2)
        #self.bn1 = nn.BatchNorm2d(16)
        self.conv2 = nn.Conv2d(16, 32, kernel_size=5, stride=2)
        #self.bn2 = nn.BatchNorm2d(32)
        self.conv3 = nn.Conv2d(32, 32, kernel_size=5, stride=2)
        #self.bn3 = nn.BatchNorm2d(32)

        # Number of Linear input connections depends on output of conv2d layers
        # and therefore the input image size, so compute it.
        def conv2d_size_out(size, kernel_size = 5, stride = 2):
            return (size - (kernel_size - 1) - 1) // stride  + 1
        convw = conv2d_size_out(conv2d_size_out(conv2d_size_out(w)))
        convh = conv2d_size_out(conv2d_size_out(conv2d_size_out(h)))
        linear_input_size = convw * convh * 32
        if dueling:
          self.adv = nn.Linear(linear_input_size, NUM_ACTIONS)
          self.val = nn.Linear(linear_input_size, 1)
        else:
          self.head = nn.Linear(linear_input_size, NUM_ACTIONS) # 448 or 512

    # Called with either one element to determine next action, or a batch
    # during optimization. Returns tensor([[left0exp,right0exp]...]).
    def forward(self, x):
        #x = F.relu(self.bn1(self.conv1(x)))
        #x = F.relu(self.bn2(self.conv2(x)))
        #x = F.relu(self.bn3(self.conv3(x)))
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))        
        x = x.view(x.size(0), -1)
        if self.dueling:
          adv = F.relu(self.adv(x))
          val = F.relu(self.val(x))
          return val + adv - adv.mean(1, keepdim=True)
        else:
          return F.log_softmax(x, dim=1)#self.head(x)

#with open('/home/uli/research/thesis/datasets/dataset_7/labels/info_images.csv', 'r' ) as T : 
#  lines = T.readlines()
#
#i = 0
#while i < len(lines):
#  sp = lines[i].split(',') # this splits between filename and poses
#  for s in sp:
#    print("sp", s)
#  i = i+1
    
class DepthImageDataset(Dataset):
    """Depth Image dataset with q-values, etc."""

    def __init__(self, csv_file, root_dir, col_oob_value=1.0, transform=None, read_8bit_png=False):

        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.depth_images_with_annotations = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform
        self.read_8bit_png = read_8bit_png
        self.col_oob_value = col_oob_value
        

    def __len__(self):
        return len(self.depth_images_with_annotations)

    def __getitem__(self, idx):
        img_name = os.path.join(self.root_dir,
                                self.depth_images_with_annotations.iloc[idx, 0])

        if self.read_8bit_png:
          image = io.imread(img_name) # to read 8 bit PNG, values will be integers in [0,255]
          image = image[:,:,0]
          image = image.astype(np.float32)/255.0
          #print("8 bit, image[0,0]", image[0,0])
          
        else:
          image = PNG.read_png16bit(img_name) #reads 16bit PNG that was written using write_png16bit. Values will be in [0,1) 
          #print("16 bit, image[0,0]", image[0,0])

        q_values = self.depth_images_with_annotations.iloc[idx, 4] # q-values
        q_values_flat = np.asarray(q_values.split(' ')).astype('float')
        q_values = q_values_flat.reshape(-1, 3, 3) #don't reshape, keep flat as outputs of neural net for q-values and actions
        best_index = np.asarray(self.depth_images_with_annotations.iloc[idx, 5]-1) # best_index, index from 0-26 (-1 because LUA in v-rep index starts from 1)
        best_action = self.depth_images_with_annotations.iloc[idx, 6] # best_action
        best_action = np.asarray(best_action.split(' '))
        best_action = best_action.astype('float') # order: {x,y,z]
        best_action_indices = np.unravel_index(best_index, q_values.shape) # order {z,y,x}, e.g. (0,2,1) corresponds to action [x,y,z]=[0,0.01,-0.01]
        best_action_indices = np.asarray(best_action_indices)
        traj_no = np.asarray(self.depth_images_with_annotations.iloc[idx, 2])
        # was image[:,:,0]
        sample = {'image': image, 'q_values': q_values, 'q_values_flat': q_values_flat,
                  'best_index': best_index, 'best_action': best_action, 'best_action_indices': best_action_indices, 'traj_no': traj_no}

        if self.transform:
            sample = self.transform(sample)

        return sample

class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __init__(self, col_oob_value=1.0, learn_best_q_value_only=False, best_q_value_margin=False, q_margin=0.0,\
                 add_noise=False,noise_prob_min=0.0,noise_prob_max=0.15):
        self.col_oob_value = col_oob_value
        self.learn_best_q_value_only = learn_best_q_value_only
        self.best_q_value_margin = best_q_value_margin
        self.q_margin = q_margin
        self.add_noise = add_noise
        self.noise_prob_min = noise_prob_min
        self.noise_prob_max = noise_prob_max

    def __call__(self, sample):
        if sample == None:
          return
        image, q_values, q_values_flat, best_index, best_action, best_action_indices, traj_no = \
        sample['image'], sample['q_values'], sample['q_values_flat'], sample['best_index'], sample['best_action'], sample['best_action_indices'], sample['traj_no']


        if self.add_noise:
          replace_rate = random.uniform(self.noise_prob_min, self.noise_prob_max)
          image_zeros = np.ones(image.shape)
          mask = np.random.choice([0, 1], size=image.shape, p=((1 - replace_rate), replace_rate)).astype(np.bool)
          image[mask] = image_zeros[mask]
        
        # swap color axis because
        # numpy image: H x W x C
        # torch image: C X H X W
        image = image[..., np.newaxis]
        image = image.transpose((2, 0, 1))
        image = np.ascontiguousarray(image, dtype=np.float32) 
        
        # Convert q-values into right format (sign etc)
        q_values = vrep_utils.ConvertQvalues(q_values,self.col_oob_value,self.learn_best_q_value_only,
                                             self.best_q_value_margin, self.q_margin)
        q_values_flat = vrep_utils.ConvertQvalues(q_values_flat,self.col_oob_value,self.learn_best_q_value_only,
                                             self.best_q_value_margin, self.q_margin)       
        return {'image': torch.from_numpy(image),
                'q_values': torch.from_numpy(q_values),
                'q_values_flat': torch.from_numpy(q_values_flat),                
                'best_index': torch.from_numpy(best_index),
                'best_action': torch.from_numpy(best_action),
                'best_action_indices': torch.from_numpy(best_action_indices),
                'traj_no': torch.from_numpy(traj_no)
                }


  
class DataFilter(object):
  """
    filter out samples that should not be used for training/test
    Req. import nonechucks
    This doesn't work well (pid/worker issue and slow)
    Instead, I preprocess the csv file for images and labels
  """
  def __init__(self):
    pass # maybe use later to define type of filter
  
  def __call__(self, sample):
    if self.drop_samples_with_collision_or_out_of_bound(sample):
      return None
    return sample
    
  def drop_samples_with_collision_or_out_of_bound(self, sample):
    """
      Check if a sample contains actions that lead to collision
      (q-value >= 1.0) or out-of-bound (q-value: -1.0)

      Input: sample

      Return: True if at least 1 of the two conditions is true 
    """
    q_values = sample['q_values']
    col = np.amax(q_values) >= 10.0
    out_of_bound = np.min(q_values) < 0.0
    if col or out_of_bound:
      #print("#################### dropping sample #######################")
      return True
    return False        
  
  '''
    Filter out samples that should not be used for training (or testing).
  '''  
  


def test_loading_data():  
  #depth_image_dataset = DepthImageDataset(csv_file='/home/uli/research/thesis/datasets/dataset_7/labels/info_images.csv',
  #                                    root_dir='/home/uli/research/thesis/datasets/dataset_7/images/',
  #                                    transform=transforms.Compose([
  #                                        ToTensor()]))
  col_oob_value=10.0 # abs value for actions that lead to collision or out-of-bound
  depth_image_dataset = dataset_with_filter('/home/uli/research/thesis/datasets/dataset_34_closestGridpointFix_1000scenes/3',\
                                            col_oob_value=col_oob_value,use_filter=False, rm_collision=False, rm_out_of_bound=False, read_8bit_png=False, learn_best_q_value_only=False, best_q_value_margin = False, q_margin = False, noise_prob_min=0.0, noise_prob_max=0.15)
  #depth_image_dataset = dataset_with_filter('/home/uli/research/thesis/datasets/dataset_29_dist_penalty/2',col_oob_value=col_oob_value,use_filter=False, rm_collision=True, rm_out_of_bound=True, read_8bit_png=False)

  #depth_image_dataset = dataset_with_filter('/home/uli/research/thesis/datasets/dataset_9_complete_testing',col_oob_value=col_oob_value,use_filter=False, rm_collision=False, rm_out_of_bound=False, read_8bit_png=True)
  #depth_image_dataset = dataset_with_filter('/home/uli/research/thesis/datasets/dataset_12/1',col_oob_value=0.5,use_filter=False, rm_collision=False, rm_out_of_bound=False, read_8bit_png=True)

  
  # This keeps track of the range of q_values (what is the min and max of q-values in the dataset)
  q_min = float("inf")
  q_max = -float("inf")
  
  #for i in range(len(depth_image_dataset)):
  #  sample = depth_image_dataset[i]    
  #  if q_min > sample['q_values_flat'].min() and -col_oob_value < sample['q_values_flat'].min():
  #    q_min = sample['q_values_flat'].min()
  #    print("i", i, "q_min", q_min)     
  #  if q_max < sample['q_values_flat'].max():
  #    q_max = sample['q_values_flat'].max()
  #  if i%10000 == 0:
  #    print("i", i)
  #print("q_min", q_min)
  #print("q_max", q_max)  
  
  # skip this for now
  for i in range(5):
      sample = depth_image_dataset[i]
      print(i, sample['image'].shape, sample['q_values'].shape)
      print("sample['image'][0,0]", sample['image'][0,0])
      print("sample['traj_no']", sample['traj_no'])      
      print("sample['q_values']", sample['q_values'])
      print("sample['q_values_flat']", sample['q_values_flat'])
      print("min q-value", sample['q_values_flat'].min())
      if q_min > sample['q_values_flat'].min() and -col_oob_value < sample['q_values_flat'].min():
        q_min = sample['q_values_flat'].min()
      if q_max < sample['q_values_flat'].max():
        q_max = sample['q_values_flat'].max()
      print("max q-value", sample['q_values_flat'].max())      
      # Warning, q_values might contain -1 as value, which are the invalid actions. If q_values contains -1, then
      # the following would return the first index with value -1
      #ind = np.unravel_index(np.argmin(sample['q_values'], axis=None), sample['q_values'].shape)
    
      best_index =  sample['best_index']
      print("sample['best_index']", best_index)
     
      print("sample['best_action']", sample['best_action'])
      best_action_indices = sample['best_action_indices']
      print("sample['best_action_indices']", best_action_indices) 
      best_action_indices = tuple(best_action_indices)
      print("sample['q_values'][sample['best_action_indices']]", sample['q_values'][best_action_indices])
      print("sample['q_values_flat'][sample['best_index']]", sample['q_values_flat'][best_index])
      assert(sample['q_values'][best_action_indices] == sample['q_values_flat'][best_index]), "best q-value not the same for flat and none-flat arrays"      
      #print("#############################################################")
      #Example output:
      #  sample['q_values'] [[[0.05975 0.05657 0.05975]
      #  [0.05561 0.05243 0.0556 ]
      #  [0.05147 0.04828 0.05146]]
      #
      # [[0.06975 0.06657 0.06974]
      #  [0.06561 0.06243 0.0656 ]
      #  [0.06147 0.05828 0.06146]]
      #
      # [[0.07976 0.07658 0.07974]
      #  [0.07561 0.07242 0.0756 ]
      #  [0.07146 0.06828 0.07145]]]
      #sample['best_index'] 7
      #sample['best_action'] [ 0.    0.01 -0.01]
      #sample['best_action_indices'] (0, 2, 1)
      #sample['q_values'][sample['best_index']] 0.04828
      #############################################################
          
      #print("sample['image'][32,32]", sample['image'][32,32])
      #plt.imshow(sample['image'])
      #plt.show()
      

  
  
  dataloader = DataLoader(depth_image_dataset, batch_size=4,
                          shuffle=True, num_workers=4)
  return dataloader

# Helper function to show a batch
def show_batch(sample_batched):
    """Show image with landmarks for a batch of samples."""
    images_batch = sample_batched['image']
    batch_size = len(images_batch)

    grid = utils.make_grid(images_batch)
    print("grid.numpy().transpose((1, 2, 0)).shape", grid.numpy().transpose((1, 2, 0)).shape)
    plt.imshow(grid.numpy().transpose((1, 2, 0)))

    for i in range(batch_size):
        plt.title('Batch from dataloader')

def test_visualize_data_from_dataloder(dataloader):      
  for i_batch, sample_batched in enumerate(dataloader):
      #print("i_batch", i_batch)
      #print("sample_batched['best_action_indices']", sample_batched['best_action_indices'])
  
      # observe 4th batch and stop.
      if i_batch == 3:
          plt.figure()
          show_batch(sample_batched)
          plt.axis('off')
          plt.ioff()
          plt.show()
          break

def train(log_interval, model,classification, device, train_loader, optimizer, epoch):
    model.train()
    #for batch_idx, (data, target) in enumerate(train_loader):
    
    for batch_idx, sample_batched in enumerate(train_loader):
        data = sample_batched['image']
        #print("data", data.shape)
        if classification:        
          target = sample_batched['best_index']
          target = target.to(device,dtype=torch.long)
        else:
          target = sample_batched['q_values_flat']
          target = target.to(device,dtype=torch.float)
        data = data.to(device,dtype=torch.float)
        #print("data", data.shape)  
        
        optimizer.zero_grad()
        output = model(data)
        if classification:          
          loss = F.nll_loss(output, target)
        else:
          #scale = 4
          #loss_debug = F.smooth_l1_loss(output, target, reduction='none')
          #print("loss_debug", loss_debug)
          #loss_debug2 = F.smooth_l1_loss(scale*output, scale*target, reduction='none')
          #print("loss_debug2", loss_debug2)          
          loss = F.smooth_l1_loss(output, target)
          #print("loss", loss)
            
        loss.backward()
        optimizer.step()
        #print(model.fc1.weight.grad)
        #input("press a key")
        if batch_idx % log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.item()))
            pass

def test(model, classification, device, data_loader,loss=[],acc=[],train_set=False):
    model.eval()
    test_loss = 0
    correct = 0
    num_samples = 0     
    with torch.no_grad():
        #for data, target in test_loader:
        for sample_batched in data_loader:
            data = sample_batched['image']
            num_samples += data.size()[0]            
            if classification:
              target = sample_batched['best_index']
              target = target.to(device,dtype=torch.long)              
            else:
              target = sample_batched['q_values_flat']
              target = target.to(device,dtype=torch.float)              
            data = data.to(device,dtype=torch.float)
            output = model(data)
            pred = output.argmax(dim=1, keepdim=True) # get the index of the max log-probability, or index of max q-value            
            if classification:
              test_loss += F.nll_loss(output, target, reduction='sum').item() # sum up batch loss
              correct += pred.eq(target.view_as(pred)).sum().item()
            else:
              test_loss += F.smooth_l1_loss(output, target, reduction='sum').item() # sum up batch loss
              target_max_q_ind = target.argmax(dim=1, keepdim=True)
              correct += pred.eq(target_max_q_ind.view_as(pred)).sum().item()          
            
             # this works for classification and prediction
            if num_samples >= 10000: # test for 10k samples only for training data, because test on full set takes too much time
              #print("num_samples", num_samples)
              break

    test_loss /= num_samples
    if train_set:
      output_string = '\nTrain set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'
    else:
      output_string = '\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'
      
    print(output_string.format(
        test_loss, correct, num_samples,
        100. * correct / num_samples))
    loss.append(test_loss)
    acc.append(100. * correct / num_samples)
    return loss, acc    


def plot_loss_acc(epochs, loss_train, acc_train, loss_test, acc_test, loss_test2=None, acc_test2=None, save_to_file=False,output_path=''):
  # Create some mock data
  #t = np.arange(0.01, 10.0, 0.01)
  #data1 = np.exp(t)
  #data2 = np.sin(2 * np.pi * t)
  #data3 = 0.5*np.exp(t)
  #data4 = np.cos(2 * np.pi * t)
  
  epochs = np.array(epochs)
  loss_train = np.array(loss_train)
  acc_train = np.array(acc_train)
  loss_test = np.array(loss_test)  
  acc_test = np.array(acc_test)
  
  fig, ax1 = plt.subplots()

  ax1.set_xlabel('epochs')
  ax1.set_ylabel('loss', color='tab:red')
  ax1.plot(epochs, loss_train, color='tab:red',label="loss_train")
  ax1.plot(epochs, loss_test, color='tab:orange',label="loss_test")
  if loss_test2 != None:
    ax1.plot(epochs, loss_test2, color='tab:orange',linestyle = '--', label="loss_test2_noClutter")  
  ax1.tick_params(axis='y', labelcolor='tab:red')
  plt.legend(loc="lower left")
  
  ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
  
  color = 'tab:blue'
  ax2.set_ylabel('accuracy', color=color)  # we already handled the x-label with ax1
  ax2.plot(epochs, acc_train, color='tab:blue',label="acc_train")
  ax2.plot(epochs, acc_test, color='tab:purple',label="acc_test")
  if acc_test2 != None:
    ax2.plot(epochs, acc_test2, color='tab:purple',linestyle = '--',label="acc_test2_noClutter")  
  ax2.tick_params(axis='y', labelcolor='tab:blue')
  plt.legend(loc="lower right")
  ax1.xaxis.set_major_locator(MaxNLocator(integer=True))
  
  fig.tight_layout()  # otherwise the right y-label is slightly clipped
  plt.show()
  if save_to_file:
    fig.savefig(os.path.join(output_path,'plot_loss_acc.png'))
 

def create_logging_files(output_path, git_location):
  '''
    Setup logging files for relevant info for supervised learning
    Also backup script files.
  '''

  if(os.path.exists(output_path) == False): #check if directory exist
      os.makedirs(output_path)
  else:
    print("Warning: Folder already exists", output_path)
    input("Press a key to continue")
  
  
  file_plot = open(os.path.join(output_path,"plot.csv"), "a")    
  #list_of_lists = [epochs,loss_train[-1],acc_train[-1],loss_test[-1],acc_test[-1],loss_test2[-1],acc_test2[-1]]
  headers_plot = ["epoch", "loss_train", "acc_train", "loss_test","acc_test","loss_test2","acc_test2"]
  scene.write_list_to_txt_file(file_plot,headers_plot)  
  file_plot.flush()

  
  # copy main scripts and config files (as backup)
  file_main_script = os.path.join(git_location,"supervised_learning/training.py")  
  
  scene_gen_utils = os.path.join(git_location,"scripts/scene_gen_utils.py")
  vrep_utils = os.path.join(git_location,"utils/vrep_utils.py")
  config_file = os.path.join(git_location,"supervised_learning/config.ini")  
  filter_data = os.path.join(git_location,"supervised_learning/filter_data.py")
  files_to_copy = [file_main_script,scene_gen_utils,vrep_utils,filter_data,config_file]
  scene.copy_files_to_path(files_to_copy,output_path)  
  return file_plot
  
def data_to_CSV(data_lists,file):
  '''Write data to CSV file.
  
  Inputs:
    - data_lists: list of list with data (floats,strings etc)
    - file: the file handle to write to
  '''
  # Check that len of lists are all the same
  for l in data_lists:
    assert len(data_lists[0]) == len(l), "data_lists in data_to_CSV not equal lengths"
  
  # Transpose lists
  data_lists_t = [list(x) for x in zip(*data_lists)]

  #  
  for line in data_lists_t:
    for item in line:
      file.write(str(item))
    file.write('\n')


def dataset_with_filter(root_dir,
                        col_oob_value=1.0,
                        use_filter=False,
                        rm_collision=False,
                        rm_out_of_bound=True,
                        rm_target_inv=True,
                        rm_obj_inv=True,
                        rm_if_no_good_q=True,
                        read_8bit_png=False,
                        learn_best_q_value_only=False,
                        best_q_value_margin=False,
                        q_margin=0.0,
                        add_noise=False,
                        noise_prob_min=0.0,
                        noise_prob_max=0.15,
                        max_scenes=None
                        ):
  '''
    wrapper to create a filtered (removed specific samples) dataset
    
    Inputs:
      - root_dir: Folder where "labels" and "images" folders are located
      - use_filter: Specifies if filtered CSV should be generated and used
  '''
  
  print(root_dir)
  if root_dir[-1] != '/':
    root_dir = root_dir + '/'

  csv_in = os.path.join(root_dir, "labels/info_images.csv")
    
  # generate CSV file with filtered data
  if use_filter:
    csv_out = os.path.join(root_dir, "labels/info_images_filtered.csv")
    fd.remove_samples(csv_in, csv_out, rm_collision, rm_out_of_bound, rm_target_inv, rm_obj_inv, rm_if_no_good_q, max_scenes, col_oob_value)
    csv_in = csv_out # use filtered CSV for dataset
  
  img_folder = root_dir + "images/"  
  
  dataset = DepthImageDataset(csv_file=csv_in,
                                    root_dir=img_folder,
                                    col_oob_value=col_oob_value,
                                    transform=transforms.Compose([
                                               #DataFilter(),
                                               ToTensor(col_oob_value,learn_best_q_value_only,
                                                        best_q_value_margin, q_margin,
                                                        add_noise,noise_prob_min,noise_prob_max)]),                                 
                                    read_8bit_png=read_8bit_png)  
  
  return dataset

def main():
  # Some test of loading data from CSV
  # dataloader = test_loading_data()
  # test_visualize_data_from_dataloder(dataloader)
  # input("press a key")
  # exit()



  # Training settings

  config = configparser.RawConfigParser()
  config.read('config.ini')   
  git_location = config.get('main', 'git_location')
  output_path = config.get('logging', 'output_path')
  print('output_path', output_path)
  save_model = config.getboolean('logging', 'save_model')
  log_interval = config.getint('logging', 'log_interval')
  
  classification = config.getboolean('training_param', 'classification')
  LR = config.getfloat('training_param', 'LR')
  train_batch_size =  config.getint('training_param', 'train_batch_size')
  test_batch_size =  config.getint('training_param', 'test_batch_size')
  n_epochs = config.getint('training_param', 'n_epochs')
  print("n_epochs", n_epochs)
  seed = config.getint('training_param', 'seed')
  load_pretrained_model = config.getboolean('training_param', 'load_pretrained_model')
  path_model = config.get('training_param', 'path_model')
  
  filter_data = config.getboolean('data_processing', 'filter_data') # If True then use following two flags
  rm_collision= config.getboolean('data_processing', 'rm_collision')
  rm_out_of_bound= config.getboolean('data_processing', 'rm_out_of_bound')
  rm_target_inv= config.getboolean('data_processing', 'rm_target_inv')
  rm_obj_inv= config.getboolean('data_processing', 'rm_obj_inv')
  if config.has_option('data_processing', 'rm_if_no_good_q'):
    rm_if_no_good_q = config.getboolean('data_processing', 'rm_if_no_good_q')
    print("Removing samples if rm_if_no_good_q")
  else:
    rm_if_no_good_q = False  
  
  

  
  if config.has_option('data_processing', 'max_scenes'):
    max_scenes = config.getint('data_processing', 'max_scenes')
    print("limiting to max_scenes", max_scenes)
  else:
    max_scenes = None

  col_oob_value = config.getfloat('data_processing', 'col_oob_value') # abs value for setting q-value for actions leading to collision or out-of-bound
  learn_best_q_value_only = config.getboolean('data_processing', 'learn_best_q_value_only')  
  if not rm_if_no_good_q and learn_best_q_value_only:
    input("WARNING! It is recommended to filter out samples with only bad q values (rm_if_no_good_q)"+\
          "if learning with learn_best_q_value_only.")

  if learn_best_q_value_only and config.has_option('data_processing', 'best_q_value_margin'):
    best_q_value_margin = config.getboolean('data_processing', 'best_q_value_margin')
    q_margin = config.getfloat('data_processing', 'q_margin')
  else:
    best_q_value_margin = False
    q_margin = 0.0

  print("learn_best_q_value_only", learn_best_q_value_only)
  print("filter_data: ", filter_data, "rm_collision: ", rm_collision,\
        "rm_out_of_bound: ", rm_out_of_bound, "col_oob_value: ", col_oob_value,\
        "rm_target_inv: ", rm_target_inv, "rm_obj_inv: ", rm_obj_inv, "rm_if_no_good_q", rm_if_no_good_q)


  add_noise = config.getboolean('image_processing', 'add_noise') 
  noise_prob_min = config.getfloat('image_processing', 'noise_prob_min')
  noise_prob_max = config.getfloat('image_processing', 'noise_prob_max')

  n_train_sets = config.getint('datasets', 'n_train_sets')
  train_sets = []
  train_set_8bit_flags = []
  
  for no in range(n_train_sets):
    train_sets.append(config.get('datasets', 'train_set_' + str(no)))
    train_set_8bit_flags.append(config.getboolean('datasets', 'train_set_8bit_flag_' + str(no)))
      
  n_test_sets = config.getint('datasets', 'n_test_sets')
  test_sets = []   
  test_set_8bit_flags = []
 
  for no in range(n_test_sets):
    test_sets.append(config.get('datasets', 'test_set_' + str(no)))
    test_set_8bit_flags.append(config.getboolean('datasets', 'test_set_8bit_flag_' + str(no)))
 
  use_GPU = config.getboolean('device', 'use_GPU')
  GPU_no = config.getint('device', 'GPU_no')

  torch.manual_seed(seed)

  file_out = create_logging_files(output_path, git_location)

  
  cuda_device = "cuda:" + str(GPU_no)
  device = torch.device(cuda_device if (torch.cuda.is_available() and use_GPU) else "cpu") 
  
  datasets_train = []
  for train_set, train_set_8bit_flag in zip(train_sets,train_set_8bit_flags):
    datasets_train.append(dataset_with_filter(train_set,
                                              col_oob_value=col_oob_value,
                                              use_filter=filter_data,
                                              rm_collision=rm_collision,
                                              rm_out_of_bound=rm_out_of_bound,
                                              rm_target_inv=rm_target_inv,
                                              rm_obj_inv=rm_obj_inv,
                                              rm_if_no_good_q=rm_if_no_good_q,
                                              read_8bit_png=train_set_8bit_flag,
                                              learn_best_q_value_only=learn_best_q_value_only,
                                              best_q_value_margin=best_q_value_margin,
                                              q_margin=q_margin,
                                              add_noise=add_noise,
                                              noise_prob_min=noise_prob_min,
                                              noise_prob_max=noise_prob_max,
                                              max_scenes=max_scenes))

  datasets_test = []
  for test_set, test_set_8bit_flag in zip(test_sets,test_set_8bit_flags):
    datasets_test.append(dataset_with_filter(test_set,
                                             col_oob_value=col_oob_value,
                                             use_filter=filter_data,
                                             rm_collision=rm_collision,
                                             rm_out_of_bound=rm_out_of_bound,
                                             rm_target_inv=rm_target_inv,
                                             rm_obj_inv=rm_obj_inv,
                                             rm_if_no_good_q=rm_if_no_good_q,                                             
                                             read_8bit_png=test_set_8bit_flag,
                                             learn_best_q_value_only=learn_best_q_value_only,
                                             best_q_value_margin=best_q_value_margin,
                                             q_margin=q_margin,                                
                                             add_noise=add_noise,
                                             noise_prob_min=noise_prob_min,
                                             noise_prob_max=noise_prob_max,
                                             max_scenes=max_scenes))

  
  #read_8bit_png = True
  #dataset_train = dataset_with_filter('/home/uli/research/thesis/datasets/dataset_8_complete_training',col_oob_value=col_oob_value, use_filter=filter_data, rm_collision=rm_collision, rm_out_of_bound=rm_out_of_bound,read_8bit_png=read_8bit_png)
  
  #dataset_train2 = dataset_with_filter('/home/uli/research/thesis/datasets/dataset_10',col_oob_value=col_oob_value, use_filter=filter_data, rm_collision=rm_collision, rm_out_of_bound=rm_out_of_bound,read_8bit_png=read_8bit_png)
  

  #dataset_train3 = dataset_with_filter('/home/uli/research/thesis/datasets/dataset_11_gui_disabled/0',col_oob_value=col_oob_value,use_filter=filter_data, rm_collision=rm_collision, rm_out_of_bound=rm_out_of_bound,read_8bit_png=read_8bit_png)
  
  #dataset_train4 = dataset_with_filter('/home/uli/research/thesis/datasets/dataset_12/1',col_oob_value=col_oob_value,use_filter=filter_data, rm_collision=rm_collision, rm_out_of_bound=rm_out_of_bound,read_8bit_png=read_8bit_png)

  #dataset_train5 = dataset_with_filter('/home/uli/research/thesis/datasets/dataset_14/0',col_oob_value=col_oob_value,use_filter=filter_data, rm_collision=rm_collision, rm_out_of_bound=rm_out_of_bound,read_8bit_png=read_8bit_png)

  #dataset_train6 = dataset_with_filter('/home/uli/research/thesis/datasets/dataset_16/0',col_oob_value=col_oob_value,use_filter=filter_data, rm_collision=rm_collision, rm_out_of_bound=rm_out_of_bound,read_8bit_png=read_8bit_png)
  
  #read_8bit_png = True  
  #dataset_test = dataset_with_filter('/home/uli/research/thesis/datasets/dataset_9_complete_testing',col_oob_value=col_oob_value,use_filter=filter_data, rm_collision=rm_collision, rm_out_of_bound=rm_out_of_bound, read_8bit_png=read_8bit_png)

  #read_8bit_png = False  
  #dataset_test2 = dataset_with_filter('/home/uli/research/thesis/datasets/dataset_19_no_clutter_DQN_16bit_images/0',col_oob_value=col_oob_value,use_filter=filter_data, rm_collision=rm_collision, rm_out_of_bound=rm_out_of_bound,read_8bit_png=read_8bit_png)  
  


  train_loaders = []
  for dataset_train in datasets_train:
    train_loaders.append(DataLoader(dataset_train, batch_size=train_batch_size,
                          shuffle=True, num_workers=20))
    test_visualize_data_from_dataloder(train_loaders[-1])
    
  test_loaders = []
  for dataset_test in datasets_test:
    test_loaders.append(DataLoader(dataset_test, batch_size=test_batch_size,
                          shuffle=True, num_workers=20))
    test_visualize_data_from_dataloder(test_loaders[-1])    
  #train_loader = DataLoader(dataset_train, batch_size=train_batch_size,
  #                        shuffle=True, num_workers=20)
  
  #train_loader2 =DataLoader(dataset_train2, batch_size=train_batch_size,
  #                        shuffle=True, num_workers=20)  
  
  #train_loader3 = DataLoader(dataset_train3, batch_size=train_batch_size,
  #                        shuffle=True, num_workers=20)
  
  #train_loader4 = DataLoader(dataset_train4, batch_size=train_batch_size,
  #                        shuffle=True, num_workers=20)  
  
  #train_loader5 = DataLoader(dataset_train5, batch_size=train_batch_size,
  #                        shuffle=True, num_workers=20)  
  
  #train_loader6 = DataLoader(dataset_train6, batch_size=train_batch_size,
  #                        shuffle=True, num_workers=20)   
   
  #test_loader = DataLoader(dataset_test, batch_size=test_batch_size,
  #                        shuffle=True, num_workers=20)
  
  #test_loader2 = DataLoader(dataset_test2, batch_size=test_batch_size,
  #                        shuffle=True, num_workers=20)  
  
#  train_loader = torch.utils.data.DataLoader(
#    datasets.MNIST('../data', train=True, download=True,
#                   transform=transforms.Compose([
#                       transforms.ToTensor(),
#                       transforms.Normalize((0.1307,), (0.3081,))
#                   ])),
#    batch_size=args.batch_size, shuffle=True, **kwargs)
#  test_loader = torch.utils.data.DataLoader(
#    datasets.MNIST('../data', train=False, transform=transforms.Compose([
#                       transforms.ToTensor(),
#                       transforms.Normalize((0.1307,), (0.3081,))
#                   ])),
#    batch_size=args.test_batch_size, shuffle=True, **kwargs)


  #model = DQN(64,64).to(device) # DQN
  model = DQN_lenet(classification).to(device) # lenet


  if load_pretrained_model:
    state_dict = torch.load(path_model)
    model.load_state_dict(state_dict)
  
  #optimizer = optim.SGD(model.parameters(), lr=args.lr, momentum=args.momentum)
  optimizer = optim.Adam(model.parameters(), lr=LR) # seems to work well for classification and regression
  #optimizer = optim.RMSprop(model.parameters(), lr=0.0001)
  #optimizer = optim.Adadelta(model.parameters()) # doesn't work for classification


  loss_train = []
  acc_train = []
  loss_test = []
  acc_test = []
  loss_test2 = []
  acc_test2 = []  

    
  epochs = [] # for x-axis 
  for epoch in range(1, n_epochs + 1):
      for train_loader in train_loaders:
        train(log_interval, model, classification, device, train_loader, optimizer, epoch)  
 
      loss_train, acc_train = test(model, classification, device, train_loaders[0],loss_train,acc_train,train_set=True)
      loss_test, acc_test = test(model, classification, device, test_loaders[0],loss_test,acc_test)
      if len(test_loaders) > 1:        
        loss_test2, acc_test2 = test(model, classification, device, test_loaders[1],loss_test2,acc_test2)
      else:
        loss_test2 = loss_test
        acc_test2 = acc_test      
      if len(test_loaders) > 2:              
        test(model, classification, device, test_loaders[2])  # only printing to terminal
      epochs.append(epoch)
      if epoch == n_epochs:
        save_to_file = True
        print ("Saving plot after final epoch.")
      else:
        save_to_file = False
      plot_loss_acc(epochs,loss_train,acc_train,loss_test,acc_test,loss_test2,acc_test2,save_to_file,output_path)
      list_of_lists = [epoch,loss_train[-1],acc_train[-1],loss_test[-1],acc_test[-1],loss_test2[-1],acc_test2[-1]]
      scene.write_list_of_lists_to_txt_file(file_out,list_of_lists)

      
      
      #input("Press a key")
  


      if(save_model and epoch % 10 == 0):
        if classification:
          torch.save(model.state_dict(),os.path.join(output_path,"policy_cnn_3D_pos_" + str(epoch).zfill(3) + "_GPU" + str(GPU_no) + ".pt"))
        else:
          torch.save(model.state_dict(),os.path.join(output_path,"q_net_cnn_3D_pos_"  + str(epoch).zfill(3) + "_GPU" +  str(GPU_no) + ".pt"))        
  if (save_model):
    if classification:
      torch.save(model.state_dict(),os.path.join(output_path,"policy_cnn_3D_pos_GPU" + str(GPU_no) + ".pt"))
    else:
      torch.save(model.state_dict(),os.path.join(output_path,"q_net_cnn_3D_pos_GPU" + str(GPU_no) + ".pt"))

  file_out.close()
  
if __name__ == '__main__':
    main()
  