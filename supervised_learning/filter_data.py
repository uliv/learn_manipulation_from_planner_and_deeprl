#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  2 15:03:51 2019

filter_data.py

Filter out samples from CSV file based on specific criteria
(e.g. states near collisions or out-of-bound)

Input: CSV file with images and annotations
Output: CSV file as input, but removed samples that meet a specific criteria

For an example CSV file see:
  /home/uli/research/thesis/datasets/dataset_7/labels/info_images.csv
  
The headers (columns) are:
0: image_name
1: scene_no
2: traj_no
3: img_no
4: q_values
5: best_index
6: best_action
7: pos_target_in_robot
8: orient_target_in_robot
9: NumPixelsVisible
  

@author: uli
"""

import numpy as np

def check_no_good_q(q_values, col_oob_value=0.5):
    q_values = np.asarray(q_values, dtype=np.float32)
    # col_oob_value is positive
    return not((q_values >= 0.0) & (q_values < col_oob_value)).any()

def remove_samples(csv_in,csv_out,
                                  rm_collision=True,
                                  rm_out_of_bound=True,
                                  rm_target_inv=True,
                                  rm_obj_inv=True,
                                  rm_if_no_good_q=True,
                                  max_scenes=None):
  with open(csv_in, 'r' ) as f_in: 
    lines_in = f_in.readlines()
    


  with open(csv_out, 'w') as f_out:
    for i,l in enumerate(lines_in):
      if i == 0: # copy header line
        f_out.write(l)
        continue
      
      # create np.array for q_values
      sp = lines_in[i].split(',')
      q_values = sp[4].split(' ') # 4: q_values are space seperated, convert to a list
      q_values = np.asarray(q_values, dtype=np.float32)
      

      
      # check if values indicate collision or out-of-bound
      col = np.amax(q_values) >= 1.0
      out_of_bound = np.min(q_values) < 0.0
      
      #values_in_0_1 = q_values[np.where( q_values >= 0.0) and np.where(q_values < 1.0 )]      
      #values_in_0_0p5 = q_values[np.where( q_values >= 0.0) and np.where(q_values < 0.5 )]
      no_good_q = check_no_good_q(q_values,col_oob_value)
      
      #if no_good_q:
      #  print("filter_data, q_values", q_values)        
#     #   print("values in 0-1", values_in_0_1)
#     #   print("np.amax(q_values)", np.amax(q_values))
#     #   print("np.min(q_values)", np.min(q_values))        
      #  input("press a key")
      NumPixelsVisibleTarget = int(float(sp[9]))
      target_inv = NumPixelsVisibleTarget < 16
      NumPixelsVisibleObj = int(float(sp[10]))
      obj_inv = NumPixelsVisibleObj < 16
      
      scene_no = int(float(sp[1]))
      
      if (col and rm_collision)\
          or (out_of_bound and rm_out_of_bound)\
          or (target_inv and rm_target_inv)\
          or (no_good_q and rm_if_no_good_q)\
          or (obj_inv and rm_obj_inv):
        # skip sample (filter out)
        continue
      
      if max_scenes != None:
        if scene_no >= max_scenes:
        # skip sample (filter out)          
          continue
      
      f_out.write(l)     
   

if __name__ == '__main__':
  # test of the script
  #csv_in = '/home/uli/research/thesis/datasets/dataset_7/labels/info_images_copy.csv'
  #csv_out = '/home/uli/research/thesis/datasets/dataset_7/labels/info_images_copy_filtered.csv'
  #remove_samples_col_and_bounds(csv_in, csv_out)

  csv_in = '/home/uli/research/thesis/datasets/dataset_27_no_clutter_DQN_dist_penalty_grid_into_table/0/labels/info_images_copy.csv'
  csv_out = '/home/uli/research/thesis/datasets/dataset_27_no_clutter_DQN_dist_penalty_grid_into_table/0/labels/info_images_copy_filtered.csv'
  rm_collision=False
  rm_out_of_bound=True
  rm_target_inv=True
  rm_obj_inv=True
  remove_samples_col_and_bounds(csv_in, csv_out, rm_collision, rm_out_of_bound, rm_target_inv, rm_obj_inv)

