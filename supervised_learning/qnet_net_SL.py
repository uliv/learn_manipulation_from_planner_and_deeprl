#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 3 12:18 2019

Script that uses a trained regression model (CNN learned the q-values for actions and observations)

Predicts all the q-values for all action for a given input image

Model is defined in training.py

@author: uli
"""

import sys
import numpy as np
import training
import torch
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
sys.path.append('./')
sys.path.append('..')
sys.path.append('../simulation')
import utils.vrep_utils as utils  

class qnet_net_SL(object):
  '''
    Class for a q-network trained using supervised learning
    This is used to make predictions of q-values
  '''
  def __init__(self,path_model,use_cuda=True):
    self.device = torch.device("cuda" if use_cuda else "cpu")
    self.model = training.DQN_lenet().to(self.device)      
    self.load_model(path_model)
    self.model.eval()
  
  def load_model(self,path_model):
    '''
      Loads the learned weigths from file
    '''
    state_dict = torch.load(path_model)
    self.model.load_state_dict(state_dict)

  def predict(self,image):
    '''
      Predicts the q-values for a given input image (as PyTorch Tensor)
      Return the q-values of all actions (as PyTorch Tensor)
    '''
    #image = ToTensor(image)
    with torch.no_grad():
      image = image.to(self.device,dtype=torch.float)
      output =self.model(image)
      #pred = output.argmax(dim=1, keepdim=True) # get the index of the max log-probability
    return output#.item()
  
  
def ToTensor(image):
  '''
    convert the numpy image to torch image (Tensor)
  '''
  image = image[:,:,0]
  image = image[..., np.newaxis]
  image = image[..., np.newaxis] # add 2 axis one for batch, one for color
  # numpy image: H x W x C X BATCH
  # torch image: BATCH X C X H x W
  
  image = image.transpose((3, 2, 0, 1))
  image = np.ascontiguousarray(image, dtype=np.float32) 
  image = torch.from_numpy(image)
  return image    # Resize, and add a batch dimension (BCHW)