#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 15:36:29 2019

@author: uli

Analyze the q-value predictions of a net trained using supervised learning.

This script is similar to "training.py" (used for supervised learning).

"""

from __future__ import print_function, division
import os
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils
import argparse
import filter_data as fd
import qnet_net_SL as qnet
from PIL import Image
import training as t
import utils.vrep_utils as utils



# Ignore warnings
import warnings
warnings.filterwarnings("ignore")

plt.ion()   # interactive mode

def print_q_values(q_values):
  '''
    Prints the q-values of a tensor to screen
    Inputs
      - q_values: batch of tensors q_values
    
    Returns:
      None
  '''  
  q_values_np = convert_q_values2np(q_values)
  print(q_values_np)
  argmax = np.argmax(q_values_np)
  action = utils.actionIndexToAction(argmax,0.01,0.01,0.01)   
  print("action: ", action)
  return action, argmax


def convert_q_values2np(q_values):
  q_values_np = q_values.cpu().detach()[0].numpy() # convert first batch
  return q_values_np  

  
def plot_q_values(target,preds,save_to_file=False,model_labels=[]):
  action_indices = np.array(range(len(target)))
  
  fig, ax1 = plt.subplots()

  ax1.set_xlabel('action_indices')
  ax1.set_ylabel('q-value')
  ax1.plot(action_indices, target, color='b',label="target (gt)")
  ax1.plot(np.argmax(target), np.max(target), '-bD')
  ax1.tick_params(axis='y')

  plt.ylim(-0.60, 0.1)   
  colors = ['r','k','y','g']
  diamonds = ['-rD','-kD','-yD','-gD']
  for i,pred in enumerate(preds):
    if i < len(colors):
      if np.max(pred) > 0.2 or np.min(pred) < -0.8: # plotting something that doesn't fit into first y-axis scale
        ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
        ax2.set_ylabel('policy-net value', color=colors[i])  # we already handled the x-label with ax1
        if i < len(model_labels):
          ax2.plot(action_indices, pred, color=colors[i], label=model_labels[i])          
        else:
          ax2.plot(action_indices, pred, color=colors[i], label="pred_"+str(i))
        ax2.tick_params(axis='y', labelcolor=colors[i])
        ax2.plot(np.argmax(pred), np.max(pred), diamonds[i])
        plt.legend(loc="lower right")
      else:
        if i < len(model_labels):
          ax1.plot(action_indices, pred, color=colors[i], label=model_labels[i])          
        else:
          ax1.plot(action_indices, pred, color=colors[i], label="pred_"+str(i))
        ax1.plot(np.argmax(pred), np.max(pred), diamonds[i])
        plt.legend(loc="lower left") 
    else:
      ax1.plot(action_indices, pred, label="pred_"+str(i))
      ax1.plot(np.argmax(pred), np.max(pred))
     
    
  #plt.legend(loc="lower left")
  
  
  #ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
  #color = 'tab:red'
  #ax2.set_ylabel('q-value pred', color=color)  # we already handled the x-label with ax1
  #ax2.plot(action_indices, pred,color='tab:red',label="pred")
  #ax2.plot(np.argmax(pred), np.max(pred), '-rD')  
  #ax2.tick_params(axis='y', labelcolor='tab:red')
  
  ax1.xaxis.set_major_locator(MaxNLocator(integer=True))
  
  fig.tight_layout()  # otherwise the right y-label is slightly clipped
  plt.show()
  if save_to_file:
    fig.savefig('q-values.png')

# for: '/home/uli/CloudStation/thesis_work/SL_training_results/06-17-19_02_closestGridPoint_policy_with_col/policy_cnn_3D_pos_GPU1.pt',  
# indices_wrong_policy_net = [85, 86, 87, 96, 97, 98, 100, 103, 106, 136, 137, 146, 147, 157, 167, 176, 177, 183, 275, 276, 277, 286, 287, 290, 366, 465, 466, 467, 477, 556, 655, 656, 657, 667, 746, 845, 846, 847, 857, 926, 935, 936, 1034, 1035, 1210, 1219, 1220, 1230, 1388, 1389, 1398, 1399, 1409, 1419, 1534, 1542, 1552, 1562, 1572, 1649, 1659, 1680, 1689, 1699, 1760, 1762, 1763, 1775, 1834, 1835, 1844, 1864, 1982, 1992, 1993, 2002, 2003, 2013, 2161, 2171, 2172, 2181, 2182, 2530, 2535, 2536, 2714, 2719, 2720, 2721, 2724, 2725, 2728, 2904, 2908, 2909, 2910, 2911, 2912, 2914, 2915, 2918, 2919, 3014, 3015, 3024, 3094, 3098, 3099, 3100, 3101, 3102, 3103, 3104, 3108, 3109, 3110, 3204, 3205, 3206, 3212, 3213, 3214, 3221, 3222, 3223, 3231, 3232, 3233, 3241, 3242, 3285, 3286, 3287, 3288, 3289, 3291, 3292, 3295, 3296]

# For super finetuned/overfit net
# '/home/uli/CloudStation/thesis_work/SL_training_results/06-19-19_04_closestGridPoint_policy_not_filtered_onlyFinetuneOnTestSetNoClutter_pretrain_06-19-19_03/policy_cnn_3D_pos_GPU1.pt'
#  col_oob_value = 0.5
#  filter_data = True
#  rm_collision = False
#  rm_out_of_bound = True
#  rm_target_inv = True
#  rm_obj_inv = True
# indices_wrong_policy_net = [] # 100% accuracy

#on not filtered test set:
#indices_wrong_policy_net = [0, 1, 2, 3, 4, 5, 12, 13, 14, 15, 16, 24, 25, 26, 27, 36, 37, 38, 48, 49, 60, 72, 84, 96, 108, 120, 132, 144, 156, 168, 180, 192, 204, 216, 228, 240, 252, 253, 254, 264, 265, 276, 288, 300, 312, 324, 336, 348, 360, 372, 384, 396, 408, 420, 432, 444, 456, 468, 480, 492, 504, 516, 528, 540, 552, 564, 576, 588, 600, 612, 624, 636, 648, 660, 672, 684, 696, 708, 720, 732, 744, 756, 768, 780, 792, 804, 816, 828, 840, 852, 864, 876, 888, 900, 912, 924, 936, 948, 960, 972, 984, 996, 1008, 1020, 1032, 1044, 1056, 1068, 1080, 1092, 1104, 1116, 1128, 1140, 1152, 1164, 1176, 1188, 1200, 1212, 1224, 1236, 1248, 1260, 1272, 1284, 1296, 1308, 1320, 1332, 1344, 1356, 1368, 1380, 1392, 1404, 1416, 1428, 1440, 1452, 1464, 1476, 1488, 1500, 1512, 1524, 1536, 1548, 1560, 1572, 1584, 1596, 1608, 1620, 1632, 1644, 1656, 1668, 1680, 1692, 1704, 1716, 1728, 1740, 1752, 1764, 1776, 1788, 1800, 1812, 1824, 1836, 1848, 1860, 1872, 1884, 1896, 1908, 1920, 1932, 1944, 1956, 1968, 1980, 1992, 2004, 2006, 2016, 2028, 2040, 2052, 2064, 2076, 2088, 2100, 2112, 2124, 2136, 2148, 2160, 2172, 2184, 2196, 2208, 2220, 2232, 2233, 2244, 2245, 2246, 2247, 2256, 2257, 2258, 2259, 2260, 2268, 2280, 2292, 2304, 2316, 2328, 2340, 2352, 2364, 2412, 2424, 2436, 2448, 2460, 2461, 2472, 2473, 2474, 2484, 2485, 2486, 2496, 2497, 2498, 2499, 2508, 2509, 2510, 2511, 2512, 2520, 2532, 2544, 2556, 2568, 2580, 2592, 2604, 2616, 2664, 2676, 2688, 2700, 2701, 2712, 2713, 2724, 2725, 2726, 2736, 2737, 2738, 2748, 2749, 2750, 2760, 2761, 2762, 2772, 2784, 2796, 2808, 2820, 2832, 2844, 2856, 2868, 2916, 2928, 2940, 2952, 2964, 2976, 2988, 3000, 3003, 3012, 3015, 3016, 3024, 3036, 3048, 3060, 3072, 3084, 3096, 3108, 3120, 3132, 3144, 3156, 3168, 3180, 3192, 3204, 3216, 3228, 3240, 3252, 3255, 3264, 3267, 3268, 3276, 3288, 3300, 3312, 3324, 3336, 3348, 3360, 3372, 3384, 3396, 3408, 3420, 3432, 3444, 3456, 3468, 3480, 3492, 3504, 3516, 3528, 3540, 3552, 3564, 3576, 3588, 3600, 3612, 3624, 3636, 3648, 3660, 3672, 3684, 3696, 3708, 3720, 3732, 3744, 3756, 3768, 3780, 3792, 3804, 3816, 3828, 3840, 3852, 3864, 3876, 3888, 3900, 3912, 3924, 3936, 3948, 3960, 3972, 3984, 3996, 4008, 4020, 4032, 4044, 4056, 4068, 4080, 4092, 4104, 4116, 4128, 4140, 4152, 4164, 4176, 4188, 4200, 4212, 4224, 4236, 4248, 4260, 4272, 4284, 4296, 4308, 4320, 4332, 4344, 4356, 4368, 4380, 4392, 4404, 4416, 4428, 4440, 4452, 4464, 4476, 4488, 4500, 4512, 4524, 4536, 4548, 4560, 4572, 4584, 4596, 4608, 4620, 4632, 4644, 4656, 4668, 4680, 4692, 4704, 4716, 4728, 4740, 4752, 4764, 4776, 4788, 4789, 4800, 4812, 4824, 4836, 4848, 4860, 4872, 4884, 4896, 4908, 4920, 4932, 4944, 4956, 4968, 4980, 4992, 5004, 5016, 5028, 5040, 5041, 5042, 5043, 5044, 5052, 5053, 5054, 5055, 5064, 5065, 5066, 5076, 5077, 5088, 5100, 5112, 5124, 5136, 5148, 5160, 5172, 5184, 5196, 5208, 5220, 5232, 5244, 5256, 5268, 5280]

# remove out of bound only, Acc: 0.99362 (=1-(23/3610))
#  filter_data = True
#  rm_collision = False
#  rm_out_of_bound = True
#  rm_target_inv = False
#  rm_obj_inv = False
#  read_8bit_png = False
indices_wrong_policy_net = [0, 1500, 1510, 1511, 1512, 1670, 1680, 1681, 1690, 1691, 1700, 1701, 1702, 1850, 1860, 1870, 1871, 1880, 1881, 1890, 1891, 2082, 2272]

def test(args, models, classification, device, data_loader,loss=[],acc=[],train_set=False,model_labels=[]):

    num_samples = 0 
    i = 0   
    losses = [0.0 for _ in models]
    correct = [0 for _ in models]
    wrong = [0 for _ in models]
    indices_wrong = [[] for _ in models]
    
    with torch.no_grad():
      for i,sample_batched in enumerate(data_loader):    
        #if i in indices_wrong_policy_net:
        if i%1 == 0:
            print("sample ", i)
            data = sample_batched['image']
            #t.show_batch(sample_batched)            
            plt.imshow(np.tile(data[0].numpy().transpose((1, 2, 0)),3))
            plt.show()
            num_samples += data.size()[0]            
            if classification:
              target = sample_batched['best_index']
              target = target.to(device,dtype=torch.long)              
            else:
              target = sample_batched['q_values_flat']
              print("targets")              
              action_target, argmax_target = print_q_values(target)
              target_np = convert_q_values2np(target)

              target = target.to(device,dtype=torch.float)              
            data = data.to(device,dtype=torch.float)
            #print("data", data) # this is data tensor([[[[ 84.,  84.,  84.,  ...,  84.
            outputs_np = []
            for j,model in enumerate(models):
              model.eval()
              output = model(data)
              print("predictions")
              action_pred, argmax_pred = print_q_values(output)
              output_np = convert_q_values2np(output)
              outputs_np.append(output_np)
              loss = F.smooth_l1_loss(output, target, reduction='sum').item()
              print("loss: ", loss)
              losses[j] += loss
              print("total loss", losses[j])
              if argmax_target == argmax_pred:
                correct[j] += 1
              else:
                wrong[j] += 1
                indices_wrong[j].append(i)
                
              accuracy = correct[j]/(correct[j]+wrong[j])
              print("accuracy: ", accuracy)
                   
            plot_q_values(target_np,outputs_np,model_labels=model_labels)
            if i%10 == 0:
              pass
              #print("indices_wrong", indices_wrong)
              #plot_q_values(target_np,output_np)
              #input("Press a key")       
            print('################################################') 
      print("indices_wrong", indices_wrong)



def imageToTensorBatch(image):
  '''
    convert the numpy image to torch image (Tensor)
  '''
  image = image[:,:,0]
  image = image[..., np.newaxis]
  image = image[..., np.newaxis] # add 2 axis one for batch, one for color
  # numpy image: H x W x C X BATCH
  # torch image: BATCH X C X H x W
  
  image = image.transpose((3, 2, 0, 1))
  image = np.ascontiguousarray(image, dtype=np.float32) 
  image = torch.from_numpy(image)
  return image    # Resize, and add a batch dimension (BCHW)

def main():
  # Some test of loading data from CSV
  #dataloader = test_loading_data()
  #test_visualize_data_from_dataloder(dataloader)

  use_GPU = True
  
  GPU_no = 0
  classification = False
  cuda_device = "cuda:" + str(GPU_no)
  device = torch.device(cuda_device if (torch.cuda.is_available() and use_GPU) else "cpu") 

  # test predictions for a single image using class qnet_net_SL
  ##################################
  '''
  image = '/home/uli/research/thesis/datasets/dataset_7/images/img_000000_000019_000000.png'  
  image = Image.open(image)
  image = np.array(image)
  image = imageToTensorBatch(image)
  path_model = '/home/uli/CloudStation/thesis_work/SL_training_results/'+\
    '05-03-19_02_continue_training_reduced_LR_more_epochs/'+\
    'q_net_cnn_3D_pos.pt'
  
  net = qnet.qnet_net_SL(path_model,use_cuda)  
  print(net.predict(image))
  '''

  # similar method as in training.py
  ###################################
  #paths_models = ['/home/uli/CloudStation/thesis_work/DQN_training_results/05-09-19_02_start_at_upper_half_planner_to_gen_episodes_Adam_2conv_3IP/target_net_1_14999.pt']
  paths_models = [
                  #'/home/uli/CloudStation/thesis_work/SL_training_results/06-06-19_01_qnet_dist_penalty_500epochs/q_net_cnn_3D_pos_GPU0.pt', # SL with dist penalty (less data)
                  #'/home/uli/CloudStation/thesis_work/SL_training_results/06-10-19_01_qnet_dist_penalty_2trainsets_testNoClutter/q_net_cnn_3D_pos_GPU0.pt', # SL with dist penalty (more data)
                  #'/home/uli/CloudStation/thesis_work/SL_training_results/06-11-19_01_qnet_dist_penalty_max_filtered_no_col/q_net_cnn_3D_pos_GPU0.pt', # SL no collision


                  #'/home/uli/CloudStation/thesis_work/SL_training_results/06-11-19_03_qnet_dist_penalty_max_filtered_scaled_output_target_4x/q_net_cnn_3D_pos_GPU0.pt', # SL no collision (4x target,pred in loss)
                  #'/home/uli/CloudStation/thesis_work/SL_training_results/06-11-19_04_qnet_dist_penalty_filtered_with_coll_scaled_output_target_4x/q_net_cnn_3D_pos_GPU1.pt'  # SL with collision (4x target,pred in loss)
                  #'/home/uli/CloudStation/thesis_work/DQN_training_results/06-03-19_01_plannerQtarget_no_distPenalty/policy_net_0_14999.pt', # planner q-next, no dist penalty
                  #'/home/uli/CloudStation/thesis_work/DQN_training_results/06-03-19_02_plannerQtarget_with_distPenalty/policy_net_1_14999.pt', # planner q-next, with dist penalty
                  #'/home/uli/CloudStation/thesis_work/SL_training_results/06-11-19_02_qnet_dist_penalty_filtered_with_col/q_net_cnn_3D_pos_GPU1.pt', # SL with collision
                  #'/home/uli/CloudStation/thesis_work/DQN_training_results/06-11-19_03_finetune_policy_net_scaled_output_smaller_LR/policy_net_0_14999.pt', # RL, finetunr policy net                  
                  #'/home/uli/CloudStation/thesis_work/SL_training_results/06-11-19_06_policy_dist_penalty_filtered_with_col/policy_cnn_3D_pos_GPU1.pt' # SL policy with collision

                  #'/home/uli/CloudStation/thesis_work/DQN_training_results/06-17-19_01_debug_DQN_baseline_after_closestGridPoint/policy_net_0_14999.pt',
                  #'/home/uli/CloudStation/thesis_work/DQN_training_results/06-17-19_01_debug_nextQ_after_closestGridPoint/policy_net_0_8000.pt',
                  #'/home/uli/CloudStation/thesis_work/SL_training_results/06-18-19_02_closestGridPoint_qnet_5x5x5_noDistPenalty_trainNoClutter_1k_epochs/q_net_cnn_3D_pos_GPU1.pt',
                  
                  #'/home/uli/CloudStation/thesis_work/SL_training_results/06-17-19_03_closestGridPoint_qnet_with_col_150_epochs/q_net_cnn_3D_pos_GPU0.pt',
                  #'/home/uli/CloudStation/thesis_work/SL_training_results/06-17-19_02_closestGridPoint_policy_with_col/policy_cnn_3D_pos_GPU1.pt',
                  #'/home/uli/CloudStation/thesis_work/SL_training_results/06-19-19_04_closestGridPoint_policy_not_filtered_onlyFinetuneOnTestSetNoClutter_pretrain_06-19-19_03/policy_cnn_3D_pos_GPU1.pt'

                  '/home/uli/CloudStation/thesis_work/SL_training_results/06-11-19_02_qnet_dist_penalty_filtered_with_col/q_net_cnn_3D_pos_GPU1.pt',
                  '/home/uli/CloudStation/thesis_work/DQN_training_results/06-12-19_01_finetune_qnet_smaller_LR/policy_net_1_14999.pt',
                  '/home/uli/CloudStation/thesis_work/DQN_training_results/06-11-19_03_finetune_policy_net_scaled_output_smaller_LR/policy_net_0_14999.pt',
                  '/home/uli/CloudStation/thesis_work/SL_training_results/06-11-19_06_policy_dist_penalty_filtered_with_col/policy_cnn_3D_pos_GPU1.pt'                  
                  
                  
                  ]
                
                  #'/home/uli/CloudStation/thesis_work/SL_training_results/05-22-19_01_qnet_100_epochs_filtered_no_out_of_bound_m0p5_for_coll/q_net_cnn_3D_pos_GPU1.pt', # before finetune RL below
                  #'/home/uli/CloudStation/thesis_work/DQN_training_results/05-28-19_01_initqnet/policy_net_1_14999.pt', # finetune RL no dist penalty
                  #'/home/uli/CloudStation/thesis_work/DQN_training_results/05-27-19_01_dqn_baseline_EPS_init_0p99/policy_net_0_14999.pt'] # DQN baseline no dist penalty
  
  # for plotting labels, if not defined, then just "pred_0, pred_1 etc"
  model_labels = ['pred qnet SL',
                  'pred qnet RL',
                  'pred policy RL',
                  'pred policy SL'
                  ]
  
  models = []
  
  i=0
  for path_model in paths_models:
    model = t.DQN_lenet(classification).to(device) # lenet
    #path_model = '/home/uli/CloudStation/thesis_work/SL_training_results/'+\
    #  '05-07-19_04_from_scratch_3_IP_layers_adadelta_85acc/'+\
    #  'q_net_cnn_3D_pos.pt'
    #path_model = '/home/uli/CloudStation/thesis_work/DQN_training_results/05-09-19_02_start_at_upper_half_planner_to_gen_episodes_Adam_2conv_3IP/target_net_1_14999.pt'
    state_dict = torch.load(path_model)
    # Scale output layer weights for policy net (necause output valuesw are about 100 larger for policy-net than for q-net)
    #if i == 1:
    #  for key in state_dict:
    #    print(key)
    #  scale = 1/100.0
    #  state_dict['fc3.weight'] = scale*state_dict['fc3.weight']
    #  state_dict['fc3.bias'] = scale*state_dict['fc3.bias']
        
    model.load_state_dict(state_dict)
    models.append(model)
    i=i+1

  # test data from a dataset   
  #path_test_set = '/home/uli/research/thesis/datasets/dataset_19_no_clutter_DQN_16bit_images/0'
  #path_test_set = '/home/uli/research/thesis/datasets/dataset_31_dist_penalty_testset_near_goal/2'
  #path_test_set = '/home/uli/research/thesis/datasets/dataset_27_no_clutter_DQN_dist_penalty_grid_into_table/0'
  #'/home/uli/research/thesis/datasets/dataset_27_no_clutter_DQN_dist_penalty_grid_into_table/0'
  #path_test_set = '/home/uli/research/thesis/datasets/dataset_36_closestGridpointFix_DQN_noClutter_5x5x5_noDistPenalty/2'  
  #path_test_set = '/home/uli/research/thesis/datasets/dataset_37_closestGridpointFix_DQN_noClutter_5x5x5_withDistPenalty/2'
  #path_test_set = '/home/uli/research/thesis/datasets/dataset_35_closestGridpointFix_DQN_noClutter/0/'
  #path_test_set = '/home/uli/research/thesis/datasets/dataset_27c_no_clutter_DQN_dist_penalty_grid_into_table_1-step_to_goal/0'
  path_test_set = '/home/uli/research/thesis/datasets/dataset_27d_no_clutter_DQN_dist_penalty_grid_into_table_1-step_to_goal_above_table/0'
  #path_test_set = '/home/uli/research/thesis/datasets/dataset_27_no_clutter_DQN_dist_penalty_grid_into_table_near_goal/0'
  
  col_oob_value = 0.5
  filter_data = True
  rm_collision = False
  rm_out_of_bound = True
  rm_target_inv = True
  rm_obj_inv = True
  read_8bit_png = False
  dataset_test = t.dataset_with_filter(path_test_set,
                                              col_oob_value=col_oob_value,
                                              use_filter=filter_data,
                                              rm_collision=rm_collision,
                                              rm_out_of_bound=rm_out_of_bound,
                                              rm_target_inv=rm_target_inv,
                                              rm_obj_inv=rm_obj_inv,
                                              read_8bit_png=read_8bit_png)  
  #dataset_test = t.dataset_with_filter(path_test_set,use_filter=True)
    
  test_loader = t.DataLoader(dataset_test, batch_size=1,
                          shuffle=False, num_workers=1)
  
  args = None
  test(args, models, classification, device, test_loader,model_labels=model_labels)
  
  
if __name__ == '__main__':
    main()
  
