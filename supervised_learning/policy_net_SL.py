#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 15 10:54:09 2019

Script that uses a trained classification model (CNN learned the action for a observation)

Predicts the action (index) for a given input image

Model is defined in nets/nets.py

@author: uli
"""

import sys
import numpy as np
#import training
import torch
sys.path.append('./')
sys.path.append('..')
#sys.path.append('../simulation')
sys.path.append('../nets')

import nets

class policy_net_SL(object):
  '''
    Class for a policy network trained using supervised learning
    This is used to make predictions for actions
  '''
  def __init__(self,path_model,use_cuda=True):
    '''
      Inputs:
        - path_model (string): path to the model weights that should be loaded
        - use_cuda (boolean): Specifies if CUDA should be used or not
    '''
    self.device = torch.device("cuda" if use_cuda else "cpu")
    self.model = nets.DQN_lenet().to(self.device)      
    self.load_model(path_model)
    self.model.eval()
  
  def load_model(self,path_model):
    '''
      Loads the learned weigths from file.
    '''
    state_dict = torch.load(path_model)
    self.model.load_state_dict(state_dict)

  def classify(self,image):
    '''
      Predicts the action (index) for a given input image (as PyTorch Tensor)
      Return the index of the predicted action (as PyTorch Tensor)
    '''
    #image = ToTensor(image)
    with torch.no_grad():
      image = image.to(self.device,dtype=torch.float)
      output =self.model(image)
      pred = output.argmax(dim=1, keepdim=True) # get the index of the max log-probability
    return pred#.item()
  
  
def ToTensor(image):
  '''
    convert the numpy image to torch image (Tensor)
  '''
  image = image[:,:,0]
  image = image[..., np.newaxis]
  image = image[..., np.newaxis] # add 2 axis one for batch, one for color
  # numpy image: H x W x C X BATCH
  # torch image: BATCH X C X H x W
  
  image = image.transpose((3, 2, 0, 1))
  image = np.ascontiguousarray(image, dtype=np.float32) 
  image = torch.from_numpy(image)
  return image    # Resize, and add a batch dimension (BCHW)