#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 15 10:54:09 2019

Script that uses a trained classification model (CNN learned the action for a observation)

This serves as a demo and test of class policy_net_SL


@author: uli
"""

import sys
import numpy as np
#import training
import torch
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
sys.path.append('./')
sys.path.append('..')
sys.path.append('../simulation')
import utils.vrep_utils as utils  
import policy_net_SL as policy 


def main():
  
  # Path to model and input image
  path_model = '/home/uli/vrep_ur5/supervised_learning/policy_cnn_3D_pos_train1-4_20_epochs.pt'
  #path_depth_image = '/home/uli/vrep_ur5/supervised_learning/example_depth_image/img_000000_000005_000000.png'
  path_depth_image = '/home/uli/research/thesis/datasets/dataset_11_gui_disabled/1/images/img_000000_000114_000000.png'
  
  # Display input image
  depth_image = mpimg.imread(path_depth_image)
  plt.imshow(depth_image,
             interpolation='none')
  plt.show()  

  # Load net with weights
  net = policy.policy_net_SL(path_model,use_cuda=False)
  depth_image = ToTensor(depth_image)
  
  # Classify image
  action_index = net.classify(depth_image)
  
  print("action_index", action_index)

  # Convert index to action vector [x,y,z]
  delta_x = 0.01
  delta_y = 0.01
  delta_z = 0.01
  action = utils.actionIndexToAction(action_index.item(),delta_x,delta_y,delta_z)
  print("action", action)

  
if __name__ == '__main__':
  main()