##############
# What is this project about ?
##############
This is the repo is for training control policies as described in the paper:
"Learning Visual Servo Policies via Planner Cloning" at the 17th International Symposium on Experimental Robotics, Malta ([ISER2020](http://www.google.com/url?q=http%3A%2F%2Fiser2020.org%2F&sa=D&sntz=1&usg=AOvVaw3PgEVrqzVaOobW5KuxiSUn))
[[paper](https://drive.google.com/file/d/1KGwjkjbTF9dX5pYAZNEcSjWk8yk7cxKI/view?usp=sharing), [slides](https://docs.google.com/presentation/d/1ewQkce0A-5mTJ2RNPas_UBoclFH5icT_xgEQDXmRcQY/edit?usp=sharing)]

Please visit https://www.ulrichviereck.com/research for more information.

This project is about learning a manipulation controller in simulation. The idea is to use a full state planner to plan collision free trajectories for the manipulator to a target pose.
Then an agent that has only partial state information (from an depth sensor mounted on the gripper) learns a policy/q-function by imitating the planner (expert).
Finally, I finetune the policy using deep RL (DQN) because 1. the planner might not be ideal (e.g. it ignores occlusions of the target) and 2. the agent does NOT have full state information,
which introduces uncertainty about the full state.


##############
# Requirements
##############
- Pytorch (I installed with Anaconda3)
conda install pytorch torchvision cudatoolkit=10.0 -c pytorch
- V-REP 3.5.0
- My PlannerGrid plugin for V-REP (instructions how to build are seperate). This is required to generate the datasets for supervised learning and RL. The plugin implements a planner that finds collision free trajectories, saves depth images, and other state info:
https://bitbucket.org/uliv/v_repextplannergrid/src/master/

##############
# Generate dataset for supervised learning
##############
Start V-REP simulator in /scripts/ with:
```
python start_vrep_simulators.py /home/uli/vrep_ur5/simulation/scenes/vrep_scene_ur5_v28_peg_insertion_keep_out_box_measurable.ttt 1 --port 19997
```
Generate dataset in /scripts/ (either along trajectories to goal using the planner). You need to setup the paths in scripts/config.ini.
```
python data_generation.py --config_file config.ini
```
Or: generate dataset as "systematic"/for all possible states on the grid. This can be used as an "offlineSim" (to run deep RL much faster without V-REP, because all the required information has been generated). You need to setup the paths in scripts/config.ini. Example to generate 10 scenes that can be used as "offline data" witn DQN.py (for deep learning).
```
python DQN_save_GT.py --config_file config_scene_full_states_sim0.ini --n_start 0 --run_n 10
```

##############
# Learning a policy/q-function (deep RL/ DQN)
##############

Example to learn DQfD (deep learning from demonstrations) baseline. Please look at config.ini for many different setting for different experiments/baselines/learning parameters etc. Also adjust input/output paths in config.ini. Run in /RL/.
```
python DQN.py --config_file config.ini
```


