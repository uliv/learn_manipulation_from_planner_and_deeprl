# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 11:57:48 2019

@author: Ulrich Viereck

Class that handles the creation and removal of clutter objects for different tasks (e.g. for peg insertion)
"""
import utils.vrep_utils as utils
import random
import os
import math
import numpy as np
from re import sub


class clutter(object):
  def __init__(self, sim_client,folder_meshes,target_obj,man_obj=None,collection_name=None,gripper_collection_name=None,n_obj_range=[1,5],ignore_ollcusions=False):
    '''
    A class that handles the creation and removal of clutter in v-rep for different tasks
    
    Args:
      - sim_client: VRep client object to communicate with simulator over
      - folder_meshes: the root folder containing subfolders with meshes
      - target_obj: the object name for the target (e.g. hole for the peg insertion)
          This is needed to determine  valid clutter placement (clutter should not obstruct the target)
      - man_obj: The object to be manipulaed (e.g. the peg). This is needed to check valid palcement of clutter (e.g. peg insertion should be possible without collision)
      - collection_name (optional): It might be helpful to add all clutter objects to a collection, that can be used for collision checking
      - gripper_collection_name (optional): Used for collision checking between gripper and clutter (to test if a clutter configuration is valid)
      - n_obj_range: the min and max number of objects placed in the clutter
    '''

    # input arguments
    self.sim_client = sim_client
    self.folder_meshes = folder_meshes
    self.man_obj = man_obj
    self.target_obj = target_obj 
    self.collection_name = collection_name
    self.gripper_collection_name = gripper_collection_name
    self.n_obj_range = n_obj_range
    self.ignore_ollcusions = ignore_ollcusions
    
    # some fixed parameters
    self.clutter_x_y_extend = 0.6 # how far from the center should the clutter be placed at max?
    self.std_dev = 0.3


    # internal variables of the class
    self.object_handles = [] # need to know the handles of all clutter objects, so that it can be removed again
    if self.man_obj != None:
      self.man_obj_handle = utils.getObjectHandle(self.sim_client,self.man_obj)[1]
    else:
      self.man_obj_handle = None
      
    self.target_handle = utils.getObjectHandle(self.sim_client,self.target_obj)[1]

    # I need to change the position of the gripper to test if clutter plcement is valid
    # use these values to restore the position/orientation

    self.pos_init = utils.getObjectPositionByName(self.sim_client,self.man_obj) # store original position in world frame (to be able to restore after checking)
    self.rot_init = utils.getObjectOrientationByname(self.sim_client,self.man_obj)

  def sample_shape_type(self):
    '''
    Sample a shape with a given probability distribution (I want cubes most, then cylinders, then spheres and cones)
    '''    
    rand_number = random.uniform(0,1)
    if rand_number < 0.5:
      return 'cuboid'  # p = 0.5
    if rand_number < 0.75:
      return 'cylinder'# p = 0.25
    if rand_number < 0.9:
      return 'sphere' #p = 0.15
    else:
      return 'cone' # p = 0.1


  def create_clutter_simple_shapes(self):
    # sample the number of clutter objects
    n_objs = random.randint(self.n_obj_range[0],self.n_obj_range[1])
    print("n_objs", n_objs)
    
    gripper_parent, cam_parent = self.move_gripper_and_camera_under_parent_obj_in_gripper(self.man_obj,gripper_name='RG2_robotiq85_tips',cam_name='CAM_SR300_body')
    target_pos_in_ref = [0,0,0]
    target_orient_in_ref = [0,0,0]
    utils.setObjectPoseByNameInOtherFrame(self.sim_client,self.man_obj,self.target_obj,target_pos_in_ref,target_orient_in_ref)    
    
    while(len(self.object_handles)) < n_objs:      
      shape = self.sample_shape_type() #random.choice(['cuboid','sphere','cylinder','cone'])
      pos_x = np.random.normal(0, 0.1)
      pos_y = np.random.normal(0, 0.1)
      pos_z = np.random.uniform(0, 0.05)      
      #print "pos_x", pos_x
      #print "pos_y", pos_y

      if pos_x > self.clutter_x_y_extend:
        pos_x = self.clutter_x_y_extend
      if pos_x < -self.clutter_x_y_extend:
        pos_x = -self.clutter_x_y_extend  
      if pos_y > self.clutter_x_y_extend:
        pos_y = self.clutter_x_y_extend
      if pos_y < -self.clutter_x_y_extend:
        pos_y = -self.clutter_x_y_extend        
      pos = [pos_x,pos_y,pos_z]
      rand_rot_x = random.uniform(0,math.pi)
      orient_x = random.choice([0,0,0,90,90,90,rand_rot_x])
      rand_rot_y = random.uniform(0,math.pi)
      orient_y = random.choice([0,0,0,90,90,90,rand_rot_y])
      orient_z = random.uniform(0,2*math.pi)      
      orient = [orient_x,orient_y, orient_z]      
      
      size_x = random.uniform(0.03,0.15)      
      size_y = random.uniform(0.03,0.15)
      size_z = random.uniform(0.03,0.15)
      size = [size_x,size_y,size_z]
      obj_name = "clutter_object_" + str(len(self.object_handles))

      obj = self.create_shape(obj_name,shape,pos,orient,size)
     
      # translate object to table top
      self.translate_mesh_to_table_top(obj)
      
      placement_valid = self.test_valid_placement(obj_name,self.ignore_ollcusions) and self.test_valid_object_size(obj)
      if self.test_valid_object_size(obj) == False:
        raw_input("Object too large?")
      #print "placement_valid: ", placement_valid
   
      if placement_valid:
        self.object_handles.append(obj)
      else:
        utils.removeObjectWithChildren(self.sim_client,obj)
    # restore position and orientation
    utils.setObjectPositionByName(self.sim_client,self.man_obj,self.pos_init)
    utils.setObjectOrientationByName(self.sim_client,self.man_obj,self.rot_init)        


  def create_clutter(self):
    # sample the number of clutter objects
    n_objs = random.randint(self.n_obj_range[0],self.n_obj_range[1])
    print("n_objs", n_objs)
    
    while(len(self.object_handles)) < n_objs:      
      mesh_file = self.sample_mesh_file()#"/home/uli/research/models_3dnet/Cat50_ModelDatabase_STL/keyboard/ef3d038046cab5cabeb3159acb187cec.stl"      
      print("mesh_file", mesh_file)
      pos_x = np.random.normal(0, 0.1)
      pos_y = np.random.normal(0, 0.1)      
      print("pos_x", pos_x)
      print("pos_y", pos_y)

      if pos_x > self.clutter_x_y_extend:
        pos_x = self.clutter_x_y_extend
      if pos_x < -self.clutter_x_y_extend:
        pos_x = -self.clutter_x_y_extend  
      if pos_y > self.clutter_x_y_extend:
        pos_y = self.clutter_x_y_extend
      if pos_y < -self.clutter_x_y_extend:
        pos_y = -self.clutter_x_y_extend        
      pos = [pos_x,pos_y,0.3]
      rand_rot_x = random.uniform(0,math.pi)
      orient_x = random.choice([0,0,0,90,90,90,rand_rot_x])
      rand_rot_y = random.uniform(0,math.pi)
      orient_y = random.choice([0,0,0,90,90,90,rand_rot_y])
      orient_z = random.uniform(0,2*math.pi)      
      orient = [orient_x,orient_y, orient_z]      
      scale = random.uniform(0.05,0.2)
      name = "clutter_object_" + str(len(self.object_handles))

      obj = self.import_mesh(mesh_file,name,pos,orient,scale)
      
      # simplified mesh:
      obj_out_name = name + "_col"
      obj_simple = utils.add_simplified_mesh(self.sim_client,obj,obj_out_name,self.collection_name)
      raw_input("Added simplified mesh")
      
      # translate object to table top
      self.translate_mesh_to_table_top(obj)
      
      placement_valid = self.test_valid_placement() and self.test_valid_object_size(obj)
      if self.test_valid_object_size(obj) == False:
        raw_input("Object too large?")
      #print "placement_valid: ", placement_valid
   
      if placement_valid:
        self.object_handles.append(obj)
      else:
        utils.removeObjectWithChildren(self.sim_client,obj)          

# This also creates simplified mesh and stores them
  def test_import_of_all_meshes_1_by_1(self):

    mesh_files = self.list_of_mesh_files()
    mesh_files.sort()
    pos = [0.3,-0.3,0.2]
    orient = [0,0,0]
    scale = 0.1

    i = 0
    for mesh_file in mesh_files:
      i = i+1   
      print(i, " mesh_file", mesh_file)
      name = "clutter_object_" + str(i)
      #file_to_load = sub('.stl$', '_copy.stl', mesh_file)
      #obj = self.import_mesh(file_to_load,name,pos,orient,1.0)#scale)
      obj = self.import_mesh(mesh_file,name,pos,orient,scale)      
      mesh_file_simple = sub('.stl$', '_simple.stl', mesh_file)
      obj_simple_imported = self.import_mesh(mesh_file_simple,name+'_simple',pos,orient,1.0)      
      raw_input("imported simplified mesh")
      
      # simplified mesh:
      obj_out_name = name + "_col"
      file_to_store = sub('.stl$', '_simple.', mesh_file) # this is complete path with . at the end but without stl, e.g. "/home/uli/temp/simple."
      print("file_to_store", file_to_store)
      file_to_store_original = sub('.stl$', '_copy.', mesh_file)
      obj_simple = utils.add_simplified_mesh(self.sim_client,obj,obj_out_name,self.collection_name,file_to_store,file_to_store_original)      

      #raw_input("next object")
      utils.removeObjectWithChildren(self.sim_client,obj)
      




  def remove_clutter(self):
    while(len(self.object_handles)):
      obj = self.object_handles[-1]
      utils.removeObjectWithChildren(self.sim_client,obj)
      del self.object_handles[-1]
      
      

  
  def sample_mesh_file(self):
    '''
    Sample a mesh file from a folder
    
    Returns:
      - the file path to the mesh file
      - False, if file is not a valid stl mesh file
    '''    
    
    #Sample the category (folder uniform random):

    category = random.choice(os.listdir(self.folder_meshes))
    while category == 'bell_pepper': # bell peper have wrong scale, too large
      category = random.choice(os.listdir(self.folder_meshes))
    path_category = os.path.join(self.folder_meshes,category)
    meshes = os.listdir(path_category)
    mesh = random.choice(meshes)
    path_to_mesh = os.path.join(path_category,mesh)
    # Check that it returns a valid mesh file
    if not path_to_mesh.endswith(".stl"):
      return False
    else:  
      return path_to_mesh
      
  def list_of_mesh_files(self):
    mesh_files = []
    categories = os.listdir(self.folder_meshes)
    for category in categories:
      path_category = os.path.join(self.folder_meshes,category)
      meshes = os.listdir(path_category)
      for mesh in meshes:
         path_to_mesh = os.path.join(path_category,mesh)
         if path_to_mesh.endswith(".stl") and not path_to_mesh.endswith("_simple.stl") and not path_to_mesh.endswith("_copy.stl"):
           mesh_files.append(path_to_mesh)
    return mesh_files
  
  def import_mesh(self,mesh_file,name,pos,orient,scale):
    frame_ref=self.target_obj
    color_values = range(256)
    color = [random.choice(color_values),random.choice(color_values),random.choice(color_values)]
    parent = "Clutter"
    use_dummy_as_handle=0
    obj_handle = utils.importShape(self.sim_client,name,mesh_file,pos,orient,frame_ref,color,self.collection_name,parent,use_dummy_as_handle,scale)
    return obj_handle
    
  def create_shape(self,name,shape,pos,orient,size):
    frame_ref=self.target_obj
    color_values = range(256)
    color = [random.choice(color_values),random.choice(color_values),random.choice(color_values)]
    #print "color", color
    parent = "Clutter"
    use_dummy_as_handle=False
    mass=0.001
    static=True
    respondable=False
    obj_handle = utils.generateShape(self.sim_client,name, shape, size, pos,
                  orient,frame_ref, mass, color,
                  parent,self.collection_name, static, respondable,use_dummy_as_handle)    
    return obj_handle
  
  def translate_mesh_to_table_top(self,obj):
    '''
    The object imported might be floating (too high) or be in side the table (too low)
    Translate the object in z-axis so that it touches the table
    Implemented in child class
    '''
    pass
  
  # this function should be implemented by the child class, which is task specific
  # E.g. for peg insertion, we want to test that a peg insertion is possible without collision with the clutter
  def test_valid_placement(self):
    '''
    Sub-clutter classes should override this to test if a clutter placemetn is valid for a specific task
    Returns: True as default
    '''    
    return True
    
  def test_valid_object_size(self,obj_handle):
    extends = utils.get_bounding_box_extends(self.sim_client,obj_handle)
    if any([abs(i) > 0.5 for i in extends]):
      return False
    else:
      return True

      
  def move_gripper_and_camera_under_parent_obj_in_gripper(self,man_obj,gripper_name='RG2_robotiq85_tips',cam_name='CAM_SR300_body'):
    '''
    For collision checking, etc. we need to move the object in gripper together with camera and gripper
    Therefore, the easiest is to move camera and gripper under parent "object in gripper" (e.g. the peg)
    '''
    gripper_parent = utils.getObjectParent(self.sim_client,gripper_name)    
    cam_parent = utils.getObjectParent(self.sim_client,cam_name)    
    utils.setObjectParent(self.sim_client,gripper_name, man_obj)
    utils.setObjectParent(self.sim_client,cam_name, man_obj)
    
    return gripper_parent, cam_parent  
  
  def restore_parents_for_gripper_and_camera(self,gripper_parent,cam_parent,gripper_name='RG2_robotiq85_tips',cam_name='CAM_SR300_body'):
    utils.setObjectParent(self.sim_client,gripper_name,gripper_parent)
    utils.setObjectParent(self.sim_client,cam_name,cam_parent)  

  
class clutter_peg_insertion(clutter):
    def __init__(self, sim_client,folder_meshes,target_obj,man_obj,collection_name,gripper_collection_name,n_obj_range,ignore_ollcusions): # sim_client,folder_meshes,target_obj,man_obj=None,collection_name=None,gripper_collection_name=None,n_obj_range=[1,5]
      super(clutter_peg_insertion,self).__init__(sim_client,folder_meshes,target_obj,man_obj,collection_name,gripper_collection_name,n_obj_range,ignore_ollcusions)
    
    # use this to overwrite the default in the superclass for the peg insertion task
    def test_valid_placement(self,obj_name='',ignore_occlusions=False):
      # need to set to parent "peg2_handle" for the two following objects  
      # store original parents so that they can be restored later
#      gripper_parent = utils.getObjectParent(self.sim_client,'RG2_robotiq85_tips')    
#      cam_parent = utils.getObjectParent(self.sim_client,'CAM_SR300_body')  
#      
#      utils.setObjectParent(self.sim_client,'RG2_robotiq85_tips',self.man_obj)
#      utils.setObjectParent(self.sim_client,'CAM_SR300_body',self.man_obj)
      
      # do collision checking after moving peg2_handle to target, then restore original position
#      target_pos_in_ref = [0,0,0]
#      target_orient_in_ref = [0,0,0]
#      utils.setObjectPoseByNameInOtherFrame(self.sim_client,self.man_obj,self.target_obj,target_pos_in_ref,target_orient_in_ref)

      # check that peg can be placed above hole without collision with objects
      #raw_input("Before collision checking")
      objs1_is_collection = 0
      objs2_is_collection = 1
      collision = utils.checkCollision(self.sim_client,obj_name,self.gripper_collection_name,objs1_is_collection,objs2_is_collection)      
      #print "collision", collision
      
      pixels_visible_target, pixels_visible_obj  = utils.test_visibility_target_obj(self.sim_client)
      
      # restore position and orientation
#      utils.setObjectPositionByName(self.sim_client,self.man_obj,self.pos_init)
#      utils.setObjectOrientationByName(self.sim_client,self.man_obj,self.rot_init)
      
      #restore original parents
#      utils.setObjectParent(self.sim_client,'RG2_robotiq85_tips',gripper_parent)
#      utils.setObjectParent(self.sim_client,'CAM_SR300_body',cam_parent)
   
      if(ignore_occlusions == False and (pixels_visible_target < 16 or pixels_visible_obj < 16)): # adding this to generate SL dataset as before
        return False
      
      if collision == 1:
        return False
      else: 
        return True
      

    # for the peg insertion case the plane with hole is the table    
    def translate_mesh_to_table_top(self,obj):
      # move object until it touches the table
      utils.translate_mesh_to_touch_object(self.sim_client,obj,self.target_handle)
      
  
      
      
      
