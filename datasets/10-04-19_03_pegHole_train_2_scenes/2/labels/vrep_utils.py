import os
import time
import numpy as np

from utils import transformations
from simulation import vrep


VREP_BLOCKING = vrep.simx_opmode_blocking
VREP_ONESHOT = vrep.simx_opmode_oneshot_wait
VREP_CHILD_SCRIPT = vrep.sim_scripttype_childscript

OBJ_FLOAT_PARAM_BBOX_MAX_Z = 26
sim_boolparam_display_enabled = 16

#------------------------------------------------------------------------------------------------#
#                                       Simulation Control                                       #
#------------------------------------------------------------------------------------------------#
# Connects to the simulation at the given address and port
def connectToSimulation(ip_address, port):
  sim_client = vrep.simxStart(ip_address, port, True, True, -1800000, 5) # timeout for blocking calls: 1800 seconds= 30 minutes
  if sim_client == -1:
    print('Failed to connect to simulation. Exiting...')
    exit()
  else:
    print('Connected to simulation')

  return sim_client

# Disconnect from the simulation
def disconnectFromSimulation(sim_client):
  vrep.simxFinish(sim_client)

# Stop the V-Rep simulator
def stopSimulation(sim_client):
  vrep.simxStopSimulation(sim_client, VREP_BLOCKING)

# Restart the V-Rep simulator
def restartSimulation(sim_client):
  #vrep.simxStopSimulation(sim_client, VREP_BLOCKING)
  #time.sleep(1)
  #vrep.simxStartSimulation(sim_client, VREP_BLOCKING)
  #time.sleep(1)

  # stop the simulation:
  vrep.simxStopSimulation(sim_client,VREP_BLOCKING)

  # method that polls a signal to check if simulation actually stopped, then restart
  # setup a useless signal
  vrep.simxSetIntegerSignal(sim_client,'asdf',1,VREP_BLOCKING)


  # IMPORTANT
  # you should poll the server state to make sure
  # the simulation completely stops before starting a new one
  while True:
      time.sleep(0.01)
      # poll the useless signal (to receive a message from server)
      vrep.simxGetIntegerSignal(
          sim_client,'asdf',VREP_BLOCKING)

      # check server state (within the received message)
      e = vrep.simxGetInMessageInfo(sim_client,
          vrep.simx_headeroffset_server_state)

      # check bit0
      not_stopped = e[1] & 1

      if not not_stopped:
          break
      else:
          print('not_stopped')
          

  # IMPORTANT
  # you should always call simxSynchronous()
  # before simxStartSimulation()
  #vrep.simxSynchronous(clientID,True)

  # then start the simulation:
  e = vrep.simxStartSimulation(sim_client,VREP_BLOCKING)
  print("sim_client", sim_client)
  print('in restartSimulation start',e)





def GUI_enabled(sim_client, GUI_enabled):
  if GUI_enabled:
    GUI_enabled = 1
  else:
    GUI_enabled = 0
  vrep.simxSetBooleanParameter(sim_client,sim_boolparam_display_enabled,GUI_enabled,VREP_ONESHOT)  

#------------------------------------------------------------------------------------------------#
#                                         Script API calls                                       #
#------------------------------------------------------------------------------------------------#

def generateShape(sim_client, name, shape_type, size, position,
                  orientation=[0.,0.,0.],pos_ref=None, mass=0.001, color=[255., 255., 255.],
                  parent=None,collection=None, static=True, respondable=False,use_dummy_as_handle=False):
  '''
  Attempts to generate the desired shape in the vrep simulation. This requires the 'createShape' function
  to be in the 'remoteAPICommandServer' dummy object in the vrep simulation. See 'simulation/sensor_example.ttt'.

  Args:
    - sim_client: vrep client object attached to the desired simulation
    - name: Name of the object to be created
    - shape_type: Type of shape to be generate ('cuboid', 'sphere', 'cylinder', 'cone')
    - size: List of 3 floats describing the shape size
    - position: List of [x, y, z] floats to generate object at
    - orientation: List of euler floats describing object orientation
    - pos_ref: object neame used as reference frame for position and orientation    
    - mass: The mass of the object to generate
    - color: Color of the shape to generate
    - parent: The parent for the new object, None or '' for no parent
    - collection: The collection this object should be added, None or '' for no collection
    - static: Indicates if the shape should be static (can't move due to dynamics relative to parent)
    - respondable: Indicates if the object can interact physically with other objects to cause them to bounce off etc.
    - use_dummy_as_handle: if true, a dummy will be added as parent of the shape as a handle. he location of the handle is on the surface of the object with z pointing inside
        This allows easy calculation of the "bottom" of the object to the top of the target (e.g. for peg into hole distance calculation)


  Returns: object handle if successful, None otherwise
  '''
  shape2number = {'cuboid': 0,'sphere': 1,'cylinder': 2,'cone': 3}  

  if pos_ref == None:
    pos_ref = ''
    
  if parent == None:
    parent = ''
    
  if collection == None:
    collection = ''
    
  if static:
    static = 1
  else:
    static = 0
    
  if respondable:
    respondable = 1
  else:
    respondable = 0
  if use_dummy_as_handle:
    use_dummy_as_handle = 1
  else:
    use_dummy_as_handle = 0
    
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'remoteApiCommandServer', VREP_CHILD_SCRIPT,
                                        'createShape', [shape2number[shape_type],static,respondable,use_dummy_as_handle],
                                        size + position + orientation + color + [mass], [name,pos_ref,parent,collection],
                                        bytearray(), VREP_BLOCKING)
  if sim_ret[0] == 8:
    return None
  else:
    return sim_ret[1][0]

def importShape(sim_client, name, mesh_file, position, orientation, frame_ref='', 
                color=[255., 255., 255.], collection='',
                parent='', use_dummy_as_handle=0,scale=1.0):
  '''
  Attempts to import the desired mesh in the vrep simulation. This requires the 'importShape' function
  to be in the 'remoteAPICommandServer' dummy object in the vrep simulation. See 'simulation/sensor_example.ttt'.

  Args:
    - sim_client: vrep client object attached to the desired simulation
    - name: Name of the object to be created
    - mesh_file: Path to the mesh to be imported
    - position: List of [x, y, z] floats to generate object at
    - orientation: List of euler floats (radians) describing object orientation
    - frame_ref: the reference frame (Object) for the position and orientation of the added mesh
    - color: Color of the shape to generate
    - collection: the collection to add the object to (collections are useful to handle a group of objects as an entity e.g. for collision checking).
                 The collection has to exist in the simulation scene.
    - parent: the name of the parent of the added object. The parent has to exist in the simulation scene.
    - use_dummy_as_handle: integer. If 1, then add a dummy as a parent, which is placed at the original local frame of the imported mesh.
                           (e.g. the reference frame of the bottle is at the opening so that the location is known to the simulation for he cap on bottle task.)
    - scale: the scale for the onject to be imported
    
  Returns:
    - shapeHandle: the handle of the shape
    - objectHandle: the handle (parent) of the shape. This is the same as shapeHandle if use_dummy_as_handle = 0
  '''

  sim_ret = vrep.simxCallScriptFunction(sim_client, 'remoteApiCommandServer', VREP_CHILD_SCRIPT,
                                        'importShape', [use_dummy_as_handle], position + orientation + color + [scale],
                                        [name, mesh_file, frame_ref, collection, parent], bytearray(), VREP_BLOCKING)
  if sim_ret[0] == 8:
    print("sim_ret", sim_ret)
    return None
  else:
    shapeHandle = sim_ret[1][0]
    objectHandle = sim_ret[1][1]
    return shapeHandle, objectHandle
    
def add_simplified_mesh(sim_client,obj_in_handle,obj_out_name='',collection_name='',file_to_store='',file_to_store_original=''):
  '''
  This function adds a simplified mesh (reduced vertices and pologons and convexHull of original shape).
  This inteded use is to add a simplified maesh to a complex mesh that will be used for collision detection (for better performance).
  The original mesh will be visible, but not collidable.
  The simplified mesh will be invisible, but collidable.
  This function assumes the function "add_simplified_mesh" be implemented in the 'remoteAPICommandServer' dummy object in the vrep simulation of the scene.
  
  Args:
    - sim_client: vrep client object attached to the desired simulation
    - obj_in_handle: handle to the mesh that should be simplified (also used as parent of simplified mesh)
    - obj_out_name (optional): the name of the created simplified mesh
    - collection_name (optional): The name that the simplified mesh should be added to (e.g. )
  
  Returns:
    - The handle to the created mesh
  '''
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'remoteApiCommandServer', VREP_CHILD_SCRIPT,
                                        'add_simplified_mesh', [obj_in_handle],[],
                                        [obj_out_name,collection_name,file_to_store,file_to_store_original], bytearray(), VREP_BLOCKING)
  if sim_ret[0] == 8:
    return None
  else:
    return sim_ret[1][0]
  


def actuateRobotiq85(sim_client, close_open):
  '''

  Args:
    - sim_client: vrep client object attached to the desired simulation
    - close_open: the desired state of the gripper as integer (0: closed, 1: open)

  Returns: status of executed command (e.g. 0=successful)
  '''
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'remoteApiCommandServer', VREP_CHILD_SCRIPT,
                                        'actuateRobotiq85', [close_open], [], [],
                                        bytearray(), VREP_BLOCKING)

  return sim_ret


def control_physics(sim_client, physics_active):
  '''
  Enable/disable physics in the simulator. This uses a child script of 'remoteAPICommandServer' dummy object in the vrep simulation. See 'simulation/scenes/vrep_scene_ur5.ttt'.

  Args:
    - sim_client: vrep client object attached to the desired simulation
    - physics_active: the desired state of the physics simulation (0: inactive, 1: active)

  Returns: status of executed command (e.g. 0=successful)
  '''
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'remoteApiCommandServer', VREP_CHILD_SCRIPT,
                                        'control_physics', [physics_active], [], [],
                                        bytearray(), VREP_BLOCKING)

  return sim_ret

def control_ik(sim_client, ik_active):
  '''
  Enable/disable IK in the simulator. This uses a child script of 'remoteAPICommandServer' dummy object in the vrep simulation. See 'simulation/scenes/vrep_scene_ur5.ttt'.

  Args:
    - sim_client: vrep client object attached to the desired simulation
    - physics_active: the desired state of the IK (0: inactive, 1: active)

  Returns: status of executed command (e.g. 0=successful)
  '''
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'remoteApiCommandServer', VREP_CHILD_SCRIPT,
                                        'control_ik', [ik_active], [], [],
                                        bytearray(), VREP_BLOCKING)

  return sim_ret

def printToTerminal(sim_client, message):
  '''
    Print message to terminal.

  Returns: status of executed command (e.g. 0=successful)
  '''
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'remoteApiCommandServer', VREP_CHILD_SCRIPT,
                                        'printToTerminal', [], [], [message],
                                        bytearray(), VREP_BLOCKING)

  return sim_ret
  
  

#------------------------------------------------------------------------------------------------#
#                                       Object Helpers                                           #
#------------------------------------------------------------------------------------------------#

# Returns the objects handle
def getObjectHandle(sim_client, obj_name):
  return vrep.simxGetObjectHandle(sim_client, obj_name, VREP_BLOCKING)

# Returns object pose as 4x4 transformation matrix
def getObjectPose(sim_client, obj_handle):
  sim_ret, obj_position = getObjectPosition(sim_client, obj_handle)
  sim_ret, obj_orientation = getObjectOrientation(sim_client, obj_handle)

  obj_pose = transformations.euler_matrix(obj_orientation[0], obj_orientation[1], obj_orientation[2])
  obj_pose[:3,-1] = np.asarray(obj_position)

  return sim_ret, obj_pose
  
def getObjectMatrix(sim_client, obj_name, ref_name=''):
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'remoteApiCommandServer', VREP_CHILD_SCRIPT,
                                          'getObjectMatrix', [], [], [obj_name,ref_name],
                                          bytearray(), VREP_BLOCKING)    
  return sim_ret[2]  

def setObjectMatrix(sim_client, obj_name, T, ref_name=''):
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'remoteApiCommandServer', VREP_CHILD_SCRIPT,
                                          'setObjectMatrix', [], T, [obj_name,ref_name],
                                          bytearray(), VREP_BLOCKING)    
  return sim_ret  

# Sets object to a given pose
def setObjectPose(sim_client, obj_handle, pose):
  pass

# Returns object position as numpy array
def getObjectPosition(sim_client, obj_handle):
  sim_ret, obj_position = vrep.simxGetObjectPosition(sim_client, obj_handle, -1, VREP_BLOCKING)
  return sim_ret, np.asarray(obj_position)

def getObjectPositionByName(sim_client, obj_name):
  ret,obj_handle = getObjectHandle(sim_client,obj_name)
  ret,position=getObjectPosition(sim_client,obj_handle)
  return position

# Sets an object to a given position
def setObjectPosition(sim_client, obj_handle, position):
  return vrep.simxSetObjectPosition(sim_client, obj_handle, -1, position, VREP_BLOCKING)

def setObjectPositionByName(sim_client, obj_name, position):
    ret,obj_handle = getObjectHandle(sim_client,obj_name)
    setObjectPosition(sim_client, obj_handle, position)
    return ret


def setObjectPoseByNameInOtherFrame(sim_client, object_name, object_name_ref, position=[0,0,0], orientation=[0,0,0]):
  '''
  Inputs:
    - sim_client: vrep client object attached to the desired simulation
    - object_name: Object by name that should be moved
    - object_name_ref:  Reference by name for new position/orientation
    - position: The new position the object should be moved to
    - orientation: The new orientation the object should be moved to
  Outputs:
    0 if successful, otherwise throws an exception
  '''
  ret1,obj_handle = vrep.simxGetObjectHandle(sim_client,object_name, VREP_BLOCKING)
  ret2,obj_handle_ref = vrep.simxGetObjectHandle(sim_client,object_name_ref, VREP_BLOCKING)
  ret3 = vrep.simxSetObjectOrientation(sim_client,obj_handle,obj_handle_ref,orientation, VREP_BLOCKING)
  ret4 = vrep.simxSetObjectPosition(sim_client,obj_handle,obj_handle_ref,position, VREP_BLOCKING)
  #print "updated position of object in ref_frame: ", vrep.simxGetObjectPosition(sim_client,obj_handle,obj_handle_ref, VREP_BLOCKING)[1]
  #print "updated orientation of object in ref_frame: ", vrep.simxGetObjectOrientation(sim_client,obj_handle,obj_handle_ref, VREP_BLOCKING)[1]
  if ret1 or ret2 or ret3 or ret4:
    print(ret1,ret2,ret3,ret4)    
    raise Exception('Error in setObjectPositionByNameInOtherFrame')
  else:
    return 0

def setObjectZpositionRelativeToReference(sim_client,object_name, object_name_ref,z_pos):
  ret1,obj_handle = vrep.simxGetObjectHandle(sim_client,object_name, VREP_BLOCKING)
  ret2,obj_handle_ref = vrep.simxGetObjectHandle(sim_client,object_name_ref, VREP_BLOCKING)
  ret3,pos_in_ref_frame = vrep.simxGetObjectPosition(sim_client,obj_handle,obj_handle_ref, VREP_BLOCKING)
  vrep.simxSetObjectPosition(sim_client,obj_handle,obj_handle_ref,[pos_in_ref_frame[0],pos_in_ref_frame[1],z_pos], VREP_BLOCKING)
  #print "updated position of object in ref_frame: ", vrep.simxGetObjectPosition(sim_client,obj_handle,obj_handle_ref, VREP_BLOCKING)[1]
  if ret1 or ret2 or ret3:  
    print(ret1,ret2,ret3)    
    raise Exception('Error in setObjectZpositionRelativeToReference')
  else:
    return 0
  

# Returns the object orientation as numpy array
def getObjectOrientation(sim_client, obj_handle):
  sim_ret, orientation = vrep.simxGetObjectOrientation(sim_client, obj_handle, -1, VREP_BLOCKING)
  return sim_ret, np.asarray(orientation)
  
def getObjectOrientationByname(sim_client, obj_name):
  ret,obj_handle = getObjectHandle(sim_client,obj_name)
  sim_ret, orientation = vrep.simxGetObjectOrientation(sim_client, obj_handle, -1, VREP_BLOCKING)
  return np.asarray(orientation)

# Sets an object to a given orientation
def setObjectOrientation(sim_client, obj_handle, orientation):
  return vrep.simxSetObjectOrientation(sim_client, obj_handle, -1, orientation, VREP_BLOCKING)

# Sets an object to a given orientation
def setObjectOrientationByName(sim_client, obj_name, orientation):
  ret,obj_handle = getObjectHandle(sim_client,obj_name)
  return vrep.simxSetObjectOrientation(sim_client, obj_handle, -1, orientation, VREP_BLOCKING)

# Sets an object to a given orientation
def setObjectQuaternion(sim_client, obj_handle, quaternion):
  return vrep.simxSetObjectQuaternion(sim_client, obj_handle, -1, quaternion, VREP_BLOCKING)  

# Get quaternion for an object
def getObjectQuaternion(sim_client, obj_handle):
  return vrep.simxGetObjectQuaternion(sim_client, obj_handle, -1, VREP_BLOCKING) 

def getObjectMaxZ(sim_client, obj_handle):
  return vrep.simxGetObjectFloatParameter(sim_client, obj_handle, OBJ_FLOAT_PARAM_BBOX_MAX_Z, VREP_BLOCKING)

def removeObjectWithChildren(sim_client,obj_handle):
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'remoteApiCommandServer', VREP_CHILD_SCRIPT,
                                        'removeObjectWithChildren', [obj_handle], [], [],
                                        bytearray(), VREP_BLOCKING)  

def removeObject(sim_client,obj_handle):
  return vrep.simxRemoveObject(sim_client, obj_handle, VREP_BLOCKING)   


def getObjectParent(sim_client, obj):
  '''
  Inputs:
    - obj: the name (string) or obj_handle (int) of the object
  
  Returns:
    - parent_handle" the handle of the parent object
  '''

  if isinstance(obj, int):
    obj_handle =  obj
  else:     
    ret,obj_handle = getObjectHandle(sim_client,obj)  
    
  
  ret,parent_handle = vrep.simxGetObjectParent(sim_client,obj_handle,VREP_BLOCKING)
  return parent_handle
  

def setObjectParent(sim_client, obj, parent):
  '''
  Inputs:
    - obj: the name (string) or obj_handle (int) of the object
    - parent: can be specified as handle or as object name
  '''
  if isinstance(parent, int):
    parent_handle = parent
  else:
    ret,parent_handle = getObjectHandle(sim_client,parent)

  if isinstance(obj, int):
    obj_handle =  obj
  else:     
    ret,obj_handle = getObjectHandle(sim_client,obj)

  inPlace = 1 # update parent without changing any positions/orientations
  ret = vrep.simxSetObjectParent(sim_client,obj_handle,parent_handle,inPlace,VREP_BLOCKING)
  return ret

# scale an object
# scale: the scaling factor
# if scale is scalar, then scale equally in all 3 axes
# if scale is list of size 3, then scale individually in each axis 
def scaleObject(sim_client, obj_handle, scale):
  if isinstance(scale, list) and len(scale) == 3:
    pass
  else:
    scale = [scale,scale,scale]
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'remoteApiCommandServer', VREP_CHILD_SCRIPT,
                                          'scaleObject', [obj_handle], scale, [],
                                          bytearray(), VREP_BLOCKING)
  return sim_ret   


# scale the plane with hole, such that the hole has a particular diameter
def scalePlaneWithHole(sim_client, plane_name, hole_diameter):
  #scale of 1 equals diameter of 2 cm for plane_2cm_dia_2x2xp1.stl
  scale = hole_diameter/0.02
  ret,plane_h = getObjectHandle(sim_client,plane_name)
  
  # x-axis is up, so only scale y,z equally
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'remoteApiCommandServer', VREP_CHILD_SCRIPT,
                                          'scalePlane', [plane_h], [1.0,scale,scale], [],
                                          bytearray(), VREP_BLOCKING)
  # it seems that I do not need to move the reference handle after scaling (not implemented in "scalePlane")

  return sim_ret

# scale the peg such that is has desired diameter and length
def scalePeg(sim_client, obj_handle, diameter=1.0, length=1.0):
  #scale of 1 equals diameter of 2 cm and length of 10 cm  
  scale_dia = diameter/0.02
  scale_l = length/0.1
  #print "scale_dia", scale_dia
  #print "scale_l", scale_l
  # z-axis is up, so only scale x,y equally
  #return scaleObject(sim_client,obj_handle,[scale_dia,scale_dia,scale_l])
  return scaleObject(sim_client,obj_handle,[scale_dia,scale_dia,scale_l])


def translate_mesh_to_touch_object(sim_client,objectHandle,objectHandleRef):
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'remoteApiCommandServer', VREP_CHILD_SCRIPT,
                                          'translate_mesh_to_touch_object', [objectHandle,objectHandleRef], [], [],
                                          bytearray(), VREP_BLOCKING)    
  return sim_ret

def get_bounding_box_extends(sim_client,objectHandle):
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'remoteApiCommandServer', VREP_CHILD_SCRIPT,
                                          'get_bounding_box_extends', [objectHandle], [], [],
                                          bytearray(), VREP_BLOCKING)
  if sim_ret[0] == 8:
    return None
  else:
    return sim_ret[1]
  


def checkCollision(sim_client,objs_name_1, objs_name_2,isCollection1=1,isCollection2=1):
  '''
  Checks collision for a collision pair.
  
  Inputs:
    - objs_name_1, objs_name_2 (string): Name of the a collection or an object
    - isCollection1,isCollection2 (int): Indicates if objs_name_1 or objs_name_2 are collections or object names
    
  Returns:
    result of collision: 1=inCollision, 0=noCollision
  '''

  sim_ret = vrep.simxCallScriptFunction(sim_client, 'remoteApiCommandServer', VREP_CHILD_SCRIPT,
                                          'checkCollision', [isCollection1,isCollection2], [], [objs_name_1,objs_name_2],
                                          bytearray(), VREP_BLOCKING)
                                          
                                          
  if sim_ret[0] != 0:
    print(sim_ret)    
    raise Exception('Error in checkCollision')    
  else:
    return sim_ret[1][0]
 
  
  
def debugPositionsForListOfObjects(sim_client,list_of_objects):
    positions = []
    for obj_name in list_of_objects:
        position = getObjectPositionByName(sim_client,obj_name)
        print(obj_name, " position: ", position)
        positions.append(position)
    return zip(list_of_objects,positions)


def debugCalculateDiffsOfPositions(objects_position_pairs1,objects_position_pairs2):
    for objects_position_pair1,objects_position_pair2 in zip(objects_position_pairs1,objects_position_pairs2):
        if objects_position_pair1[0] != objects_position_pair2[0]:
            print("Error! Unexpected different objects: ", objects_position_pair1[0], objects_position_pair2[0])
            return 0;
        print("Difference for object (old-new)", objects_position_pair1[0], " is: ",  objects_position_pair1[1],"-", objects_position_pair2[1],'=',[a-b for a,b in zip(objects_position_pair1[1],objects_position_pair2[1])])
    return 1


#-----------------------------------------------------------------------------------------------
#                   RL simulation 
#------------------------------------------------------------------------------------------------
# Execute an action (delta movement) in gripper frame
# Returns reward and "done=1" in case of terminal condition 
# Will be used as "step" by DQN
# Inputs:
#    action: action in gripper frame [dx,dy,dz]
# Inputs for collision pair: objs_name_1, objs_name_2, isCollection1=1, isCollection2=1
def executeAction(sim_client, action, task_id, robotName, targetObject, objs_name_1, objs_name_2, isCollection1=1, isCollection2=1):
  res,retInts,retFloats,retStrings,retBuffer = vrep.simxCallScriptFunction(sim_client, 'pathPlannerPoseSpace3dPos', VREP_CHILD_SCRIPT,
                                          'executeAction', [task_id,isCollection1,isCollection2], action, [robotName, targetObject,objs_name_1,objs_name_2],
                                          bytearray(), VREP_BLOCKING)

  if res != 0:
    print(res)
    raise Exception('Error in executeAction') 

  else:
    done = retInts[0]
    reward = retFloats[0]
    return reward, done    
#------------------------------------------------------------------------------------------------#
#                                       Joint Helpers                                       #
#------------------------------------------------------------------------------------------------#

# Gets the joint position
def getJointPosition(sim_client, joint):
  sim_ret, position = vrep.simxGetJointPosition(sim_client, joint, VREP_BLOCKING)
  return sim_ret, position

# Set a joint position  
# This should NOT be done when dynamics are enabled, since this will cause infinite accelerations if set positions instantaniously
# However, with dynamics disabled (as I plan to do), then this should be okay
def setJointPosition(sim_client, joint, position):   
  sim_ret = vrep.simxSetJointPosition(sim_client, joint, position, VREP_BLOCKING)
  return sim_ret

# multiple joint positions at the same time (pause API communication between sending commands for individual joints)  
def setJointPositions(sim_client, joints, positions):
  vrep.simxPauseCommunication(sim_client,True)
  for joint,position in zip(joints, positions):
      vrep.simxSetJointPosition(sim_client, joint, position, vrep.simx_opmode_oneshot)
  vrep.simxPauseCommunication(sim_client,False)


# Sets a joints force to a given force
def setJointForce(sim_client, joint, force):
  return vrep.simxSetJointForce(sim_client, joint, force, VREP_BLOCKING)

# Sets a joints target velocity to a given velocity
def setJointTargetVelocity(sim_client, joint, velocity):
  return vrep.simxSetJointTargetVelocity(sim_client, joint, velocity, VREP_BLOCKING)
  

#------------------------------------------------------------------------------------------------#
#                                       ROBOTIQ_85 gripper control via signal                    #
#------------------------------------------------------------------------------------------------#

def controlRobotiq85(sim_client, gripperOpen):
  '''
  Attempts to open/close the robotiq85 gripper using signal that is used by child script of 'ROBOTIQ_85' in scene vrep_scene_ur5.ttt

  Args:
    - sim_client: vrep client object attached to the desired simulation
    - gripperOpen: 1: open, 0: close gripper

  Returns: status of executed command (e.g. 0=successful)
  '''
  sim_ret = vrep.simxSetIntegerSignal(sim_client, "gripperOpen", gripperOpen, VREP_BLOCKING)

  return sim_ret


#------------------------------------------------------------------------------------------------#
#                                      Transform helpers                                         #
#------------------------------------------------------------------------------------------------#

# helper to transform position from "in_ref_to_object" to global
# Notation T_ref_obj : Transform of obj in ref frame
def pos_to_global(sim_client,pos,ref_frame_name):
  ret, ref_frame = getObjectHandle(sim_client,ref_frame_name)
  sim_ret, T_global_ref = getObjectPose(sim_client, ref_frame)
  #print "T_global_ref", T_global_ref # ref in global frame
  T_ref_pos = np.eye(4)
  T_ref_pos[:3,-1] = np.asarray(pos)
  #print "T_ref_pos", T_ref_pos
  T_global_pos = np.dot(T_global_ref,T_ref_pos)
  #print "startpos_in_global", T_global_pos
  V_global_pos = T_global_pos[:3,-1]
  V_global_pos.tolist()
  #print "startpos_in_global (as list)", V_global_pos.tolist()
  return V_global_pos
  


#------------------------------------------------------------------------------------------------#
#                                       path planning                                            #
#------------------------------------------------------------------------------------------------#


def sampleValidStatesOnGrid(sim_client,robotName,taskName,targetObject,gripper_collection_name,clutter_collection_name,
                            dim_x,dim_y,dim_z,dim_t,
                            checkCol,checkVis,checkDist,
                            use_collision_cost_for_planner,
                            bound_target_x_low,bound_target_x_high,
                            bound_target_y_low,bound_target_y_high,
                            bound_target_z_low,bound_target_z_high,
                            maxDistance=0.0205, distNoPenalty=0.02, distPenaltyFactor=100):
  emptyBuff = bytearray()
  '''
    This is the first function to call for planning on a grid. It is also most time consuming.
    Inputs:
      - robotName (string): The object name for object used to change poses for planning (e.g. "peg_handle")
      - taskName (string): A name for the planning task (not really used, but available)
      - targetObject (string): The object name for object used as the target (e.g. "plane_handle")
      - gripper_collection_name (string): The first name of the collection used to define the collection pair. Usually defines a collection of the objects corresponding to the gripper.
      - clutter_collection_name (string): The second name of the collection used to define the collection pair. Usually defines a collection of the objects corresponding to the clutter/target.
      - dim_x (int): the dimension of the grid in x-axis (in global frame)
      - dim_y (int): the dimension of the grid in y-axis (in global frame)
      - dim_z (int): the dimension of the grid in z-axis (in global frame)
      - dim_t (int): the dimension of the grid in t-axis (theta for orientation about z-axis, in global frame)
      - checkCol (int): 0 or 1: indicates if collision should be checked for the collision grid,cost_grid
      - checkVis (int): 0 or 1: indicates if visibility (num of visible pixels) should be checked for the visibility grid,cost_grid
      - checkDist (int) NOT IMPLEMENTED YET: 0 or 1: indicates if distacne between gripper and clutter objects should be checked for the distance_grid,cost_grid
      - bound_target_x_low: State space limit in target frame in x-axis (lower bound)
      - bound_target_x_high: State space limit in target frame in x-axis (higher bound)
      - bound_target_y_low: State space limit in target frame in y-axis (lower bound)
      - bound_target_y_high: State space limit in target frame in y-axis (higher bound)
      - bound_target_z_low: State space limit in target frame in z-axis (lower bound)
      - bound_target_z_high: State space limit in target frame in z-axis (higher bound)
      - maxDistance(float): The threshold used for checkDistance calls.
          If the actual distance is greater than maxDistance, then the actual
          distance value is not calculated. If maxDistance is set to 0 or negative,
          then no threshold is used (distance values are always calculated).
      - distNoPenalty(float): The threshold in meters, where a penalty will be 
          applied (penalty if actual distance < distNoPenalty). If actual
          distance > distNoPenalty, then there is no penalty.
      - distPenaltyFactor(float): The max penalty factor for distance 0
          (decreases linearly until distance distNoPenalty). The total cost is
          calculated as (for distance &lt; distNoPenalty, otherwise no penalty):
          normalMoveCost * (1 + distPenaltyFactor*(distNoPenalty-actualDistance))
      - 
    Returns:
      - taskHandle: The task planning handle used to access results.
      
  '''

  VREP_planning_script = 'pathPlannerPoseSpace3dPos'
  remoteAPIfunction = 'sampleValidStatesOnGrid'
  inInts = [dim_x,dim_y,dim_z,dim_t,checkCol,checkVis,checkDist,use_collision_cost_for_planner]
  inFloats = [bound_target_x_low,bound_target_x_high,bound_target_y_low,bound_target_y_high,bound_target_z_low,bound_target_z_high, maxDistance, distNoPenalty, distPenaltyFactor]
  #robotName = peg_name + '_handle'
  #taskName = 'planning_peg'
  #targetObject = plane_name + '_handle'
  #gripper_collection_name = 'Fingers_and_man_obj'
  #clutter_collection_name = 'Clutter_objects_with_target'
  inStrings =[robotName,taskName,targetObject,gripper_collection_name,clutter_collection_name]
  res,retInts,retFloats,retStrings,retBuffer=vrep.simxCallScriptFunction(sim_client,VREP_planning_script,vrep.sim_scripttype_childscript,remoteAPIfunction,inInts,inFloats,inStrings,emptyBuff,vrep.simx_opmode_oneshot_wait)
  #print "res,retInts,retFloats,retStrings", res,retInts,retFloats,retStrings
  if res != 0:
    print(res)
    raise Exception('sampleValidStatesOnGrid')
  else:     
    taskHandle = retInts[0]
    return taskHandle

def setDistPenaltyParams(sim_client, task_id, distNoPenalty, distPenaltyFactor):
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'pathPlannerPoseSpace3dPos', VREP_CHILD_SCRIPT,
                                          'setDistPenaltyParams', [task_id], [distNoPenalty,distPenaltyFactor], [],
                                          bytearray(), VREP_BLOCKING)
  if sim_ret[0] != 0:
    print(sim_ret)    
    raise Exception('setDistPenaltyParams')   
 
def getDistPenaltyParams(sim_client, task_id):
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'pathPlannerPoseSpace3dPos', VREP_CHILD_SCRIPT,
                                          'getDistPenaltyParams', [task_id], [], [],
                                          bytearray(), VREP_BLOCKING)
  if sim_ret[0] != 0:
    print(sim_ret)    
    raise Exception('getDistPenaltyParams')
  else:
    maxDistance = sim_ret[2][0]
    distNoPenalty = sim_ret[2][1]
    distPenaltyFactor = sim_ret[2][2]
    return maxDistance, distNoPenalty, distPenaltyFactor
    

def findAllPathsToGoal(sim_client, task_id):
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'pathPlannerPoseSpace3dPos', VREP_CHILD_SCRIPT,
                                          'findAllPathsToGoal', [task_id], [], [],
                                          bytearray(), VREP_BLOCKING)
  if sim_ret[0] != 0:
    print(res)    
    raise Exception('findAllPathsToGoal')   



def queryPathAndCost(sim_client, task_id, startState, ref_frame=''):
  ''' Requires findAllPathsToGoal and sampleValidStatesOnGrid be called before
      Inputs:
        - sim_client: the v-rep client to connect to
        - task_id: the planning_id (see sampleValidStatesOnGrid)
        - startState: The start state for the trajectory to goal (the goal was defined already in sampleValidStatesOnGrid)
          Example for startState: [pox_x,pos_y,pos_z]
        - ref_frame: the reference frame the position is specified in.
            Planning in done in global frame, so if startState is specified in a ref_frame, then startState in global pos has to be calculated
      Returns:
        - path:
        - path_length:
        - path_cost:
  '''
  #print("startState", startState)
  if ref_frame != '':
    startpos_in_global = pos_to_global(sim_client,startState,ref_frame)
  else:
    startpos_in_global = startState
  
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'pathPlannerPoseSpace3dPos', VREP_CHILD_SCRIPT,
                                          'queryPathAndCost', [task_id], startpos_in_global, [ref_frame],
                                          bytearray(), VREP_BLOCKING)
  if sim_ret[0] != 0:
    print(sim_ret)
    raise Exception('queryPathAndCost')
  else:
    path_length = sim_ret[1][0]
    #task_id_ret = sim_ret[1][1]    
    path_cost = sim_ret[2][0]
    path = sim_ret[2][1:]
    return path, path_length, path_cost

# like queryPathAndCost, but trajectory as 2D list without duplicate goal entry
# also remove first entry in trajectory if (almost) same as current state
def queryPathPositionsAsArray(sim_client, task_id, startState, ref_frame=''):
  path, path_length, path_cost = queryPathAndCost(sim_client, task_id, startState, ref_frame)
  path = np.asarray(path).reshape((int(path_length/3),-1))
  goal = path[-1]
  last_grid_point = path[-2]
  # dist is less than 0.01 mm, then goal and last_grid_point are essentially the same
  # in that case remove the last entry
  if np.linalg.norm(goal-last_grid_point) < 0.00001: 
    path = path[:-1]
  
  fist_grid_point = path[0]  
  startState = np.asarray(startState)

  # dist is less than 0.01 mm, then startState and fist_grid_point are essentially the same
  # in that case remove the first entry
  if np.linalg.norm(startState-fist_grid_point) < 0.00001 and path.shape[0] > 1: 
    #print("removing first entry in path")
    path = path[1:]
  
  path = path.tolist()
  return path, len(path)


  
def check_goal_reached(position,goal):
  '''
    Given the current 3D position (in global) and the last entry in path,
    are the positions the same? If yes, the goal is reached.

    Inputs:
      - position (list of floats): current position as list in format [x,y,z]
      - goal (list of floats): goal as list in format [x,y,z]

    Returns:
      - goal_reached (bool): True if goal is reached (position ~= goal)

  '''
  
  position = np.asarray(position)
  goal = np.asarray(goal)
  if np.linalg.norm(position-goal) < 0.00001:
    goal_reached = True
  else:
    goal_reached = False
  return goal_reached
      
# do not use, it might overestimate the distance to goal (because Can't connect to cheapest grid point with findCheapestGridPoint at start since the costs haven't calculated for all grid points)    
def findPath(sim_client, task_id, startState, ref_frame=''):
  if ref_frame != '':
    startpos_in_global = pos_to_global(sim_client,startState,ref_frame)
  else:
    startpos_in_global = startState
  
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'pathPlannerPoseSpace3dPos', VREP_CHILD_SCRIPT,
                                          'findPath', [task_id], startpos_in_global, [],
                                          bytearray(), VREP_BLOCKING)
                   
  if sim_ret[0] != 0:
    print(sim_ret)
    raise Exception('findPath')
  else:
    path_length = sim_ret[1][0]
    #task_id_ret = sim_ret[1][1]    
    path_cost = sim_ret[2][0]
    path = sim_ret[2][1:]
    return path, path_length, path_cost

def getGridAxesValues(sim_client, task_id):
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'pathPlannerPoseSpace3dPos', VREP_CHILD_SCRIPT,
                                          'getGridAxesValues', [task_id], [], [],
                                          bytearray(), VREP_BLOCKING)
                                          
                                          
  if sim_ret[0] != 0:
    print(sim_ret)    
    raise Exception('getGridAxesValues')
  else:
    dim_x = sim_ret[1][0]
    dim_y = sim_ret[1][1]
    dim_z = sim_ret[1][2]
    
    retFloats = sim_ret[2]
    if len(retFloats) != dim_x + dim_y + dim_z:
      raise Exception('getGridAxesValues: len(retFloats) != dim_x + dim_y + dim_z') 
    values_x = retFloats[:dim_x]
    values_y = retFloats[dim_x:dim_x+dim_y]
    values_z = retFloats[dim_x+dim_y:]
    #task_id_ret = sim_ret[1][1]    
    return values_x, values_y, values_z


def getGridCol(sim_client, task_id):
  '''
    Grid is returned flattened
    Value= 1: in collision
    Value=0: Not in collision    
  '''  
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'pathPlannerPoseSpace3dPos', VREP_CHILD_SCRIPT,
                                          'getGridCol', [task_id], [], [],
                                          bytearray(), VREP_BLOCKING)
                                          
  if sim_ret[0] != 0:
    print(sim_ret)    
    raise Exception('getGridCol')
  else:
    grid_col = sim_ret[2]
    return grid_col

def getGridVisTarget(sim_client, task_id):
  '''
    Grid is returned flattened
    Value: Number of pixels visible    
  '''
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'pathPlannerPoseSpace3dPos', VREP_CHILD_SCRIPT,
                                          'getGridVisTarget', [task_id], [], [],
                                          bytearray(), VREP_BLOCKING)
                                          
  if sim_ret[0] != 0:
    print(sim_ret)    
    raise Exception('getGridVisTarget')
  else:
    grid_vis = sim_ret[2]
    return grid_vis
  
def getGridVisObj(sim_client, task_id):
  '''
    Grid is returned flattened
    Value: Number of pixels visible    
  '''
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'pathPlannerPoseSpace3dPos', VREP_CHILD_SCRIPT,
                                          'getGridVisObj', [task_id], [], [],
                                          bytearray(), VREP_BLOCKING)
                                          
  if sim_ret[0] != 0:
    print(sim_ret)    
    raise Exception('getGridVisObj')
  else:
    grid_vis = sim_ret[2]
    return grid_vis  

def getGridDist(sim_client, task_id):
  '''
    Returns:
    Grid is returned flattened
    value: distance of gripper to obstacles for distance<maxDistance, otherwise maxDistance
    maxDistance: The maxDistance used in v-reps call "checkDistance". If values in grid = maxDistance, then
    it should be considered as distance >= maxDistance (the actual value was not calculated).
  '''  
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'pathPlannerPoseSpace3dPos', VREP_CHILD_SCRIPT,
                                          'getGridDist', [task_id], [], [],
                                          bytearray(), VREP_BLOCKING)
                                          
  if sim_ret[0] != 0:
    print(sim_ret)    
    raise Exception('getGridDist')
  else:
    maxDistance = sim_ret[2][-1]
    grid_dist = sim_ret[2][:-1]
    return grid_dist, maxDistance 

def grid_shaped_as_nparray(grid,dim_x,dim_y,dim_z,dim_t=1):
  '''
    converts a grid (flat list) to a numpy array of dimension
    (dim_x,dim_y,dim_z,dim_t)
    if dim_t=1, then output shape is (dim_x,dim_y,dim_z)
  '''
  grid_as_array = np.array(grid)
  if dim_t==1:
    grid_as_array=grid_as_array.reshape((dim_z,dim_y,dim_x))
  else:
    grid_as_array=grid_as_array.reshape((dim_t,dim_z,dim_y,dim_x))

  return grid_as_array  

# translates 0..26 to actions in form [x,y,z], e.g. [0.0,01,-0.01]
# e,g, index 7 -> (z_i,y_i,x_i)=(0,2,1) ->  [x,y,z]=[0.0,01,-0.01]
def actionIndexToAction(index,delta_x,delta_y,delta_z,delta_t=0,state_dim=3):
  
  if state_dim == 3:
    action = [0,0,0] # in order x,y,z
    action_indices = np.unravel_index(index,(3,3,3)) # in order z,y,x
    action = [(action_indices[2]-1)*delta_x,\
              (action_indices[1]-1)*delta_y,\
              (action_indices[0]-1)*delta_z]
  else:
    print("actionIndexToAction only implemented for state_dim == 3")
    raise NotImplementedError
    
  return action

def actionToActionIndex(action):
  state_dim = len(action)
  if state_dim != 3:
    print("actionToActionIndex only implemented for state_dim == 3")
    raise NotImplementedError
    
  action_indices = [0,0,0]
  for i,a in enumerate(action):  
    if a < 0: # order x,y,z
      action_indices[state_dim-i-1] = 0 # order z,y,x
    elif a == 0:
      action_indices[state_dim-i-1] = 1
    else:
      action_indices[state_dim-i-1] = 2
      
  action_indices = tuple(action_indices)
  
  action_index = action_indices[0]*3*3 + action_indices[1]*3 + action_indices[2]
  
  return action_index, action_indices


# conversion between action in only one axis at a time
#  map 0-6 to actions in one axis only, e.g. 1 to [-0.01,0,0] in x-axis
def indexToActionNoDiag(index,delta_x,delta_y,delta_z):
  if index == 0:
    action = [0,0,0]
  if index == 1:
    action = [-delta_x,0,0]
  if index == 2:
    action = [delta_x,0,0]
  if index == 3:
    action = [0,-delta_y,0]
  if index == 4:
    action = [0,delta_y,0]
  if index == 5:
    action = [0,0,-delta_z]
  if index == 6:
    action = [0,0,delta_z]    
  return action

def getLabelsForCurrentState(sim_client, task_id, robotName, targetObject):
  res,retInts,retFloats,retStrings,retBuffer = vrep.simxCallScriptFunction(sim_client, 'pathPlannerPoseSpace3dPos', VREP_CHILD_SCRIPT,
                                          'getLabelsForCurrentState', [task_id], [], [robotName, targetObject],
                                          bytearray(), VREP_BLOCKING)
                                          

                                        
  if res != 0:
    print(res)    
    raise Exception('getLabelsForCurrentState')
  else:
    #print "retInts", retInts
    q_values_len = retInts[0]
    #print "q_values_len", q_values_len
    best_action_len = retInts[1]
    action_deltas_len = retInts[2]
    pos_target_in_robot_len = retInts[3]
    orient_target_in_robot_len = retInts[4]
    best_index = retInts[5]
    
    idx = 0
    q_values = retFloats[idx:idx+q_values_len]
    #print "len(q_values)", len(q_values)
    idx = idx+q_values_len
    best_action = retFloats[idx:idx+best_action_len]
    idx = idx+best_action_len
    action_deltas =  retFloats[idx:idx+action_deltas_len]
    idx = idx+action_deltas_len    
    pos_target_in_robot =  retFloats[idx:idx+pos_target_in_robot_len]
    idx = idx+pos_target_in_robot_len        
    orient_target_in_robot =  retFloats[idx:idx+orient_target_in_robot_len]
    idx = idx+orient_target_in_robot_len
    best_value = retFloats[idx:idx+1]
    
    
    return q_values, best_action, action_deltas, pos_target_in_robot, orient_target_in_robot, best_value[0], best_index




def getFewerLabelsForCurrentState(sim_client, task_id, robotName, targetObject):
  res,retInts,retFloats,retStrings,retBuffer = vrep.simxCallScriptFunction(sim_client, 'pathPlannerPoseSpace3dPos', VREP_CHILD_SCRIPT,
                                          'getFewerLabelsForCurrentState', [task_id], [], [robotName, targetObject],
                                          bytearray(), VREP_BLOCKING)
  '''
    Returns:
      - q_values: list of 27 values
      - best_action: list for the action (e.g. [0.01,0,-0.01] for action in x,y,z)
      - None (placeholder)
      - pos_target_in_robot: list for the position of target in robot hand frame
      - orient_target_in_robot: list for the orientation (euler angles) of target in robot hand frame
      - None (placeholder)
      - best_index: value between 1 and 27 (does not start with 0!). corresponds
        to the index of q_values with best value
  '''                                          

                                        
  if res != 0:
    print(res)    
    raise Exception('getFewerLabelsForCurrentState')
  else:
    #print "retInts", retInts
    q_values_len = retInts[0]
    #print "q_values_len", q_values_len
    best_action_len = retInts[1]
    pos_target_in_robot_len = retInts[2]
    orient_target_in_robot_len  = retInts[3]
    best_index = retInts[4]
    
    idx = 0
    q_values = retFloats[idx:idx+q_values_len]
    #print "len(q_values)", len(q_values)
    idx = idx+q_values_len
    best_action = retFloats[idx:idx+best_action_len]
    idx = idx+best_action_len
    pos_target_in_robot = retFloats[idx:idx+pos_target_in_robot_len]
    idx = idx+pos_target_in_robot_len   
    orient_target_in_robot = retFloats[idx:idx+orient_target_in_robot_len]

    
    return q_values, best_action, None, pos_target_in_robot, orient_target_in_robot, None, best_index

def ConvertQvalues(q_values,col_oob_value=1.0,learn_best_q_value_only=False,best_q_value_margin=False,q_margin=0.0):
  '''
    The q-values might not be with correct sign etc
    There are 3 types of q-values: in csv file
      1: regular values in 0<= x < 1.0: for "distance to goal" (without collision)
      2: values idicating "out of bound: -1.0. Is a terminal state.
      3: values >= 1.0 for states in collision. The value is 1.0+"dist to goal".
         However, when collision occurs (), then this is a terminal state.
    Q-values should be negative, because the cost that occurs is negative (e.g. movement cost)
    
    Inputs:
      - q_values (np-array): the q-values from the CSV file (with inconsisted sign and value)
      - col_oob_value (float, optional): The value that should be used for collision and out-of-bound case
  '''
  
  q_values_original = list(q_values)
  # The newer datasets with dist penalty have increased value of 10 (was 1 before) for collisions
  q_values[q_values >= 10.0] = col_oob_value # collision case
  q_values[q_values < 0.0] = col_oob_value # out of bound case
  # switch sign of all q_values (also the +1 to -1 above)
  q_values = -q_values # rewards are all negative
  
  only_bad_q = False
  
  if learn_best_q_value_only:
    best_q_value = np.amax(q_values)
    # it might be that the best value is pretty low, if target is far away and difficult to reach
    # (many obstacles nearby, which will decrease the value)
    # is it possible that goal is not reachable without out of bound or collision?
    
    if not best_q_value > -col_oob_value:
      print("best_q_value", best_q_value, "-col_oob_value",\
          -col_oob_value, "q_values_original", q_values_original)
      only_bad_q = True 
    #assert(best_q_value > -col_oob_value), "best q value is lower than actions for collision or out pf bound!"+\
    #  "Need to filter out states that only have bad action values (e.g. need to pass near obstacles or out of bound?)"+\
    #  "with rm_if_no_good_q"
    if best_q_value_margin:
      q_values[q_values < best_q_value] = best_q_value-q_margin
    else:
      q_values[q_values < best_q_value] = -col_oob_value

  return q_values, only_bad_q


def visualizePath(sim_client, path, color):
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'pathPlannerPoseSpace3dPos', VREP_CHILD_SCRIPT,
                                          'visualizePath', color, path, [],
                                          bytearray(), VREP_BLOCKING)
  if sim_ret[0] != 0:
    print(sim_ret)    
    raise Exception('visualizePath')
  else:
    return sim_ret[1][0]   

def removeLine(sim_client, lineContainer):
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'pathPlannerPoseSpace3dPos', VREP_CHILD_SCRIPT,
                                          'removeLine', [lineContainer], [], [],
                                          bytearray(), VREP_BLOCKING)
  if sim_ret[0] != 0:
    print(sim_ret)    
    raise Exception('removeLine')  
  
def displayMessage(sim_client, message):
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'remoteApiCommandServer', VREP_CHILD_SCRIPT,
                                          'displayMessage', [], [], [message],
                                          bytearray(), VREP_BLOCKING)
  if sim_ret[0] != 0:
    print(sim_ret)    
    raise Exception('displayMessage')  



def checkDistance(sim_client, task_id, maxDistance):
  '''
  
  Requires sampleValidStatesOnGrid to be called first.
  Checks the distance between gripper_collection_name and clutter_collection_name
  (which was passed to sampleValidStatesOnGrid).
  
  Inputs:
    - sim_client: vrep client object attached to the desired simulation
    - task_id: The task planning handle returned by sampleValidStatesOnGrid
    - maxDistance: If the actual distance is greater than maxDistance,
       then the actual distance value is not calculated. If maxDistance
       is set to 0 or negative, then no threshold is used (distance values
       are always calculated).
    
    Returns:
      - distance: The distance between gripper and clutter (=maxDistance if 
         actual distance is > maxDistance).
  '''
  res,retInts,retFloats,retStrings,retBuffer = vrep.simxCallScriptFunction(sim_client, 'pathPlannerPoseSpace3dPos', VREP_CHILD_SCRIPT,
                                          'checkDistance', [task_id], [maxDistance], [],
                                          bytearray(), VREP_BLOCKING)
  if res != 0:
    print(res)    
    raise Exception('checkDistance')
  else:
    distance = retFloats[0]
    return distance                              
                                          

def destroyPlanningTask(sim_client, task_id):
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'pathPlannerPoseSpace3dPos', VREP_CHILD_SCRIPT,
                                          'destroyPlanningTask', [task_id], [], [],
                                          bytearray(), VREP_BLOCKING)
                                          
                                          
  if sim_ret[0] != 0:
    print(sim_ret)    
    raise Exception('destroyPlanningTask')    

#------------------------------------------------------------------------------------------------#
#                                       Sensor related                                           #
#------------------------------------------------------------------------------------------------#


def checkVisibility(sim_client,taskhandle,depthCamera,depthCameraVisibility,pixelDeltaThreshold=0.01):
  '''
  Checks the visibility of the target object in the depth camera view.
  Possible reasons for invisible could be oclusion by clutter, out of view, or too far away
  
  Inputs:
    - taskhandle: the planners handle
    - depthCamera: The default camera that does not see "visibility" objects (objects for purpose of checking visibility)
    - depthCameraVisibility: Special camera that can see the "visibility" object
    - pixelDeltaThreshold: The threshold in meters where two pixels from different sensors should be considered different.
  
  Returns:
    - NumPixelsVisible: number of pixels visible
  '''
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'pathPlannerPoseSpace3dPos', VREP_CHILD_SCRIPT,
                                          'checkVisibility', [taskhandle], [pixelDeltaThreshold], [depthCamera,depthCameraVisibility],
                                          bytearray(), VREP_BLOCKING)                                      
                                          
  if sim_ret[0] != 0:
    print(sim_ret)    
    raise Exception('checkVisibility')
  else:
    NumPixelsVisible = sim_ret[1][0]
    return NumPixelsVisible    



def checkVisibilityNoTask(sim_client,depthCamera,depthCameraVisibility,pixelDeltaThreshold=0.01):
  '''
  Checks the visibility of the target object in the depth camera view.
  Same as checkVisibility, but does not require the taskhandle (it can be used before taskhandle is created, e.g. during 
  clutter generation)
  Possible reasons for invisible could be oclusion by clutter, out of view, or too far away
  
  Inputs:
    - depthCamera: The default camera that does not see "visibility" objects (objects for purpose of checking visibility)
    - depthCameraVisibility: Special camera that can see the "visibility" object
    - pixelDeltaThreshold: The threshold in meters where two pixels from different sensors should be considered different.
  
  Returns:
    - NumPixelsVisible: number of pixels visible
  '''
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'pathPlannerPoseSpace3dPos', VREP_CHILD_SCRIPT,
                                          'checkVisibilityNoTask', [], [pixelDeltaThreshold], [depthCamera,depthCameraVisibility],
                                          bytearray(), VREP_BLOCKING)                                      
                                          
  if sim_ret[0] != 0:
    print(sim_ret)    
    raise Exception('checkVisibilityNoTask')
  else:
    NumPixelsVisible = sim_ret[1][0]
    return NumPixelsVisible    

def test_visibility_target_obj(sim_client):
  '''
    Check how many pixels are visible of target and peg
    Returns numbers of pixels visible
  '''
  cam_name = 'CAM_SR300_D'
  cam_name_target_vis = 'CAM_SR300_visibility_check'
  cam_name_obj_vis = 'CAM_SR300_visibility_check_man_obj'
  pixel_threshold_target_vis = 0.01 # don't change this, or otherwise it would be inconsistent with planner plugin
  pixel_threshold_obj_vis = 0.001 # don't change this, or otherwise it would be inconsistent with planner plugin
  pixels_visible_target = checkVisibilityNoTask(sim_client,
                                               cam_name,cam_name_target_vis,
                                               pixel_threshold_target_vis)
  pixels_visible_obj = checkVisibilityNoTask(sim_client,
                                            cam_name,
                                            cam_name_obj_vis,
                                            pixel_threshold_obj_vis)
  return pixels_visible_target, pixels_visible_obj    



def getDepthData(sim_client):
  sim_ret = vrep.simxCallScriptFunction(sim_client, 'pathPlannerPoseSpace3dPos', VREP_CHILD_SCRIPT,
                                          'getDepthData', [], [], [],
                                          bytearray(), VREP_BLOCKING)                                      
                                          
  if sim_ret[0] != 0:
    print(sim_ret)    
    raise Exception('getDepthData')
  else:
    resolution = sim_ret[1]
    depth_buffer = sim_ret[2]
    return resolution, depth_buffer     
  

class VisionSensor(object):
  def __init__(self, sim_client, sensor_name,
                     z_near=0.1, z_far=1.0):
    '''
    VRep vision sensor class.

    Args:
      - sim_client: VRep client object to communicate with simulator over
      - sensor_name: Sensor name in simulator
      - intrinsics: sensor intrinsics
      - z_near: Minimum distance for depth sensing
      - z_far: Maximum distance for depth sensing
    '''
    self.sim_client = sim_client

    # Setup sensor and default sensor values
    sim_ret, self.sensor = getObjectHandle(self.sim_client, sensor_name)

    self.z_near = z_near
    self.z_far = z_far
    
  def getDepthData(self):
    '''
    Get the depth data from the sensor.
    
    Returns: (resolution, resolution) depth image
    '''    
    # need to call sim.handleVisionSensor on v-rep to update the buffer ?
    
    #sim_ret, resolution, depth_buffer = vrep.simxGetVisionSensorDepthBuffer(self.sim_client, self.sensor, VREP_BLOCKING)
    resolution,depth_buffer = getDepthData(self.sim_client)
    depth_img = np.asarray(depth_buffer)
    depth_img.shape = (resolution[1], resolution[0])
    #print "depth_img value at 0,0 before scaling and inverting", depth_img[0,0]
    # need to scale and offset depth data to get values in meters
    depth_img = depth_img * (self.z_far - self.z_near) + self.z_near
    depth_img = np.where(depth_img<0.1,0,depth_img) # replace far pixels close to to sensor (<0.1m) with "0" indicating "invalid"
    depth_img = np.where(depth_img>self.z_far-0.00001,0,depth_img) # replace far pixels close to far clipping plane with "0" indicating "invalid"
    depth_image_inv = 1-depth_img/self.z_far
    #print "depth_image_inv value at 0,0 after scaling and inverting", depth_image_inv[0,0]    
    depth_img = np.flipud(depth_image_inv) #np.flipud(depth_img * (self.z_far - self.z_near) + self.z_near)
    
    return depth_img
    
  def getDepthDataRaw(self):
    '''
    Get the raw depth data from the sensor.
    
    Returns: (resolution, resolution) depth image
    '''    

    sim_ret, resolution, depth_buffer = vrep.simxGetVisionSensorDepthBuffer(self.sim_client, self.sensor, VREP_BLOCKING)
    depth_img = np.asarray(depth_buffer)
    depth_img.shape = (resolution[1], resolution[0])
    #print "depth_img value at 0,0 before scaling and inverting", depth_img[0,0]
    #depth_image_inv = 1-depth_img/self.z_far
    #print "depth_image_inv value at 0,0 after scaling and inverting", depth_image_inv[0,0]    
    depth_img = np.flipud(depth_img * (self.z_far - self.z_near) + self.z_near) #np.flipud(depth_img * (self.z_far - self.z_near) + self.z_near)
    
    return depth_img    
    
  def getRGBData(self):
    '''
    Get the RGB data from the sensor.
    
    Returns: (resolution, resolution) image
    '''    
    rgb = 0    
    sim_ret, resolution, img_buffer = vrep.simxGetVisionSensorImage(self.sim_client, self.sensor, rgb, VREP_BLOCKING)
    img = np.asarray(img_buffer)
    img.shape = (resolution[1], resolution[0],3)
    return np.flipud(img)    


