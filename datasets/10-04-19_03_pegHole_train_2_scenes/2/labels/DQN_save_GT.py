# -*- coding: utf-8 -*-
"""
Reinforcement Learning (DQN) with V-REP simulator to learn manipulation task
=====================================
based on:
website: https://pytorch.org/tutorials/intermediate/reinforcement_q_learning.html

"""

import sys
import random
import matplotlib.pyplot as plt
import argparse
import configparser
import os


sys.path.append('./')
sys.path.append('..')
sys.path.append('../simulation')
sys.path.append('../supervised_learning')
sys.path.append('../nets')
sys.path.append('../scripts')


from scene_gen_utils import vrepEnv, import_config_planner
import scene_gen_utils as scene
import utils.vrep_utils as utils


def main(config_file,run_no=None):
  print("main start")
  print("random sample in main (is it random?)", random.random())

  ###########################################
  # Setup files for logging, parameters etc #
  ###########################################


  config = configparser.RawConfigParser()
  config.read(config_file)   
  git_location = config.get('main', 'git_location')  
  dataset_path_output = config.get('logging', 'output_path')
 
  sim_port_offset = config.getint('simulator', 'sim_port_offset')
  use_fixed_seed = config.getboolean('simulator', 'use_fixed_seed')
  print("dataset_path_output", dataset_path_output)

  if use_fixed_seed:
    random.seed(7) # fixed seed for deteministic initial scene, this seems to fix the peg/hole config, but not the clutter
  else:
    random.seed()  

  if run_no != None: # use run_no as the subfolder number
    dataset_path_output = os.path.join(dataset_path_output, str(run_no))   
  elif sim_port_offset != None: # else use the sim_port_offset as subfolder number
    dataset_path_output = os.path.join(dataset_path_output,str(sim_port_offset))
    
  file_general,file_scenes,file_images,dataset_path_images,dataset_path_labels\
        = scene.create_logging_files(dataset_path_output,git_location,RL=True,config_file=config_file)
     
  
  env = vrepEnv(config_file,sim_port_offset,file_scenes)
  setup_scene_success = env.setup_scene()
  while(not setup_scene_success):
    setup_scene_success = env.setup_scene()
  env.init_state()
  
  data_to_file = [env.plane_pos, env.values_x, env.values_y, env.values_z, env.maxDistance]
  scene.write_list_of_lists_to_txt_file(file_general,data_to_file,round_digits=False)
  file_general.close()    

  #depth_image = env.getObservations()
  #img_x, img_y = depth_image.shape
  #q_values, best_action, best_index = env.getLabels()

  random.seed()  
  print("random sample in main (is it random?)", random.random())  
  
  # step an action in environment
  #action = [0.01,-0.01,-0.01]
  #reward, done = env.step(action)
  #print("reward", reward)
  #print("done", done)

  plt.ion()


  scene_no = 0
  traj_no = 0
  img_no = 0
  dim_x, dim_y, dim_z = env.get_grid_dims()
  for x in range(dim_x):
    for y in range(dim_y):
      for z in range(dim_z):
        position,collision,NumPixelsVisible_target,NumPixelsVisible_obj = env.init_state_systematic(x,y,z)
        env.save_image_and_labels(scene_no, traj_no, img_no, dataset_path_images, file_images, position, x, y, z)
        img_no += 1
        
  

  plt.ioff()
  plt.show()
  print("Done")
  env.disconnect_from_sim()


  
if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('--config_file', type=str, default = 'config.ini',
      help='Config file with training parameters etc')
  parser.add_argument('--run_start', type=int, default = 0,
      help='(optional) used as the subfolder as a number for the run')
  parser.add_argument('--run_n', type=int, default = 1,
      help='(optional) specifies how many runs to run')   
  
  args = parser.parse_args()
  for run_no in range(args.run_start, args.run_start + args.run_n):
    print("run_no ", run_no)
    main(args.config_file, run_no)
