# 
# This utility script contains code needed to generate scene in vrep and 
# do trajectory planning from all (collision free) grid points to goal grid point.
# 
# This ised used by:
# - "data_generation.py": to generate labeled depth_images for supervised learning.
# - "DQN.py": to generate scene and interact with the environment via class "vrep_env"
##########################
import sys
import time
import numpy as np
import matplotlib.pyplot as plt
import random
import os
from shutil import copyfile

sys.path.append('./')
sys.path.append('..')
sys.path.append('../simulation')
from simulation import vrep
from robots.ur5 import UR5
#from grippers.robotiq85 import robotiq85
from grippers.rg2 import RG2
import utils.vrep_utils as utils
#from timeit import default_timer as timer
import clutter.clutter as cl
import configparser
import PNG_16bit as PNG

VREP_BLOCKING = vrep.simx_opmode_blocking

def import_config_planner(config_file):
  '''
    Import grid configuration for planner.
    Config contains information about size of grid for collision checks and planning.
    
    Returns:
      - config_planner:  Dict containing the information for the grid.
  '''
  config = configparser.RawConfigParser()
  config.read(config_file)
  config_planner = {}
  config_planner['dim_x'] = config.getint('path_planner', 'dim_x')
  config_planner['dim_y'] = config.getint('path_planner', 'dim_y')
  config_planner['dim_z'] = config.getint('path_planner', 'dim_z')
  config_planner['dim_t'] = config.getint('path_planner', 'dim_t')
  config_planner['checkCol'] = config.getint('path_planner', 'checkCol')
  config_planner['checkVis'] = config.getint('path_planner', 'checkVis')
  config_planner['checkDist'] = config.getint('path_planner', 'checkDist')
  config_planner['use_collision_cost_for_planner'] = config.getint('path_planner', 'use_collision_cost_for_planner')  
  config_planner['bound_target_x_low'] = config.getfloat('path_planner', 'bound_target_x_low')
  config_planner['bound_target_x_high'] = config.getfloat('path_planner', 'bound_target_x_high')
  config_planner['bound_target_y_low'] = config.getfloat('path_planner', 'bound_target_y_low')
  config_planner['bound_target_y_high'] = config.getfloat('path_planner', 'bound_target_y_high')
  config_planner['bound_target_z_low'] = config.getfloat('path_planner', 'bound_target_z_low')
  config_planner['bound_target_z_high'] = config.getfloat('path_planner', 'bound_target_z_high')
  config_planner['maxDistance'] = config.getfloat('path_planner', 'maxDistance')
  config_planner['distNoPenalty'] = config.getfloat('path_planner', 'distNoPenalty')
  config_planner['distPenaltyFactor'] = config.getfloat('path_planner', 'distPenaltyFactor')

  return config_planner


class vrepEnv(object):
  def __init__(self,config_file,sim_port_offset=0,file_scenes=None):
    self.config_planner = import_config_planner(config_file)
    config = configparser.RawConfigParser()
    config.read(config_file)
    self.timeout_reward = config.getfloat('training_param', 'timeout_reward')
    self.git_location = config.get('main', 'git_location')
    self.create_clutter = config.getboolean('simulator', 'create_clutter')
    try:
      if config.getboolean('simulator', 'fixed_peg_hole'):
        hole_d = config.getfloat('simulator', 'hole_d')
        peg_d = config.getfloat('simulator', 'peg_d')
        peg_pos_x = config.getfloat('simulator', 'peg_pos_x')
        peg_pos_y = config.getfloat('simulator', 'peg_pos_y')
        peg_pos_z = config.getfloat('simulator', 'peg_pos_z')
        peg_l = config.getfloat('simulator', 'peg_l')
        self.cPegHole = pegHoleConfig(hole_d, peg_d, peg_pos_x, peg_pos_y, peg_pos_z, peg_l)
      else:
        self.cPegHole = None
    except:
      print("No peg/hole config found in config file, using random config")
      self.cPegHole = None
    print("connecting to simulator")
    print("port: ", 19997+sim_port_offset)
    self.sim_port_offset = sim_port_offset
    self.sim_client = utils.connectToSimulation('127.0.0.1', 19997+self.sim_port_offset)
    print("self.sim_client", self.sim_client)
    self.gripper = RG2(self.sim_client)
    self.sensor_d = utils.VisionSensor(self.sim_client, 'CAM_SR300_D',z_near=0.01, z_far=0.5)
    self.ur5 = UR5(self.sim_client, self.gripper)
    self.scene_no = 0
    self.file_scenes = file_scenes
    self.planning_handle = None
    
    self.GUI_enabled = config.getboolean('simulator', 'GUI_enabled') 
    self.collisionPair1 = 'Fingers_and_man_obj' # collection of objects for collision checking
    self.collisionPair2 =  'Clutter_objects_with_target' # collection of objects for collision checking
    self.ignore_occlusions = False
    
    
        
  #def config_scene(self,config):
  #  if "GUI_enabled" in config:
  #    self.GUI_enabled = config["GUI_enabled"]

  def get_sim_client(self):
    return self.sim_client
  
  def setup_scene(self):
    utils.restartSimulation(self.sim_client)
    self.planning_handle = None # restart Simulation destroys planningHandle
    utils.GUI_enabled(self.sim_client,self.GUI_enabled)

    #  IMPORT PLANE, PEG AND scale
    self.plane_obj,self.plane_pos,self.plane_name,self.peg_obj,self.peg_name = \
      generate_env_for_peg_hole(self.sim_client,self.gripper,self.scene_no,self.file_scenes,self.git_location,self.cPegHole)    
    ret,self.plane_obj_handle = utils.getObjectHandle(self.sim_client,self.plane_name + '_handle')
    ret,self.peg_obj_handle = utils.getObjectHandle(self.sim_client,self.peg_name + '_handle')
    
    peg_hole_visible = self.test_visibility_target_obj_in_goal_state(self.peg_name + '_handle', self.plane_name + '_handle')
    if not peg_hole_visible:
      return False
    
    # Create Clutter
    if(self.create_clutter):
      clutter_creation(self.sim_client,self.plane_name,self.peg_name,self.ignore_occlusions)

    # need to move gripper and camera in the hierarchy under parent "peg" so that if the peg is moved to a desired location, camera and gripper follow
    self.gripper_parent, self.cam_parent = move_gripper_and_camera_under_parent_peg(self.sim_client,self.peg_name,self.plane_name)
    
    #input("scene_gen_utils, setup_scene, Press a key to continue")
    #Detroy a previous planning task (if exists)
    if self.planning_handle:
      print("self.planning_handle (before planning_trajectories_on_grid)", self.planning_handle)
      utils.destroyPlanningTask(self.sim_client,self.planning_handle)  
    
    # Planning: Check collisions on grid, find trajectories to goal
    self.planning_handle = planning_trajectories_on_grid(self.sim_client,
                                                         self.peg_name,
                                                         self.plane_name,
                                                         self.config_planner)
    print("self.planning_handle (after planning_trajectories_on_grid)", self.planning_handle)
    # Check if correct dist parameters has been used.
    maxDistance, distNoPenalty, distPenaltyFactor =  utils.getDistPenaltyParams(self.sim_client, self.planning_handle)
    print("self.config_planner['checkDist']", self.config_planner['checkDist'])
    print("dist penalty params. maxDistance, distNoPenalty, distPenaltyFactor: ", maxDistance, distNoPenalty, distPenaltyFactor)
    print("self.config_planner['use_collision_cost_for_planner']", self.config_planner['use_collision_cost_for_planner'])    
    utils.findAllPathsToGoal(self.sim_client,self.planning_handle)
    
    self.values_x,self.values_y,self.values_z = utils.getGridAxesValues(self.sim_client,self.planning_handle)
    dim_x = len(self.values_x)
    dim_y = len(self.values_y)
    dim_z = len(self.values_z)
    
    # collision Grid in shape of x,y,z dimensions
    self.grid_col = getCollisionGrid(self.sim_client,self.planning_handle,dim_x,dim_y,dim_z,dim_t=1)
    self.grid_vis_target = getVisibilityGridTarget(self.sim_client,self.planning_handle,dim_x,dim_y,dim_z,dim_t=1)
    self.grid_vis_obj = getVisibilityGridObj(self.sim_client,self.planning_handle,dim_x,dim_y,dim_z,dim_t=1)
    self.grid_dist, self.maxDistance = getDistanceGrid(self.sim_client,self.planning_handle,dim_x,dim_y,dim_z,dim_t=1) 
  
    # allow same initial state visited more than once for DQN
    self.grid_visited = gridVisitedArray(self.grid_col,self.grid_vis_target,self.grid_vis_obj)
    return True


  def test_visibility_target_obj(sim_client):
    '''
      Check how many pixels are visible of target and peg
      Returns numbers of pixels visible
    '''
    cam_name = 'CAM_SR300_D'
    cam_name_target_vis = 'CAM_SR300_visibility_check'
    cam_name_obj_vis = 'CAM_SR300_visibility_check_man_obj'
    pixel_threshold_target_vis = 0.01 # don't change this, or otherwise it would be inconsistent with planner plugin
    pixel_threshold_obj_vis = 0.001 # don't change this, or otherwise it would be inconsistent with planner plugin
    pixels_visible_target = utils.checkVisibilityNoTask(sim_client,
                                                 cam_name,cam_name_target_vis,
                                                 pixel_threshold_target_vis)
    pixels_visible_obj = utils.checkVisibilityNoTask(sim_client,
                                              cam_name,
                                              cam_name_obj_vis,
                                              pixel_threshold_obj_vis)
    return pixels_visible_target, pixels_visible_obj     

  def test_visibility_target_obj_in_goal_state(self, man_obj, target_obj):
    #move_gripper_and_camera_under_parent_obj_in_gripper
    # Move peg to goal
    gripper_name='RG2_robotiq85_tips'
    cam_name='CAM_SR300_body'
    gripper_parent = utils.getObjectParent(self.sim_client,gripper_name)    
    cam_parent = utils.getObjectParent(self.sim_client,cam_name)    
    utils.setObjectParent(self.sim_client,gripper_name, man_obj)
    utils.setObjectParent(self.sim_client,cam_name, man_obj)   
    
    # Check that enough pixels are visible for peg in goal position:
    target_pos_in_ref = [0,0,0]
    target_orient_in_ref = [0,0,0]
    utils.setObjectPoseByNameInOtherFrame(self.sim_client,man_obj,target_obj,target_pos_in_ref,target_orient_in_ref)      
  
    pixels_visible_target, pixels_visible_obj  = utils.test_visibility_target_obj(self.sim_client)
    # restore parents
    utils.setObjectParent(self.sim_client,'RG2_robotiq85_tips',gripper_parent)
    utils.setObjectParent(self.sim_client,'CAM_SR300_body',cam_parent)     

    print("pixels_visible_target, pixels_visible_obj", pixels_visible_target, pixels_visible_obj)  
    if pixels_visible_target < 16 or pixels_visible_obj < 16 and self.cPegHole == None: # resample scene, but only if not fixed peg hole given
      #Import new peg and hole with different size
      #print("pixels_visible_target, pixels_visible_obj", pixels_visible_target, pixels_visible_obj)
      print("peg and hole configuration has too few visible pixels in goal state, resample peg/hole")
      #input("Press a key")
      return False  
    else:
      return True      
  
  def get_grid_dims(self):
    return len(self.values_x),len(self.values_y),len(self.values_z)
  
  def get_grid_deltas(self):
    return self.values_x[1]-self.values_x[0], self.values_y[1]-self.values_y[0], self.values_z[1]-self.values_z[0]

  def init_state(self,high_z_only=True):
    ''' Set gripper to initial state.
      
    
    '''
    result = sample_visible_and_collision_free_state(self.sim_client,self.peg_obj_handle,\
                                                     self.grid_col,self.grid_vis_target,self.grid_vis_obj,\
                                                     self.values_x,self.values_y,self.values_z,self.grid_visited,\
                                                     update_grid_visited=False,\
                                                     high_z_only = high_z_only\
                                                     )
    if result != False:

      self.p_global_peg,self.collision,self.NumPixelsVisibleTarget,self.NumPixelsVisibleObj,self.grid_visited = result
      state_z_i, state_y_i, state_x_i = get_closest_grid_indices_from_position(self.p_global_peg, self.values_x, self.values_y, self.values_z)     
    #return result # this is False if no visible and collision free state could be found
      return state_x_i, state_y_i, state_z_i 
    else:
      return False
    # if state found, then
    # result = p_global_peg,collision,NumPixelsVisible,grid_visited
  
  
  def init_state_systematic(self, state_x_i, state_y_i, state_z_i):
    '''
    A determinitic initialization of the state. Instead of randomly sampling a state
    use the grid indices provided.
    Used to generate all GT for supervised learning/testing from the RL environment.
    '''
    collision = self.grid_col[state_z_i, state_y_i, state_x_i] # order z,y,x for index seem to work    
    NumPixelsVisible_target = self.grid_vis_target[state_z_i, state_y_i, state_x_i]
    NumPixelsVisible_obj = self.grid_vis_obj[state_z_i, state_y_i, state_x_i]  
     
    position = [self.values_x[state_x_i], self.values_y[state_y_i], self.values_z[state_z_i]]
    utils.setObjectPosition(self.sim_client,self.peg_obj_handle, position)
    # add oriantation later
    #utils.setObjectOrientation(sim_client,obj_handle, [0,0,math.pi/2])

    # testing new function (num of pixels visible should still be the same) 
    '''
    cam_name = 'CAM_SR300_D'
    cam_name_target_vis = 'CAM_SR300_visibility_check'
    cam_name_obj_vis = 'CAM_SR300_visibility_check_man_obj'
    pixel_threshold_target_vis = 0.01 # don't change this, or otherwise it would be inconsistent with planner plugin
    pixel_threshold_obj_vis = 0.001 # don't change this, or otherwise it would be inconsistent with planner plugin
    visible_target = utils.checkVisibilityNoTask(self.sim_client,
                                                 cam_name,
                                                 cam_name_target_vis,
                                                 pixel_threshold_target_vis)
    visible_target2 = utils.checkVisibility(self.sim_client,
                                            self.planning_handle,
                                                 cam_name,
                                                 cam_name_target_vis,
                                                 pixel_threshold_target_vis)   

    if visible_target != visible_target2 or visible_target != NumPixelsVisible_target:
      print("visible_target, visible_target (original), NumPixelsVisible_target", visible_target, visible_target2, NumPixelsVisible_target)
      input("Press a key")

    
    visible_obj = utils.checkVisibilityNoTask(self.sim_client,
                                              cam_name,
                                              cam_name_obj_vis,
                                              pixel_threshold_obj_vis)   
    visible_obj2 = utils.checkVisibility(self.sim_client,
                                         self.planning_handle,
                                              cam_name,
                                              cam_name_obj_vis,
                                              pixel_threshold_obj_vis)  
    if visible_obj != visible_obj2 or visible_obj != NumPixelsVisible_obj:
      print("visible_obj, visible_obj (original), NumPixelsVisible_obj", visible_obj, visible_obj2, NumPixelsVisible_obj)    
      input("Press a key")        
   '''
    return position,collision,NumPixelsVisible_target,NumPixelsVisible_obj      
  
  def save_image_and_labels(self,scene_no,traj_no,img_no,dataset_path_images,file_images,position, x, y, z):
    save_image_and_labels(self.sim_client, self.sensor_d,\
              scene_no, traj_no, img_no,\
              dataset_path_images,\
              self.planning_handle, self.peg_name, self.plane_name,\
              file_images,
              self.grid_vis_target,
              self.grid_vis_obj,
              self.grid_dist,
              position,
              self.values_x, self.values_y, self.values_z,
              x, y, z                      
              )
      
  def step(self,action):
    reward, done = utils.executeAction(self.sim_client, action, self.planning_handle, self.peg_name + '_handle', self.plane_name + '_handle', self.collisionPair1 , self.collisionPair2, isCollection1=1, isCollection2=1)
    return reward, done

  def getObservations(self):
    ''' Returns depth images as numpy (depths inverted an scaled to 0-1)
    '''
    return self.sensor_d.getDepthData()
  

  def getLabels(self):
    ''' Returns:
          - q_values2 (list): value of each action
          - best_action2 (list): best action
          - best_index2 (int): index of the best action (the argmax of q_values2)
              best_index2 starts with 1
    '''
    
    # slower method with more information about the state
    #q_values, best_action, action_deltas, pos_target_in_robot, orient_target_in_robot, best_value, best_index = utils.getLabelsForCurrentState(sim_client, planning_handle, peg_name + '_handle', plane_name + '_handle')
    q_values2, best_action2, _ , _ , _, _, best_index2 = utils.getFewerLabelsForCurrentState(self.sim_client, self.planning_handle, self.peg_name + '_handle', self.plane_name + '_handle')  
    return q_values2, best_action2, best_index2
  
  def visualizePath(self):
      res, p_global_peg = utils.getObjectPosition(self.sim_client,self.peg_obj_handle)
      path, path_length, path_cost = utils.queryPathAndCost(self.sim_client, self.planning_handle, p_global_peg)
      line_handle = utils.visualizePath(self.sim_client, path, [0,0,255])
      return line_handle

  def clearPath(self,line_handle):
    utils.removeLine(self.sim_client, line_handle)
    
  def printToTerminal(self, message):
    utils.printToTerminal(self.sim_client, "sim_port_offset " + str(self.sim_port_offset))
    utils.printToTerminal(self.sim_client, "self.sim_client " + str(self.sim_client))    
    utils.printToTerminal(self.sim_client, message)

  def disconnect_from_sim(self):
    vrep.simxStopSimulation(self.sim_client, VREP_BLOCKING)
    time.sleep(2)
    vrep.simxFinish(self.sim_client)    

'''
class simData(object):
  def __init__(self):
    states = []
    pass

  # states could be a list of:
  # - observation
  # - grid index (x,y,z)
  # - relative posiion to goal
  # - labels (q-values, best_action, dist to goal, numVisiblePixels etc)

  

class vrepEnvCached(vrepEnv):
  #Similar to parent class vrepEnv, but this caches all states with information (writes to disk).
  #This assumes that only states on grid p[oints are consodered (and that the grid is not too large)
  def __init__(self,config_planner,sim_port_offset=0, path_to_offline_data = ''):
    super(vrepEnvCached, self).__init__(config_planner,sim_port_offset)
    
    # variable to indicate if all the images, labels etc were loaded
    self.offline_data_loaded = False
    self.simData = None
    
    # path used to save offline data (aka cache) or to load previously generated data
    self.path_to_offline_data = path_to_offline_data
    
    # some data structure to contain info about current state (gridpoint, x,y,z position etc)
    # this should be updated after call to step(action)
    self.current_state = None
    
  
  # Iterate over all grid points
  # save all observations (images) and labels to disk
  def generate_offline_data(self):
    pass  

  # Iterate over all grid points
  # load all observations (images) and labels from disk
  # to a datastructure (RAM/memory)  
  def load_offline_data(self):
    pass
'''  
  
 

def import_plane_with_hole(sim_client,name,git_location='/home/uli/vrep_ur5/'):  
  # Import the plane
  position = [0.0, 0.5, 0.1]
  orientation = [0, 0, 0] # values in radians, e.g. np.pi/2
  color = [255, 255, 0]
  shape_path = os.path.join(git_location,'simulation/objects/plane_2cm_dia_2x2xp1.stl')  # default size of hole is 2 cm diameter
  result = utils.importShape(sim_client, name, shape_path,
                          position, orientation, frame_ref='',
                          color=color,
                          collection = '', parent='Clutter', use_dummy_as_handle=1)

  if result != None:                        
    planeShape, planeHandle = result
    return planeHandle,position
  else:
    return None


def clutter_creation(sim_client,plane_name,peg_name,ignore_occlusions):
  folder_meshes = ""#"/home/uli/research/models_3dnet/Cat50_ModelDatabase_STL"
  target_obj = plane_name + '_handle'
  man_obj = peg_name + '_handle'
  collection_name = 'Clutter_objects_with_target'
  gripper_collection_name = 'Fingers_and_man_obj'
  n_obj_range = [5,15]
  clutter = cl.clutter_peg_insertion(sim_client,folder_meshes,target_obj,man_obj,
                                     collection_name,gripper_collection_name,n_obj_range,ignore_occlusions)
  clutter.create_clutter_simple_shapes()
  return clutter


def move_gripper_and_camera_under_parent_peg(sim_client,peg_name,plane_name):
  # similar to clutter creation, need to move objects to differetn parents
  # need to set to parent "peg2_handle" for the two following objects  
  # store original parents so that they can be restored later
  gripper_parent = utils.getObjectParent(sim_client,'RG2_robotiq85_tips')    
  cam_parent = utils.getObjectParent(sim_client,'CAM_SR300_body')    
  utils.setObjectParent(sim_client,'RG2_robotiq85_tips', peg_name + '_handle')
  utils.setObjectParent(sim_client,'CAM_SR300_body', peg_name + '_handle')
  
  return gripper_parent, cam_parent

def restore_parents_for_gripper_and_camera(sim_client,gripper_parent,cam_parent):
  utils.setObjectParent(sim_client,'RG2_robotiq85_tips',gripper_parent)
  utils.setObjectParent(sim_client,'CAM_SR300_body',cam_parent)  

def planning_trajectories_on_grid(sim_client,peg_name,plane_name,config_planner):
                                    
  robotName = peg_name + '_handle'
  taskName = 'planning_peg'
  targetObject = plane_name + '_handle'
  gripper_collection_name = 'Fingers_and_man_obj'
  clutter_collection_name = 'Clutter_objects_with_target'
  

  taskHandle = utils.sampleValidStatesOnGrid(sim_client,
                            robotName,taskName,
                            targetObject,
                            gripper_collection_name,
                            clutter_collection_name,
                            config_planner['dim_x'],
                            config_planner['dim_y'],
                            config_planner['dim_z'],
                            config_planner['dim_t'],
                            config_planner['checkCol'],
                            config_planner['checkVis'],
                            config_planner['checkDist'],
                            config_planner['use_collision_cost_for_planner'],
                            config_planner['bound_target_x_low'],
                            config_planner['bound_target_x_high'],
                            config_planner['bound_target_y_low'],
                            config_planner['bound_target_y_high'],
                            config_planner['bound_target_z_low'],
                            config_planner['bound_target_z_high'],
                            config_planner['maxDistance'],
                            config_planner['distNoPenalty'],
                            config_planner['distPenaltyFactor']                            
                            )
  return taskHandle


class pegHoleConfig():
  def __init__(self, hole_d, peg_d, peg_pos_x, peg_pos_y, peg_pos_z, peg_l):
    self.hole_d = hole_d
    self.peg_d = peg_d
    self.peg_pos_x = peg_pos_x
    self.peg_pos_y = peg_pos_y
    self.peg_pos_z = peg_pos_z
    self.peg_l = peg_l


def generate_env_for_peg_hole(sim_client,gripper,scene_no,file_scenes, git_location, pegHoleConfig=None):
  # hole related
  plane_name = 'plane_' + str(scene_no)
  ret = import_plane_with_hole(sim_client,plane_name, git_location)
  if ret == None:
    print("import_plane_with_hole returned None")
    return None
  plane_obj,plane_pos = ret
  
  if pegHoleConfig != None:
    hole_d = pegHoleConfig.hole_d  
  else:
    hole_d = random.uniform(0.025,0.06)
  print("hole_d", hole_d)
  utils.scalePlaneWithHole(sim_client,plane_name,hole_d)
  
  # Generate a cylinder as the peg
  if pegHoleConfig != None:
    peg_d = pegHoleConfig.peg_d  
  else:
    peg_d = random.uniform(0.02,hole_d-0.005)
  
  print("peg_d", peg_d)
  peg_name = 'peg_' + str(scene_no)

  if pegHoleConfig != None:
    peg_pos_x = pegHoleConfig.peg_pos_x
    peg_pos_y = pegHoleConfig.peg_pos_y 
    peg_pos_z = pegHoleConfig.peg_pos_z     
  else:
    peg_pos_x = random.uniform(-0.005,0.005) # left, right
    peg_pos_y = random.uniform(-peg_d/4,peg_d/4)
    peg_pos_z = random.uniform(-0.07,-0.025)    

  position = [peg_pos_x, peg_pos_y, peg_pos_z ] # relative to pos_ref
  orientation = [0, 0, 0]

  if pegHoleConfig != None:
    peg_l = pegHoleConfig.peg_l
  else:
    peg_l = random.uniform(abs(peg_pos_z),abs(peg_pos_z)+0.03) # peg_l should be in [abs(peg_pos_z),abs(peg_pos_z)+0.03]
  size = [peg_d, 0.0, peg_l] #y is ignored for cylinder, diameter 2 cm, length 7 cm
  mass = 0.001
  color = [255, 0, 0]
  pos_ref='UR5_tip' # UR5_tip is actually fixed to wrist, not to the fingers
  shape_type = 'cylinder' # can be 'cuboid','sphere','cylinder','cone'
  parent = 'UR5_tip'
  collection = 'Fingers_and_man_obj'
  static = True
  respondable = True
  use_dummy_as_handle = True
  peg_obj = utils.generateShape(sim_client, peg_name, shape_type, size, position, orientation, pos_ref, mass, color,
                            parent,collection,static,respondable,use_dummy_as_handle)
  
  parent = peg_name + '_handle'
  utils.setObjectParent(sim_client, 'Visibility_cube_peg', parent)
  # cube is of shape x=0.02 ,y=0.02 ,z= 0.01
  position = [0, peg_d/2-0.01+0.001, 0.005]
  utils.setObjectPoseByNameInOtherFrame(sim_client, 'Visibility_cube_peg', parent, position)
  

  '''
  green = [0, 255, 0]
  name = 'Visibility_peg' + str(scene_no)
  size = [peg_d+0.01, 0.0, 0.01]
  position = [0.0,0.0,0.005]
  pos_ref = peg_name + '_handle'
  parent = peg_name + '_handle'
  collection = ''
  static = True
  respondable = False
  use_dummy_as_handle = False
  peg_vis_check_obj = utils.generateShape(sim_client, name, shape_type, size, position, orientation, pos_ref, mass, green,
                            parent,collection,static,respondable,use_dummy_as_handle)
  '''
                            
  # log info about peg in file
  data_to_log = [scene_no,hole_d,peg_d,peg_pos_x,peg_pos_y,peg_pos_z,peg_l]
  if file_scenes:
    write_list_of_lists_to_txt_file(file_scenes,data_to_log,round_digits=6)
  #input("press a key")
  # close the gripper
  utils.control_physics(sim_client,1)
  gripper.open()
  time.sleep(0.5)    
  gripper.close()
  time.sleep(0.5)  
  utils.control_physics(sim_client,0) 
  

  return plane_obj,plane_pos,plane_name,peg_obj,peg_name


class gridVisitedArray(object):
  '''Class that keeps track which (initial) states have been visited.
    
    This is used by the data generation for supervised learning, so that
    the same (initial) state is not repeated (which would result in identical
    samples).
    The grid_visited is initialized using the collision grid and visibility
    grid (grid points are considered "visited" if they are in collision or the
    target is not visible)
  '''
  def __init__(self,grid_col,grid_vis_target,grid_vis_obj):
    
    self.grid_visited, self.num_not_visited = self.create_grid_visited(grid_col,grid_vis_target,grid_vis_obj)
  
  def create_grid_visited(self,grid_col,grid_vis_target,grid_vis_obj):
    # create an array where "false" indicates that grid point can be visited (no collision and target visible) and wasn't visited yet
    # Set to true if it can't be visited (collision or target not visible)   
    grid_visited1 = (grid_col ==1)  # true if in collision or target not visible
    grid_visited2 = (grid_vis_target < 16)
    grid_visited3 = (grid_vis_obj < 16) # usually around 20 pixels visible if not occluded
    grid_visited = np.logical_or(grid_visited1, grid_visited2)    
    grid_visited = np.logical_or(grid_visited, grid_visited3)   

    num_not_visited = (grid_visited == False).sum()
    return grid_visited, num_not_visited

  def visit_state(self,x,y,z,t=1):
    if(t==1):
      if(self.grid_visited[z,y,x] == False):
        self.grid_visited[z,y,x] = True
        self.num_not_visited = self.num_not_visited-1
        
    else:
      if(self.grid_visited[t,z,y,x] == False):
        self.grid_visited[t,z,y,x] = True
        self.num_not_visited = self.num_not_visited-1
    assert(self.num_not_visited >= 0), "Error self.num_not_visited is negative"
          
  def get_grid_visited(self):
    return self.grid_visited
    
  def get_visited(self,x,y,z,t=1):
    if(t==1):
      return self.grid_visited[z,y,x]
    else:
      return self.grid_visited[t,z,y,x]
      

  def get_num_not_visited(self):
    return self.num_not_visited
  
  def count_num_not_visited_high_z(self,min_z_i):
    # Asume t=1
    num_not_visited_high_z = (self.grid_visited[min_z_i:,:,:] == False).sum()
    return num_not_visited_high_z

# used for data generation?
def sample_visible_and_collision_free_state(sim_client,obj_handle,\
                                            grid_col,grid_vis_target,grid_vis_obj,\
                                            values_x,values_y,values_z,\
                                            grid_visited,\
                                            update_grid_visited=True,\
                                            high_z_only=False):
  '''Samples uniformly a grid point that is NOT in collision or with target occlusion.
    
  This also keeps track which grid points are being visited and are excluded
  during the next sampling (for supervised learning, identical samples should
  be avoided, but for RL it is okay to sample same start point many times.)
  '''

  if high_z_only:
    min_z_i = int(len(values_z)/2)
  else:
    min_z_i = 0

  if grid_visited.get_num_not_visited() == 0:
    print("No visible and collision free state was found, need to resample clutter?")
    return False
  state_x_i = random.choice(range(len(values_x)))
  state_y_i = random.choice(range(len(values_y)))
  state_z_i = random.choice(range(min_z_i,len(values_z)))
  
  while grid_visited.get_visited(state_x_i,state_y_i,state_z_i) and grid_visited.get_num_not_visited() > 0:
    if grid_visited.count_num_not_visited_high_z(min_z_i) == 0:
      print("No visible and collision free state for min_z_i ", min_z_i)
      return False
    # high_z_only = True, this might end up in an endless loop, if  upper half are all visited (but lower half still has some unvisited)
    state_x_i = random.choice(range(len(values_x)))
    state_y_i = random.choice(range(len(values_y)))
    state_z_i = random.choice(range(min_z_i,len(values_z)))

    
  if update_grid_visited: # For RL do not update grid_visited, because we want to allow to start from same init pose   
    grid_visited.visit_state(state_x_i,state_y_i,state_z_i) # update that grid was visited

  
  # Use lookup table to check visibility (this works if we sample initial states on grid)
  NumPixelsVisible_target = grid_vis_target[state_z_i, state_y_i, state_x_i]
  NumPixelsVisible_obj = grid_vis_obj[state_z_i, state_y_i, state_x_i]  
  collision = grid_col[state_z_i, state_y_i, state_x_i] # order z,y,x for index seem to work  
  
  assert(NumPixelsVisible_target >= 16 and NumPixelsVisible_obj >= 16 and collision == 0), "NumPixelsVisible_target < 16 or NumPixelsVisible_obj < 16 or collision == 1"
    
  position = [values_x[state_x_i], values_y[state_y_i], values_z[state_z_i]]
  utils.setObjectPosition(sim_client,obj_handle, position)
  # add oriantation later
  #utils.setObjectOrientation(sim_client,obj_handle, [0,0,math.pi/2])
  
  return position,collision,NumPixelsVisible_target,NumPixelsVisible_obj,grid_visited


def getCollisionGrid(sim_client,planner_id,dim_x,dim_y,dim_z,dim_t=1):
  grid_as_list = utils.getGridCol(sim_client,planner_id)
  grid_col = utils.grid_shaped_as_nparray(grid_as_list,dim_x,dim_y,dim_z,dim_t)
  return grid_col

def getVisibilityGridTarget(sim_client,planner_id,dim_x,dim_y,dim_z,dim_t=1):
  grid_as_list = utils.getGridVisTarget(sim_client,planner_id)
  grid_vis = utils.grid_shaped_as_nparray(grid_as_list,dim_x,dim_y,dim_z,dim_t)
  return grid_vis

def getVisibilityGridObj(sim_client,planner_id,dim_x,dim_y,dim_z,dim_t=1):
  grid_as_list = utils.getGridVisObj(sim_client,planner_id)
  grid_vis = utils.grid_shaped_as_nparray(grid_as_list,dim_x,dim_y,dim_z,dim_t)
  return grid_vis

def getDistanceGrid(sim_client,planner_id,dim_x,dim_y,dim_z,dim_t=1):
  grid_as_list, maxDistance = utils.getGridDist(sim_client,planner_id)
  grid_dist = utils.grid_shaped_as_nparray(grid_as_list,dim_x,dim_y,dim_z,dim_t)
  return grid_dist, maxDistance

def find_closest_index(value, sorted_list):
  ''' Find the index with the closest value in sorted_list to "value".
  
  Inputs:
    - value (float): the value that should be searched in sorted_list
    - sorted_list (list of floats): The list used to find the value with index
      that is closest to "value".
  
  Returns:
    - ind_ret (int): The index for the value in sorted_list closest to "value"
  '''
  idx_low = 0
  idx_high = len(sorted_list)-1

  while(idx_low != idx_high):
    idx_mid = int((idx_low+idx_high)/2)
    if value > sorted_list[idx_mid]:
      idx_low = idx_mid+1
    else:
      idx_high = idx_mid
  idx_ret = None
  dist = float("inf")  
  for idx in [idx_mid-1,idx_mid, idx_mid+1]:
    if idx >= 0 and idx <= len(sorted_list)-1:
      if abs(sorted_list[idx]-value) < dist:
        dist = abs(sorted_list[idx]-value)
        idx_ret = idx
  return idx_ret
      

def get_closest_grid_indices_from_position(position, values_x, values_y, values_z):
  ''' Find the grid point closest to position (3D grid).
  
  '''
  idx_x = find_closest_index(position[0],values_x)
  idx_y = find_closest_index(position[1],values_y)
  idx_z = find_closest_index(position[2],values_z)
  
  return idx_z,idx_y,idx_x
  
  

def save_image_and_labels(sim_client, sensor_d,
                          scene_no, traj_no, img_no,
                          dataset_path_images,
                          planning_handle, peg_name, plane_name,
                          file_images,
                          grid_vis_target,
                          grid_vis_obj,
                          grid_dist,
                          position,
                          values_x, values_y, values_z,
                          grid_x, grid_y, grid_z):
    depth_data = sensor_d.getDepthData() # 0.5 meters=black, 0 meters=white

    # Save image and label
    scene_no_str = str(scene_no).zfill(6)
    traj_no_str = str(traj_no).zfill(6)
    img_no_str = str(img_no).zfill(6)
    #image_file_name_old = 'img_' + scene_no_str + '_' + traj_no_str + '_' + img_no_str +'_8bit.png'  
    #plt.imsave(dataset_path_images + image_file_name_old, depth_data,cmap=plt.cm.gray, vmin=0, vmax=1)
    
    # save as 16 bit
    image_file_name = 'img_' + scene_no_str + '_' + traj_no_str + '_' + img_no_str +'.png'
    PNG.write_png16bit(depth_data, dataset_path_images + image_file_name)
    


    q_values2, best_action2, _ , pos_target_in_robot, orient_target_in_robot, _, best_index2 =\
      utils.getFewerLabelsForCurrentState(sim_client, planning_handle, peg_name + '_handle', plane_name + '_handle')      
    
    
    state_z_i, state_y_i, state_x_i = get_closest_grid_indices_from_position(position, values_x, values_y, values_z)
    NumPixelsVisibleTarget = grid_vis_target[state_z_i, state_y_i, state_x_i]
    NumPixelsVisibleObj = grid_vis_obj[state_z_i, state_y_i, state_x_i]
    distToObstacles = grid_dist[state_z_i, state_y_i, state_x_i]
    
    # best_index2 = 14: no action for "stay in place"
    traj_last = 1 if best_index2 == 14 else 0
    
    # reduced number label info to file
    data_to_file = [image_file_name,scene_no,traj_no,img_no,q_values2,best_index2,best_action2,
                    pos_target_in_robot,orient_target_in_robot,NumPixelsVisibleTarget,
                    NumPixelsVisibleObj,traj_last,distToObstacles,
                    grid_x, grid_y, grid_z]
    write_list_of_lists_to_txt_file(file_images,data_to_file,round_digits=5)  

def write_list_to_txt_file(file_out,list_of_data):
    for info in list_of_data:    
        file_out.write(str(info))
        file_out.write(',')
    file_out.write('\n')  

def write_list_of_lists_to_txt_file(file_out,list_of_lists,round_digits=5):
  '''
  Elements between lists (top level) are comma seperated, then in sublists are space separated.
  Float numbers are rounded to 5 digits.
  
  # Example:
  # list_of_lists = ["string",0.123,1,[1,2,3],[0.12345667,9.61837628368], ["string", "anotherString", 0.14454]]
  # result in file_out is:
  # string,0.123,1,1 2 3,0.12346 9.61838,string anotherString 0.14454
  '''
  for i,l in enumerate(list_of_lists):
    if isinstance(l, list):
      for j,x in enumerate(l):
        if isinstance(x, float) and round_digits:
          x = round(x,round_digits)
        file_out.write(str(x))
        if j != len(l)-1: # do not write the last ' ', because this will be a ',' instead, if there are more poses to be written
          file_out.write(' ')

    else: # not a list
      if isinstance(l, float) and round_digits:
        l = round(l,round_digits)
      file_out.write(str(l))      
    if i != len(list_of_lists)-1: # do not write the last ','
      file_out.write(',')
  file_out.write('\n')      

# used directly by DQN_multiple_scenes_eval.py
# or indirectly by other scripts using create_logging_files below
def logging_files_for_save_experience(dataset_path,config_file='config.ini'):
  '''
    Save images and labels for SL learning.
  '''  
  dataset_path_images = os.path.join(dataset_path,'images') + "/"
  dataset_path_labels = os.path.join(dataset_path,'labels') + "/"
  if(os.path.exists(dataset_path_images) == False): #check if directory exist
      os.makedirs(dataset_path_images)
  else:
    print("Warning: Folder already exists", dataset_path_images)
    input("Press a key to continue")
  
  if(os.path.exists(dataset_path_labels) == False): #check if directory exist
      os.makedirs(dataset_path_labels)
  else:
    print("Warning: Folder already exists", dataset_path_labels)
    input("Press a key to continue")
  
  file_general = open(dataset_path_labels + "info_general.csv", "a")    
  file_scenes = open(dataset_path_labels + "info_scenes.csv", "a")
  file_images = open(dataset_path_labels + "info_images.csv", "a")
  
  #delta_actions = ["delta_action_x","delta_action_y","delta_action_z","delta_action_theta"]
  #bounds_global = ["x_min","x_max","y_min","y_max","z_min","z_max"]
  #grid_dims = ["dim_x","dim_y","dim_z","dim_t"]
  #position_target_global = ["p_target_x","p_target_y","p_target_z"]
  headers_general = ["position_target_global", "values_x", "values_y", "values_z","maxDistance"]
  write_list_to_txt_file(file_general,headers_general)
  
  headers_scenes = ["scene_no","hole_d","peg_d","peg_pos_x","peg_pos_y","peg_pos_z","peg_l"]
  write_list_to_txt_file(file_scenes,headers_scenes)

  headers_images = ["image_name","scene_no","traj_no","img_no","q_values","best_index","best_action",\
                    "pos_target_in_robot","orient_target_in_robot",\
                    "NumPixelsVisibleTarget","NumPixelsVisibleObj","traj_last","distToObstacles",
                    "grid_x","grid_y","grid_z"]
  write_list_to_txt_file(file_images,headers_images)
  
  file_general.flush()
  file_scenes.flush()
  file_images.flush()

  return file_general,file_scenes,file_images,dataset_path_images,dataset_path_labels  
  

def create_logging_files(dataset_path,git_location,RL=False,config_file='config.ini'):
  '''
    Setup logging files for relevant info on different levels (general, scenes, images)
    Also backup script files and simulation file.
  '''
  
  # do the SL dataset related stuff
  file_general,file_scenes,file_images,dataset_path_images,dataset_path_labels =\
    logging_files_for_save_experience(dataset_path,config_file)
  
  # copy main script to dataset_path_labels (as backup)
  #file_main_script = os.path.realpath(__file__)
  if RL: # for storing the full state space for RL offline learning
    file_main_script = os.path.join(git_location,"RL/DQN_save_GT.py")    
    scene_gen_utils = os.path.join(git_location,"scripts/scene_gen_utils.py")
    utils_file = os.path.join(git_location,"utils/vrep_utils.py")
    scene_file = os.path.join(git_location,"simulation/scenes/vrep_scene_ur5_v27_peg_insertion_merged25b_26.ttt")
    config_file = os.path.join(git_location,"RL/",config_file)  
    offlineSim_file = os.path.join(git_location,"RL/offlineSimEnv.py")     
    files_to_copy = [file_main_script,scene_gen_utils,utils_file,scene_file,config_file,offlineSim_file]
    copy_files_to_path(files_to_copy,dataset_path_labels)      
  else: # for the supervised learning data generation
    file_main_script = os.path.join(git_location,"scripts/data_generation.py")  
    scene_gen_utils = os.path.join(git_location,"scripts/scene_gen_utils.py")
    utils_file = os.path.join(git_location,"utils/vrep_utils.py")
    scene_file = os.path.join(git_location,"simulation/scenes/vrep_scene_ur5_v27_peg_insertion_merged25b_26.ttt")
    config_file = os.path.join(git_location,"scripts/", config_file)  
    files_to_copy = [file_main_script,scene_gen_utils,utils_file,scene_file,config_file]
    copy_files_to_path(files_to_copy,dataset_path_labels)  
  
  return file_general,file_scenes,file_images,dataset_path_images,dataset_path_labels

def copy_files_to_path(files,path):
  for src in files:
    if os.path.isfile(src) and os.path.exists(path):
      dst = os.path.join(path,os.path.basename(src))
      copyfile(src, dst)
    else:
      print("Error copying file")
      if os.path.isfile(src) == False:
        print("Source file does not exist: ", src)
      if os.path.exists(path) == False:
        print("Output path does not exist: ", path)
      return False
  return True



