import os
import sys
import time
from timeit import default_timer as timer
import numpy as np

from simulation import vrep
import utils.vrep_utils as utils
from utils import transformations

class UR5(object):
  '''
  VRep UR5 robot class. Works with any gripper included in this package.
  Currently does IK control and setting joint values directly.

  Args:
    - sim_client: VRep client object to communicate with simulator over
    - gripper: Gripper which is attached to UR5 in simulator. Currently using robotiq85.
    '''
  def __init__(self, sim_client, gripper):  
    self.sim_client = sim_client
    self.UR5 = utils.getObjectHandle(self.sim_client, 'UR5')    
    self.gripper = gripper
    self.num_joints = 6

    # Create handles to the UR5 target and tip which control IK control
    sim_ret, self.UR5_target = utils.getObjectHandle(self.sim_client, 'UR5_target')
    sim_ret, self.UR5_tip = utils.getObjectHandle(self.sim_client, 'UR5_tip')

    # get joint handles    
    self.jh = []
    for i in range(self.num_joints):
      sim_ret, joint = utils.getObjectHandle(self.sim_client,'UR5_joint'+str(i+1))
      if sim_ret != 0:
          print("Error while getting joint handles of robot")
      self.jh.append(joint)      

  def getEndEffectorPose(self):
    '''
    Get the current end effector pose

    Returns: 4D pose of the gripper
    '''
    sim_ret, pose = utils.getObjectPose(self.sim_client, self.UR5_tip)
    return pose

  def openGripper(self):
    '''
    Opens the gripper as much as is possible
    '''
    self.gripper.open()

  def closeGripper(self):
    '''
    Closes the gripper as much as is possible

    Returns: True if gripper is fully closed, False otherwise
    '''
    return self.gripper.close()

  def moveTo(self, pose):
    '''
    Moves the tip of the gripper to the target pose

    Args:
      - pose: 4D target pose
    '''
    # Get current position and orientation of UR5 target
    sim_ret, UR5_target_position = utils.getObjectPosition(self.sim_client, self.UR5_target)
    print("sim_ret, UR5_target_position", sim_ret, UR5_target_position)
    sim_ret, UR5_target_orientation = utils.getObjectOrientation(self.sim_client, self.UR5_target)
    print("UR5_target_orientation", UR5_target_orientation)

    # Calculate the movement increments
    move_direction = pose[:3,-1] - UR5_target_position
    move_magnitude = np.linalg.norm(move_direction)
    move_step = 0.01 * move_direction / move_magnitude
    num_move_steps = int(np.floor(move_magnitude / 0.01))

    # Calculate the rotation increments
    rotation = np.asarray(transformations.euler_from_matrix(pose))
    rotation_step = rotation - UR5_target_orientation
    rotation_step[rotation > 0] = 0.1
    rotation_step[rotation < 0] = -0.1
    num_rotation_steps = np.floor((rotation - UR5_target_orientation) / rotation_step).astype(np.int)

    # Move and rotate to the target pose
    for i in range(max(num_move_steps, np.max(num_rotation_steps))):
      pos = UR5_target_position + move_step*min(i, num_move_steps)
      rot = [UR5_target_orientation[0]+rotation_step[0]*min(i, num_rotation_steps[0]),
             UR5_target_orientation[1]+rotation_step[1]*min(i, num_rotation_steps[1]),
             UR5_target_orientation[2]+rotation_step[2]*min(i, num_rotation_steps[2])]
      utils.setObjectPosition(self.sim_client, self.UR5_target, pos)
      utils.setObjectOrientation(self.sim_client, self.UR5_target, rot)

# Todo, get all joint positions at the same time (similar to setjoints)
  def getJoints(self):
      '''
      Get the joints of the UR5 to a target joint configuration
      
      Returns:
        - joint_positions: list of floats that specify the joint positions in radians.
      '''
      joint_positions = []
      for i,joint in enumerate(self.jh):
          sim_ret, position = utils.getJointPosition(self.sim_client,joint)
          joint_positions.append(position)
      return joint_positions

  def setJoints(self, joint_positions):
      '''
      Set the joints of the UR5 to a target joint configuration
      This requires the dynamics to be disables and
      IK disables for these joints to work propoerly
      
      Args:
        - joint_positions: list of floats that specify the joint positions in radians.
      '''
      
      utils.setJointPositions(self.sim_client,self.jh,joint_positions)


# I might want to make that a script on the simulator side (because otherwise many api commands might be too slow)         
  def follow_trajectory_in_joint_space(self,path):
      '''
      Move the robot along a joint trajectory
      Args:
      - path: a sequence of joint positions (as a long simple list)
      '''
      # convert list to list of lists (for list of joints positions of length self.num_joints)
      path_joint_positions = [path[i:i+self.num_joints] for i in range(0, len(path), self.num_joints)]
      for i,joint_positions in enumerate(path_joint_positions):
          start = timer()
          self.setJoints(joint_positions)
          time.sleep(0.01) # adding a delay so that the montion of the gripper can be seen (should be removed if collecting data)
          end = timer()          

  def follow_trajectory_in_pose_space(self,path):
      '''
      Move the robot along a 6-DOF pose trajectory for the end-effector
      Args:
      - path: a sequence of poses (as a long simple list). A pose consists of 3 values for position and 4 values for rotation (quaternion)
      '''
      # convert list to list of lists (for list of poses of length 7)
      path_poses = [path[i:i+7] for i in range(0, len(path), 7)]
      for i,pose in enumerate(path_poses):
          start = timer()
          pos = pose[0:3] 
          quat = pose[3:7]
          utils.setObjectPosition(self.sim_client, self.UR5_target, pos)
          #if i == 0: debug output
          #    UR5_target_q = utils.getObjectQuaternion(self.sim_client, self.UR5_target)
          #    print "UR5_target_q", UR5_target_q
          #    print "quat", quat
          utils.setObjectQuaternion(self.sim_client, self.UR5_target, quat)
          time.sleep(0.01) # adding a delay so that the montion of the gripper can be seen (should be removed if collecting data)
          end = timer()       
          #print "time delta", end - start  

  def follow_trajectory_in_position_space(self,path):
      '''
      Move the robot along a 3-DOF pose trajectory (position only) for the end-effector
      Args:
      - path: a sequence of poses (as a long simple list). A pose consists of 3 values for position
      '''
      # convert list to list of lists (for list of posistions of length 3)
      path_positions = [path[i:i+3] for i in range(0, len(path), 3)]
      for i,position in enumerate(path_positions):
          start = timer()
          pos = position[0:3] 
          #quat = pose[3:7]
          utils.setObjectPosition(self.sim_client, self.UR5_target, pos)
          #utils.setObjectQuaternion(self.sim_client, self.UR5_target, quat)
          #time.sleep(1.0) # adding a delay so that the montion of the gripper can be seen (should be removed if collecting data)
          end = timer()       


  def follow_trajectory_in_position_1rot_space(self,path):
      '''
      Move the robot along a 3-DOF pose +1 dof rot trajectory (position and rotation about z-axis) for the end-effector
      Args:
      - path: a sequence of poses (as a long simple list). A pose consists of 3 values for position and one value for rotation about z  axis (Yaw)
      '''
      # convert list to list of lists (for list of posistions of length 3)
      path_poses = [path[i:i+4] for i in range(0, len(path), 4)]
      for i,pose in enumerate(path_poses):
          #start = timer()
          pos = pose[0:3] 
          rot_z = pose[3]#quat = pose[3:7]
          utils.setObjectPosition(self.sim_client, self.UR5_target, pos)
          utils.setObjectOrientation(self.sim_client, self.UR5_target,[0,0,rot_z])
          #utils.setObjectQuaternion(self.sim_client, self.UR5_target, quat)
          #time.sleep(1.0) # adding a delay so that the montion of the gripper can be seen (should be removed if collecting data)
          #end = timer()       
          #print "time delta", end - start            
      
  def pick(self, grasp_pose, offset):
    '''
    Attempts to execute a pick at the target pose

    Args:
      - grasp_pose: 4D pose to execture grasp at
      - offset: Grasp offset for pre-grasp pose

    Returns: True if pick was successful, False otherwise
    '''
    pre_grasp_pose = np.copy(grasp_pose)
    # TODO: offset should be along approach vector. Currently just the z component.
    pre_grasp_pose[2,-1] += offset

    self.openGripper()
    self.moveTo(pre_grasp_pose)
    self.moveTo(grasp_pose)
    is_fully_closed = self.closeGripper()
    self.moveTo(pre_grasp_pose)

    is_full_closed = self.closeGripper()

    return not is_fully_closed

  def place(self, drop_pose, offset):
    '''
    Attempts to execute a place at the target pose

    Args:
      - drop_pose: 4D pose to place object at
      - offset: Grasp offset for pre-grasp pose
    '''
    pre_drop_pose = np.copy(drop_pose)
    pre_drop_pose[2,-1] += offset

    self.moveTo(pre_drop_pose)
    self.moveTo(drop_pose)
    self.openGripper()
    self.moveTo(pre_drop_pose)
